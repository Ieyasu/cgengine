#include "stdafx.h"
#include "CppUnitTest.h"
#include "World.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;
using namespace cg::entity;

namespace CGEntityTests
{		
	TEST_CLASS(UnitTest1)
	{
	public:

		TEST_METHOD(TestEmptyWorld)
		{
			World world;
			const auto& entities = world.getAllEntities();
			Assert::AreEqual(0, (int)entities.size(), L"Newly created World is not empty.");
			Assert::AreEqual(0, (int)entities.capacity(), L"Newly created World is not empty.");
		}
		
		TEST_METHOD(TestSingleEntityHandle)
		{
			World world;
			const auto& entities = world.getAllEntities();

			Handle<Entity> entity = world.createEntity();
			Assert::IsTrue(entity.isValid(), L"Newly created Handle<Entity> is invalid.");
			Assert::IsTrue(entity->isEnabled(), L"Newly created Entity is disabled.");
			Assert::AreEqual(1, (int)entities.size(), L"World's Entity count doesn't match amount of created Entities.");

			entity->setEnabled(false);
			Assert::IsFalse(entity->isEnabled(), L"Disabled Entity is still enabled.");
			Assert::AreEqual(1, (int)entities.size(), L"World's Entity count doesn't match amount of created Entities.");

			entity->destroy();
			Assert::IsFalse(entity.isValid(), L"Handle<Entity> pointing to destroyed Entity is still valid.");
			Assert::AreEqual(0, (int)entities.size(), L"World's Entity count doesn't match amount of created Entities.");
		}

		TEST_METHOD(TestSharedEntityHandles)
		{
			World world;
			const auto& entities = world.getAllEntities();

			Handle<Entity> entity1 = world.createEntity();
			Assert::AreEqual(1, (int)entities.size(), L"World's Entity count doesn't match amount of created Entities.");
			Handle<Entity> entity2 = entity1;
			Assert::AreEqual(1, (int)entities.size(), L"World's Entity count doesn't match amount of created Entities.");

			entity1->setEnabled(false);
			Assert::IsFalse(entity1->isEnabled(), L"Disabled Entity is still enabled.");
			Assert::IsFalse(entity2->isEnabled(), L"Disabled Entity is still enabled.");

			entity2->setEnabled(true);
			Assert::IsTrue(entity1->isEnabled(), L"Enabled Entity is still disabled.");
			Assert::IsTrue(entity2->isEnabled(), L"Enabled Entity is still disabled.");

			entity1->destroy();
			Assert::IsFalse(entity1.isValid(), L"Handle<Entity> pointing to destroyed Entity is still valid.");
			Assert::IsFalse(entity2.isValid(), L"Handle<Entity> pointing to destroyed Entity is still valid.");
			Assert::AreEqual(0, (int)entities.size(), L"World's Entity count doesn't match amount of created Entities.");
		}

		TEST_METHOD(TestSingleEntityHandleRepeatedDestroy)
		{
			World world;
			const auto& entities = world.getAllEntities();

			int n = 1000;

			for (int i = 0; i < n; ++i)
			{
				Assert::AreEqual(0, (int)entities.size(), L"World's Entity count doesn't match amount of created Entities.");
				Handle<Entity> entity = world.createEntity();
				Assert::AreEqual(1, (int)entities.size(), L"World's Entity count doesn't match amount of created Entities.");
				Assert::IsTrue(entity.isValid(), L"Newly created Handle<Entity> is invalid.");
				entity->destroy();
				Assert::IsFalse(entity.isValid(), L"Destroyed Handle<Entity> is still valid.");
			}
		}

		TEST_METHOD(TestMultipleEntityHandles1)
		{
			World world;
			const auto& entities = world.getAllEntities();

			int n = 1000;
			std::vector<Handle<Entity>> entityHandles;
			
			for (int i = 0; i < n; ++i)
			{
				Handle<Entity> entity = world.createEntity();
				Assert::IsTrue(entity.isValid(), L"Newly created Handle<Entity> is invalid.");
				Assert::IsTrue(entity->isEnabled(), L"Newly created Entity is disabled.");
				Assert::AreEqual(i + 1, (int)entities.size(), L"World's Entity count doesn't match amount of created Entities.");
				entityHandles.push_back(entity);
			}

			for (int i = 0; i < n; ++i)
			{
				Handle<Entity> entity = entityHandles[i];
				Assert::IsTrue(entity.isValid(), L"Handle<Entity> is not valid.");
				entity->destroy();
				Assert::IsFalse(entity.isValid(), L"Destroyed Handle<Entity> is still valid.");
				Assert::AreEqual(n - i - 1, (int)entities.size(),L"World's Entity count doesn't match amount of created Entities.");
			}
		}

		TEST_METHOD(TestMultipleEntityHandles2)
		{
			World world;
			const auto& entities = world.getAllEntities();

			int n = 1000, nDestroyed = 0;
			std::vector<Handle<Entity>> entityHandles = world.createEntities(n);
			Assert::AreEqual(n, (int)entities.size(), L"World's Entity count doesn't match amount of created Entities.");

			for (int i = 0; i < n; ++i)
			{
				Handle<Entity> entity = entityHandles[i];
				Assert::IsTrue(entity.isValid(), L"Newly created Handle<Entity> is invalid.");
				Assert::IsTrue(entity->isEnabled(), L"Newly created Entity is disabled.");
				if (i % 2 == 0)
				{
					entity->destroy();
					nDestroyed++;
					Assert::IsFalse(entity.isValid(), L"Destroyed Handle<Entity> is still valid."); 
					Assert::AreEqual(n - nDestroyed, (int)entities.size(), L"World's Entity count doesn't match amount of created Entities.");
				}
			}

			

			for (int i = 0; i < n; ++i)
			{
				Handle<Entity> entity = entityHandles[i];
				if (i % 2 == 0)
				{
					Assert::IsFalse(entity.isValid(), L"Destroyed Handle<Entity> is still valid.");
				}
				else
				{
					Assert::IsTrue(entity.isValid(), L"Handle<Entity> is not valid.");
				}
			}

			for (int i = 0; i < n / 2; ++i)
			{
				if (i % 2 == 0)
				{
					entityHandles[i] = world.createEntity();
					nDestroyed--;
					Assert::AreEqual(n - nDestroyed, (int)entities.size(), L"World's Entity count doesn't match amount of created Entities.");
				}
				Handle<Entity> entity = entityHandles[i];
				Assert::IsTrue(entity.isValid(), L"Handle<Entity> is invalid.");
				Assert::IsTrue(entity->isEnabled(), L"Entity is disabled.");
			}
		}

		TEST_METHOD(TestMultipleEntityHandles3)
		{
			World world;
			const auto& entities = world.getAllEntities();

			int n = 1000, nDestroyed = 0;
			std::vector<Handle<Entity>> entityHandles = world.createEntities(n);
			Assert::AreEqual(n, (int)entities.size(), L"World's Entity count doesn't match amount of created Entities.");

			for (int i = 0; i < n; ++i)
			{
				Handle<Entity> entity = entityHandles[i];
				Assert::IsTrue(entity.isValid(), L"Newly created Handle<Entity> is invalid.");
				Assert::IsTrue(entity->isEnabled(), L"Newly created Entity is disabled.");
			}

			std::vector<Handle<Entity>> entityHandles2 = world.createEntities(n);
			Assert::AreEqual(2 * n, (int)entities.size(), L"World's Entity count doesn't match amount of created Entities.");

			for (int i = 0; i < n; ++i)
			{
				Handle<Entity> entity = entityHandles[i];
				Assert::IsTrue(entity.isValid(), L"Handle<Entity> is invalid.");
				Assert::IsTrue(entity->isEnabled(), L"Entity is disabled.");
				entity->destroy();
				Assert::IsFalse(entity.isValid(), L"Destroyed Handle<Entity> is still valid.");
				Assert::AreEqual(2 * n - i - 1, (int)entities.size(), L"World's Entity count doesn't match amount of created Entities.");
			}

			for (int i = 0; i < n; ++i)
			{
				Handle<Entity> entity = entityHandles2[i];
				Assert::IsTrue(entity.isValid(), L"Handle<Entity> is invalid.");
				Assert::IsTrue(entity->isEnabled(), L"Entity is disabled.");
				entity->destroy();
				Assert::IsFalse(entity.isValid(), L"Destroyed Handle<Entity> is still valid.");
				Assert::AreEqual(n - i - 1, (int)entities.size(), L"World's Entity count doesn't match amount of created Entities.");
			}
		}
	};
}