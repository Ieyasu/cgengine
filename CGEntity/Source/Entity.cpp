#include "CGEntity/Entity.h"

#include "CGEntity/World.h"

namespace cg
{
    namespace entity
    {
        Entity::Entity()
            : m_isEnabled(true)
            , m_world(nullptr)
        {
        }

        Entity::Entity(World& world)
            : m_isEnabled(true)
            , m_world(&world)
        {
        }

        void Entity::destroy()
        {
            m_world->destroyEntity(*this);
        }

        void Entity::setEnabled(bool enabled)
        {
            if (enabled)
            {
                m_world->enableEntity(*this);
            }
            else
            {
                m_world->disableEntity(*this);
            }
        }

        bool Entity::isEnabled() const
        {
            return m_isEnabled;
        }
    }  // namespace entity
}  // namespace cg
