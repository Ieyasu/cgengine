#include "CGEntity/Handles/Handle.h"

namespace cg
{
    namespace entity
    {
        HandleBase::HandleBase()
            : m_value(0)
            , m_handleArray(nullptr)
        {
        }

        HandleBase::HandleBase(HandleArrayBase& handleArray, const HandleValue& value)
            : m_value(value)
            , m_handleArray(&handleArray)
        {
        }

        const HandleValue& HandleBase::getValue() const
        {
            return m_value;
        }
    }  // namespace entity
}  // namespace cg
