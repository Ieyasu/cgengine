#include "CGEntity/World.h"

namespace cg
{
    namespace entity
    {
        void World::update()
        {
            for (auto& system : m_systems)
            {
                system->onUpdate();
            }
        }

        Handle<Entity> World::createEntity()
        {
            auto entityHandle = m_entities.insert(*this);
            entityHandle->m_handle = entityHandle;
            return entityHandle;
        }

        std::vector<Handle<Entity>> World::createEntities(size_t n)
        {
            // Reserve space in advance.
            std::vector<Handle<Entity>> entityHandles;
            entityHandles.reserve(n);
            m_entities.reserve(m_entities.size() + (EntityCollection::handle_index_type)n);

            for (size_t i = 0; i < n; ++i)
            {
                auto entityHandle = m_entities.insert(*this);
                entityHandle->m_handle = entityHandle;
                entityHandles.push_back(entityHandle);
            }
            return entityHandles;
        }

        void World::destroyEntity(const Entity& entity)
        {
            // Remove all components the Entity owns.
            for (const auto& componentsOfType : entity.m_componentHandles)
            {
                const std::type_index& typeIndex = componentsOfType.first;
                HandleArrayBase*       allComponentsOfType = m_componentsByType[typeIndex].get();
                for (const auto& component : componentsOfType.second)
                {
                    allComponentsOfType->remove(component.getValue());
                }
            }

            m_entities.remove(entity.m_handle.getValue());
        }

        void World::enableEntity(Entity& entity)
        {
            if (!entity.m_isEnabled)
            {
                entity.m_isEnabled = true;
            }
        }

        void World::disableEntity(Entity& entity)
        {
            if (entity.m_isEnabled)
            {
                entity.m_isEnabled = false;
            }
        }

        const World::EntityCollection& World::getAllEntities() const
        {
            return m_entities;
        }

        bool World::addSystem(std::shared_ptr<System> system)
        {
            if (system->m_world != nullptr)
            {
                return false;
            }

            m_systems.push_back(system);
            system->m_world = this;
            system->onInitialize();
            return true;
        }

        bool World::removeSystem(std::shared_ptr<System> system)
        {
            for (auto it = m_systems.begin(), end = m_systems.end(); it != end; ++it)
            {
                if (*it == system)
                {
                    system->onDestroy();
                    m_systems.erase(it);
                    return true;
                }
            }

            return false;
        }
    }  // namespace entity
}  // namespace cg
