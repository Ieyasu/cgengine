#include "CGEntity/System.h"

#include "CGEntity/World.h"

namespace cg
{
    namespace entity
    {
        System::System()
            : m_world(nullptr)
        {
        }

        void System::onInitialize()
        {
        }

        void System::onUpdate()
        {
        }

        void System::onDestroy()
        {
        }
    }  // namespace entity
}  // namespace cg
