#pragma once

#include <memory>

namespace cg
{
    namespace entity
    {
        class World;

        /// <summary>
        /// Used to handle updating the game state using entities and components.
        /// </summary>
        class System
        {
            friend World;

        public:
            virtual ~System() = default;

        protected:
            /// <summary>
            /// Creates a new empty System.
            /// </summary>
            System();

            /// <summary>
            /// Called when the system is initialized. This is called before any <see cref="System::onUpdate"/> calls.
            /// </summary>
            virtual void onInitialize();

            /// <summary>
            /// Called every update cycle of the World.
            /// </summary>
            virtual void onUpdate();

            /// <summary>
            /// Called when the system is destroyed. No update calls are called after this
            /// </summary>
            virtual void onDestroy();

            /// <summary>
            /// The World this System belongs to.
            /// </summary>
            World* m_world;
        };
    }  // namespace entity
}  // namespace cg
