#pragma once

#include <cassert>
#include <utility>
#include <vector>

#include "Handle.h"

namespace cg
{
    namespace entity
    {
        class HandleArrayBase
        {
        public:
            virtual ~HandleArrayBase() = default;
            using handle_index_type = HandleIndex;
            using handle_counter_type = HandleCounter;
            using handle_value_type = HandleValue;
            using size_type = handle_index_type;

            virtual bool remove(const handle_value_type& handleValue) = 0;
        };

        template <class T>
        class HandleArray : public HandleArrayBase
        {
        public:
            using value_type = T;
            using handle_type = Handle<value_type>;
            using DataContainer = std::vector<value_type>;
            using HandleIndexContainer = std::vector<handle_index_type>;
            using iterator = typename DataContainer::iterator;
            using reverse_iterator = typename DataContainer::reverse_iterator;
            using const_iterator = typename DataContainer::const_iterator;
            using const_reverse_iterator = typename DataContainer::const_reverse_iterator;

        private:
            struct HandleMapper
            {
                HandleMapper();
                explicit HandleMapper(handle_index_type nextInactiveIndex);

                handle_index_type   nextInactiveIndex;
                handle_counter_type counter;
                bool                isActive;
                handle_index_type   dataIndex;
            };
            using HandleMapperContainer = std::vector<HandleMapper>;

        public:
            HandleArray();

            void clear();
            void reserve(size_type n);

            template <class... Args>
            HandleArray<T>::handle_type insert(Args&&... data);
            HandleArray<T>::handle_type insert(const value_type& data);

            bool              remove(const handle_value_type& handleValue) override;
            value_type*       at(const handle_value_type& handleValue);
            const value_type* at(const handle_value_type& handleValue) const;
            value_type*       operator[](const handle_value_type& handleValue);
            const value_type* operator[](const handle_value_type& handleValue) const;

            const DataContainer& data() const;
            size_type            capacity() const noexcept;
            size_type            size() const noexcept;
            bool                 empty() const noexcept;

            iterator       begin();
            iterator       end();
            const_iterator begin() const;
            const_iterator end() const;
            const_iterator cbegin() const noexcept;
            const_iterator cend() const noexcept;

        private:
            DataContainer        m_data;
            HandleIndexContainer m_handleIndices;

            handle_index_type     m_nextInactiveIndex;
            HandleMapperContainer m_handleMappers;

            static inline handle_value_type getHandleValue(
                    const handle_counter_type& counter,
                    const handle_index_type&   index);
            static inline handle_index_type   getHandleIndex(const handle_value_type& handle);
            static inline handle_counter_type getHandleCounter(const handle_value_type& handle);
        };

        template <class T>
        HandleArray<T>::HandleMapper::HandleMapper()
            : HandleMapper(0)
        {
        }

        template <class T>
        HandleArray<T>::HandleMapper::HandleMapper(handle_index_type nextInactiveIndex)
            : nextInactiveIndex(nextInactiveIndex)
            , counter(0)
            , isActive(false)
        {
        }

        template <class T>
        HandleArray<T>::HandleArray()
            : m_nextInactiveIndex(0)
        {
        }

        template <class T>
        void HandleArray<T>::clear()
        {
            m_data.clear();
            m_handleIndices.clear();
            m_handleMappers.clear();
            m_nextInactiveIndex = 0;
        }

        template <class T>
        void HandleArray<T>::reserve(size_type n)
        {
            m_data.reserve(n);
            m_handleIndices.reserve(n);

            // Create HandleMappers in advance.
            auto oldMappersSize = (size_type)m_handleMappers.size();
            if (n > oldMappersSize)
            {
                m_handleMappers.resize(n);
                for (auto i = oldMappersSize; i < n; ++i)
                {
                    m_handleMappers[i].nextInactiveIndex = i + 1;
                }
            }
        }

        template <class T>
        typename HandleArray<T>::handle_type HandleArray<T>::insert(const value_type& value)
        {
            // Make sure there's enough room for another handle.
            while (m_handleMappers.size() <= m_nextInactiveIndex)
            {
                auto nextFreeMapper = (handle_index_type)(m_handleMappers.size() + 1);
                m_handleMappers.emplace_back(nextFreeMapper);
            }

            // Get the next free handle entry and the corresponding index.
            const auto newIndex = m_nextInactiveIndex;
            auto&      handleMapper = m_handleMappers[newIndex];
            assert(!handleMapper.isActive);

            // Update mapper connections.
            m_nextInactiveIndex = handleMapper.nextInactiveIndex;
            handleMapper.isActive = true;

            handleMapper.counter++;
            if (handleMapper.counter == CG_HANDLE_INVALID_COUNTER)
            {
                handleMapper.counter = CG_HANDLE_INVALID_COUNTER + 1;
            }
            handleMapper.dataIndex = m_data.size();

            m_data.push_back(value);
            m_handleIndices.push_back(newIndex);

            return handle_type(*this, getHandleValue(handleMapper.counter, newIndex));
        }

        template <class T>
        template <class... Args>
        typename HandleArray<T>::handle_type HandleArray<T>::insert(Args&&... args)
        {
            // Make sure there's enough room for another handle.
            while (m_handleMappers.size() <= m_nextInactiveIndex)
            {
                auto nextFreeMapper = (handle_index_type)(m_handleMappers.size() + 1);
                m_handleMappers.emplace_back(nextFreeMapper);
            }

            // Get the next free handle entry and the corresponding index.
            const auto newIndex = m_nextInactiveIndex;
            auto&      handleMapper = m_handleMappers[newIndex];
            assert(!handleMapper.isActive);

            // Update mapper connections.
            m_nextInactiveIndex = handleMapper.nextInactiveIndex;
            handleMapper.isActive = true;

            handleMapper.counter++;
            if (handleMapper.counter == CG_HANDLE_INVALID_COUNTER)
            {
                handleMapper.counter++;
            }
            handleMapper.dataIndex = (handle_index_type)m_data.size();

            m_data.emplace_back(std::forward<Args>(args)...);
            m_handleIndices.push_back(newIndex);

            return handle_type(*this, getHandleValue(handleMapper.counter, newIndex));
        }

        template <class T>
        bool HandleArray<T>::remove(const handle_value_type& handleValue)
        {
            const auto index = getHandleIndex(handleValue);
            const auto counter = getHandleCounter(handleValue);
            assert(index < m_handleMappers.size());

            auto& handleMapper = m_handleMappers[index];
            if (counter != handleMapper.counter || !handleMapper.isActive)
            {
                return false;
            }

            handleMapper.nextInactiveIndex = m_nextInactiveIndex;
            m_nextInactiveIndex = index;
            handleMapper.isActive = false;

            if (size() > 1)
            {
                const auto& lastHandleIndex = m_handleIndices.back();
                auto&       lastHandleMapper = m_handleMappers[lastHandleIndex];
                std::swap(m_data[handleMapper.dataIndex], m_data.back());
                std::swap(m_handleIndices[handleMapper.dataIndex], m_handleIndices.back());
                lastHandleMapper.dataIndex = handleMapper.dataIndex;
            }

            m_data.pop_back();
            m_handleIndices.pop_back();
            return true;
        }

        template <class T>
        typename HandleArray<T>::value_type* HandleArray<T>::at(const handle_value_type& handleValue)
        {
            return const_cast<value_type*>(static_cast<const HandleArray<T>*>(this)->at(handleValue));
        }

        template <class T>
        const typename HandleArray<T>::value_type* HandleArray<T>::at(const handle_value_type& handleValue) const
        {
            const auto index = getHandleIndex(handleValue);
            const auto counter = getHandleCounter(handleValue);
            assert(index < m_handleMappers.size());

            const auto& handleMapper = m_handleMappers[index];
            if (handleMapper.counter != counter || !handleMapper.isActive)
            {
                return nullptr;
            }

            assert(handleMapper.dataIndex >= 0 && handleMapper.dataIndex < m_data.size());
            return &m_data[handleMapper.dataIndex];
        }

        template <class T>
        typename HandleArray<T>::value_type* HandleArray<T>::operator[](const handle_value_type& handleValue)
        {
            return at(handleValue);
        }

        template <class T>
        const typename HandleArray<T>::value_type* HandleArray<T>::operator[](
                const handle_value_type& handleValue) const
        {
            return at(handleValue);
        }

        template <class T>
        const typename HandleArray<T>::DataContainer& HandleArray<T>::data() const
        {
            return m_data;
        }

        template <class T>
        typename HandleArray<T>::size_type HandleArray<T>::size() const noexcept
        {
            return (size_type)m_data.size();
        }

        template <class T>
        typename HandleArray<T>::size_type HandleArray<T>::capacity() const noexcept
        {
            return (size_type)m_data.capacity();
        }

        template <class T>
        bool HandleArray<T>::empty() const noexcept
        {
            return m_data.empty();
        }

        template <class T>
        typename HandleArray<T>::iterator HandleArray<T>::begin()
        {
            return m_data.begin();
        }

        template <class T>
        typename HandleArray<T>::iterator HandleArray<T>::end()
        {
            return m_data.end();
        }

        template <class T>
        typename HandleArray<T>::const_iterator HandleArray<T>::begin() const
        {
            return m_data.begin();
        }

        template <class T>
        typename HandleArray<T>::const_iterator HandleArray<T>::end() const
        {
            return m_data.end();
        }

        template <class T>
        typename HandleArray<T>::const_iterator HandleArray<T>::cbegin() const noexcept
        {
            return m_data.cbegin();
        }

        template <class T>
        typename HandleArray<T>::const_iterator HandleArray<T>::cend() const noexcept
        {
            return m_data.cend();
        }

        template <class T>
        inline HandleArrayBase::handle_value_type HandleArray<T>::getHandleValue(
                const handle_counter_type& counter,
                const handle_index_type&   index)
        {
            return ((handle_value_type)counter << (sizeof(handle_value_type) * 4)) + (handle_value_type)index;
        }

        template <class T>
        inline HandleArrayBase::handle_index_type HandleArray<T>::getHandleIndex(const handle_value_type& value)
        {
            return (handle_index_type)((value << (sizeof(handle_value_type) * 4)) >> (sizeof(handle_value_type) * 4));
        }

        template <class T>
        inline HandleArrayBase::handle_counter_type HandleArray<T>::getHandleCounter(const handle_value_type& value)
        {
            return (handle_counter_type)(value >> (sizeof(handle_value_type) * 4));
        }
    }  // namespace entity
}  // namespace cg
