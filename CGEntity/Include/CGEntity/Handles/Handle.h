#pragma once

#include <cassert>
#include <cstdint>

#define CG_HANDLE_INVALID_COUNTER 0

namespace cg
{
    namespace entity
    {
        using HandleValue = uint64_t;
        using HandleIndex = uint32_t;
        using HandleCounter = uint32_t;

        class HandleArrayBase;
        template <class T>
        class HandleArray;

        class HandleBase
        {
        public:
            HandleBase();
            HandleBase(HandleArrayBase& handleArray, const HandleValue& value);
            virtual ~HandleBase() = default;

            const HandleValue& getValue() const;

        protected:
            HandleValue      m_value;
            HandleArrayBase* m_handleArray;
        };

        template <class T>
        class Handle : public HandleBase
        {
        public:
            Handle() = default;
            Handle(const HandleBase& handleBase);
            Handle(HandleArray<T>& handleArray, const HandleValue& value);

            T*   get() const;
            T&   operator*() const;
            T*   operator->() const;
            bool isValid() const;
        };

        template <class T>
        Handle<T>::Handle(const HandleBase& handleBase)
            : HandleBase(handleBase)
        {
        }

        template <class T>
        Handle<T>::Handle(HandleArray<T>& handleArray, const HandleValue& value)
            : HandleBase(handleArray, value)
        {
        }

        template <class T>
        T* Handle<T>::get() const
        {
            return static_cast<HandleArray<T>*>(m_handleArray)->at(m_value);
        }

        template <class T>
        T& Handle<T>::operator*() const
        {
            auto ptr = static_cast<HandleArray<T>*>(m_handleArray)->at(m_value);
            assert(ptr != nullptr);
            return *ptr;
        }

        template <class T>
        T* Handle<T>::operator->() const
        {
            auto ptr = get();
            assert(ptr != nullptr);
            return ptr;
        }

        template <class T>
        bool Handle<T>::isValid() const
        {
            return m_handleArray != nullptr && static_cast<HandleArray<T>*>(m_handleArray)->at(m_value) != nullptr;
        }
    }  // namespace entity
}  // namespace cg
