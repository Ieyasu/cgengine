#pragma once

namespace cg
{
	namespace entity
	{
		/// <summary>
		/// Building block for entities. 
		/// </summary>
		class Component
		{
		public:
			virtual ~Component() = default;
		};
	}
}

