#pragma once

#include <map>
#include <typeindex>
#include <vector>

#include "Handles/Handle.h"
#include "Handles/HandleArray.h"

namespace cg
{
    namespace entity
    {
        class World;

        /// <summary>
        /// Represents an object in a World.
        /// An entity by itself contains no functionality, but is instead built from different custom Components.
        /// </summary>
        class Entity
        {
            friend World;

        public:
            /// <summary>
            /// Creates a new empty Entity.
            /// </summary>
            Entity();

            /// <summary>
            /// Creates a new Entity belonging to a World.
            /// </summary>
            /// <param name="world">The World this Entity belongs to.</param>
            /// <remarks>You should never call this by yourself. Instead use <see
            /// cref="World::createEntity"/>.</remarks>
            explicit Entity(World& world);

            /// <summary>
            /// Destroys the Entity, invalidating all handles pointing to it.
            /// </summary>
            void destroy();

            /// <summary>
            /// Sets whether the Entity is enabled.
            /// </summary>
            /// <param name="enabled>Whether the Entity should be enabled.</param>
            void setEnabled(bool enabled);

            /// <summary>
            /// Adds a new Component with type T to the Entity.
            /// </summary>
            /// <param name="args">The arguments for the Component's constructor.</param>
            /// <returns>Handle to the new Component.</returns>
            template <class T, class... Args>
            Handle<T> addComponent(Args&&... args);

            /// <summary>
            /// Adds a copy of an existing Component with type T to the Entity.
            /// </summary>
            /// <param name="component">The Component to copy.</param>
            /// <returns>Handle to the new Component.</returns>
            template <class T>
            Handle<T> copyComponent(const T& component);

            /// <summary>
            /// Gets a Component of type T from the Entity.
            /// </summary>
            /// <remarks>If there are more than one Component of type T, use <see
            /// cref="Entity::getComponents"/>.</remarks> <returns>Handle to the Component.</returns>
            template <class T>
            Handle<T> getComponent() const;

            /// <summary>
            /// Gets all Components of type T from the Entity.
            /// </summary>
            /// <returns>Handles to the Components.</returns>
            template <class T>
            std::vector<Handle<T>> getComponents() const;

            /// <summary>
            /// Removes a Component of type T from the Entity.
            /// </summary>
            /// <returns>Whether the Component was succesfully removed.</returns>
            /// <remarks>If there are more than one Component of type T, use <see
            /// cref="Entity::removeComponents"/>.</remarks>
            template <class T>
            bool removeComponent();

            /// <summary>
            /// Removes all Components of type T from the Entity.
            /// </summary>
            /// <returns>Number of Components removed.</returns>
            template <class T>
            HandleArrayBase::size_type removeComponents();

            /// <summary>
            /// Gets whether the Entity and all it's Components are enabled.
            /// </summary>
            /// <returns>Whether the Entity is enabled.</returns>
            bool isEnabled() const;

        private:
            using ComponentHandles = std::map<std::type_index, std::vector<HandleBase>>;

            /// <summary>
            /// Whether the Entity and all it's Components are enabled.
            /// </summary>
            bool m_isEnabled;

            /// <summary>
            /// The World this Entity belongs to.
            /// </summary>
            World* m_world;

            /// <summary>
            /// The Handle that maps this Entity to the World's global Entity collection.
            /// </summary>
            Handle<Entity> m_handle;

            /// <summary>
            /// Handles used to access the Entity's own Components in the World's global Component collection.
            /// </summary>
            ComponentHandles m_componentHandles;
        };
    }  // namespace entity
}  // namespace cg
