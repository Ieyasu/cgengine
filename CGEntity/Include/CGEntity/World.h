#pragma once

#include <map>
#include <memory>
#include <typeindex>

#include "Component.h"
#include "Entity.h"
#include "Handles/HandleArray.h"
#include "System.h"

namespace cg
{
    namespace entity
    {
        /// <summary>
        /// Manages all Entities and their Components.
        /// </summary>
        class World
        {
        public:
            using EntityCollection = HandleArray<Entity>;
            using ComponentCollection = std::map<std::type_index, std::unique_ptr<HandleArrayBase>>;
            using SystemCollection = std::vector<std::shared_ptr<System>>;

            /// <summary>
            /// Creates an empty World with no Entities.
            /// </summary>
            World() = default;

            // Copying a World is prohibited
            World(const World& other) = delete;
            World& operator=(const World& other) = delete;
            World(World&& other) = delete;
            World& operator=(World&& other) = delete;

            /// <summary>
            /// Updates the state of the World by one step.
            /// </summary>
            void update();

            /// <summary>
            /// Creates a new Entity.
            /// </summary>
            /// <returns>A Handle pointing to the created Entity.</returns>
            Handle<Entity> createEntity();

            /// <summary>
            /// Creates several Entities at a time.
            /// </summary>
            /// <returns>Handles pointing to the created Entities.</returns>
            /// <remarks>Calling this for creating many Entities is much faster than calling <see
            /// cref="World::createEntity"/> individually.</remarks>
            std::vector<Handle<Entity>> createEntities(size_t amount);

            /// <summary>
            /// Destroys an Entity, invalidating all Handles pointing to it.
            /// </summary>
            /// <param name="handle">The Entity to destroy.</param>
            void destroyEntity(const Entity& entity);

            /// <summary>
            /// Enables an Entity and all it's Components.
            /// </summary>
            /// <param name="handle">The Entity to enable.</param>
            void enableEntity(Entity& entity);

            /// <summary>
            /// Disables an Entity and all it's Components.
            /// </summary>
            /// <param name="handle">The Entity to disable.</param>
            void disableEntity(Entity& entity);

            /// <summary>
            /// Gets all Entities of the World.
            /// </summary>
            /// <returns>The collection of Entities in the World.</returns>
            const EntityCollection& getAllEntities() const;

            /// <summary>
            /// Adds a new System to the World.
            /// </summary>
            /// <param name="system">The System to add.</param>
            /// <returns>Whether the System was added to the world succesfully.</returns>
            bool addSystem(std::shared_ptr<System> system);

            /// <summary>
            /// Removes a System from the World.
            /// </summary>
            /// <param name="system">The System to remove.</param>
            /// <returns>Whether the System was succesfully removed.</returns>
            bool removeSystem(std::shared_ptr<System> system);

            /// <summary>
            /// Adds a new Component with type T to the given Entity.
            /// </summary>
            /// <param name="entity">The Entity to add the Component to.</param>
            /// <param name="args">The arguments for the Component's constructor.</param>
            /// <returns>Handle to the new Component.</returns>
            template <class T, class... Args>
            Handle<T> addComponent(Entity& entity, Args&&... args);

            /// <summary>
            /// Adds a copy of an existing Component with type T to the given Entity.
            /// </summary>
            /// <param name="entity">The Entity to add the Component to.</param>
            /// <param name="component">The Component to copy.</param>
            /// <returns>Handle to the new Component.</returns>
            template <class T>
            Handle<T> copyComponent(Entity& entity, const T& component);

            /// <summary>
            /// Removes a Component of type T from the given Entity.
            /// </summary>
            /// <param name="entity">The Entity to remove the Component from.</param>
            /// <returns>Whether the Component was succesfully removed.</returns>
            /// <remarks>If there are more than one Component of type T, use <see
            /// cref="World::removeComponents"/>.</remarks>
            template <class T>
            bool removeComponent(Entity& entity);

            /// <summary>
            /// Removes all Components of type T from the given Entity.
            /// </summary>
            /// <param name="entity">The Entity to remove the Components from.</param>
            /// <returns>Number of Components removed.</returns>
            template <class T>
            HandleArrayBase::size_type removeComponents(Entity& entity);

            /// <summary>
            /// Gets all Components of Type T from all Entities.
            /// </summary>
            /// <returns>An array containing the Components.</returns>
            template <class T>
            const HandleArray<T>* getAllComponentsOfType() const;

        private:
            /// <summary>
            /// All entities are stored in a single datastructure.
            /// </summary>
            EntityCollection m_entities;

            /// <summary>
            /// A common use case is to access all Components of a certain type.
            /// Components of same type are organized together into arrays.
            /// </summary>
            ComponentCollection m_componentsByType;

            /// <summary>
            /// Contains all the Systems active in the World.
            /// </summary>
            SystemCollection m_systems;

            /// <summary>
            /// Gets all Components of Type T from all Entities.
            /// If an array for the Components of type T doesn't already exist, creates it.
            /// </summary>
            /// <returns>An array containing the Components.</returns>
            template <class T>
            HandleArray<T>& getOrCreateAllComponentsOfType();
        };

        template <class T, class... Args>
        Handle<T> World::addComponent(Entity& entity, Args&&... args)
        {
            static_assert(
                    std::is_base_of<Component, T>::value,
                    "Template parameter of addComponent<T> must have cg::entity::Component as it's base class.");

            static const auto typeIndex = std::type_index(typeid(T));

            // Create a new Component.
            auto& allComponentsOfTypeT = getOrCreateAllComponentsOfType<T>();
            auto  newComponentHandle = allComponentsOfTypeT.insert(T(std::forward<Args>(args)...));

            // Add it's handle to the Entity.
            auto& entitysComponentsOfTypeT = entity.m_componentHandles[typeIndex];
            entitysComponentsOfTypeT.push_back(newComponentHandle);

            return newComponentHandle;
        }

        template <class T>
        Handle<T> World::copyComponent(Entity& entity, const T& componentToCopy)
        {
            static_assert(
                    std::is_base_of<Component, T>::value,
                    "Template parameter of copyComponent<T> must have cg::entity::Component as it's base class.");

            static const auto typeIndex = std::type_index(typeid(T));

            // Create a new Component.
            auto& allComponentsOfTypeT = getOrCreateAllComponentsOfType<T>();
            auto  newComponentHandle = allComponentsOfTypeT.insert(T(componentToCopy));

            // Add it's handle to the Entity.
            auto& entitysComponentsOfTypeT = entity.m_componentHandles[typeIndex];
            entitysComponentsOfTypeT.push_back(newComponentHandle);

            return newComponentHandle;
        }

        template <class T>
        bool World::removeComponent(Entity& entity)
        {
            static_assert(
                    std::is_base_of<Component, T>::value,
                    "Template parameter of removeComponent<T> must have cg::entity::Component as it's base class.");

            static const auto typeIndex = std::type_index(typeid(T));

            // Make sure Entity has a Component of type T.
            auto it = entity.m_componentHandles.find(typeIndex);
            if (it == entity.m_componentHandles.end() || it->second.size() == 0)
            {
                return false;
            }

            // Remove Component from global collection...
            auto& allComponentsOfTypeT = getOrCreateAllComponentsOfType<T>();
            auto& entitysComponentsOfTypeT = it->second;
            bool  removeSuccesful = allComponentsOfTypeT.remove(entitysComponentsOfTypeT.back());

            // ...and corresponding handle from Entity's handle collection.
            entitysComponentsOfTypeT.pop_back();

            return removeSuccesful;
        }

        template <class T>
        HandleArrayBase::size_type World::removeComponents(Entity& entity)
        {
            static_assert(
                    std::is_base_of<Component, T>::value,
                    "Template parameter of removeComponents<T> must have cg::entity::Component as it's base class.");

            static const auto typeIndex = std::type_index(typeid(T));

            // Make sure Entity has a Component of type T.
            auto it = entity.m_componentHandles.find(typeIndex);
            if (it == entity.m_componentHandles.end() || it->second.size() == 0)
            {
                return 0;
            }

            // Remove Components from global collection...
            auto  componentsRemoved = 0u;
            auto& allComponentsOfTypeT = getOrCreateAllComponentsOfType<T>();
            auto& entitysComponentsOfTypeT = it->second;

            for (const auto& handle : entitysComponentsOfTypeT)
            {
                if (allComponentsOfTypeT.remove(handle))
                {
                    componentsRemoved++;
                }
            }

            // ...and corresponding handles from Entity's handle collection.
            entity.m_componentHandles.erase(it);

            return componentsRemoved;
        }

        template <class T>
        const HandleArray<T>* World::getAllComponentsOfType() const
        {
            static const auto typeIndex = std::type_index(typeid(T));

            auto it = m_componentsByType.find(typeIndex);
            if (it == m_componentsByType.end())
            {
                // Couldn't find Components of type T.
                return nullptr;
            }

            auto allComponentsOfTypeT = static_cast<HandleArray<T>*>(it->second.get());
            return allComponentsOfTypeT;
        }

        template <class T>
        HandleArray<T>& World::getOrCreateAllComponentsOfType()
        {
            static_assert(
                    std::is_base_of<Component, T>::value,
                    "Template parameter of getAllComponentsOfType<T> must have cg::entity::Component as it's base "
                    "class.");

            static const auto typeIndex = std::type_index(typeid(T));

            auto it = m_componentsByType.find(typeIndex);
            if (it == m_componentsByType.end())
            {
                // Create a new array to hold Components of type T and return it.
                m_componentsByType[typeIndex] = std::make_unique<HandleArray<T>>();
                return *static_cast<HandleArray<T>*>(m_componentsByType[typeIndex].get());
            }

            // Return existing.
            return *static_cast<HandleArray<T>*>(it->second.get());
        }

        template <class T, class... Args>
        Handle<T> Entity::addComponent(Args&&... args)
        {
            return this->m_world->addComponent<T>(*this, std::forward<Args>(args)...);
        }

        template <class T>
        Handle<T> Entity::copyComponent(const T& component)
        {
            return this->m_world->copyComponent<T>(*this, component);
        }

        template <class T>
        bool Entity::removeComponent()
        {
            return this->m_world->removeComponent<T>(*this);
        }

        template <class T>
        HandleArrayBase::size_type Entity::removeComponents()
        {
            return this->m_world->removeComponents<T>(*this);
        }

        template <class T>
        Handle<T> Entity::getComponent() const
        {
            static_assert(
                    std::is_base_of<Component, T>::value,
                    "Template parameter of getComponent<T> must have cg::entity::Component as it's base class.");

            static const auto typeIndex = std::type_index(typeid(T));

            // Make sure the Entity has a Component of type T.
            auto it = m_componentHandles.find(typeIndex);
            if (it == m_componentHandles.end() || it->second.size() == 0)
            {
                return Handle<T>();
            }

            return it->second.front();
        }

        template <class T>
        std::vector<Handle<T>> Entity::getComponents() const
        {
            static const auto typeIndex = std::type_index(typeid(T));

            // Make sure the Entity has a Component of type T.
            auto it = m_componentHandles.find(typeIndex);
            if (it == m_componentHandles.end() || it->second.size() == 0)
            {
                return std::vector<Handle<T>>();
            }

            return std::vector<Handle<T>>();
        }
    }  // namespace entity
}  // namespace cg
