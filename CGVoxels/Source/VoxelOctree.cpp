#include "CGVoxels/VoxelOctree.h"

#include <algorithm>

#include "CGMath/Common.h"
#include "CGMath/IntersectionTesting.h"

namespace cg
{
    using math::clampToPowerOfTwo;

    namespace voxel
    {
        VoxelOctree::VoxelOctree(uint16_t size, float voxelSize, const Vector3f& origin)
            : m_root(0)
            , m_voxelSize(voxelSize)
            , m_normalNodeCount(0)
            , m_leafNodeCount(0)
            , m_voxelCount(0)
        {
            m_size = clampToPowerOfTwo(size);
            if (m_size < VOXEL_OCTREE_BRICK_SIZE)
            {
                m_size = VOXEL_OCTREE_BRICK_SIZE;
            }
            m_bounds = BoundsF(origin, origin + Vector3f(m_size * voxelSize));
        }

        VoxelOctree::VoxelOctree(const VoxelOctree& other)
            : m_root(other.m_root)
            , m_size(other.m_size)
            , m_voxelSize(other.m_voxelSize)
            , m_bounds(other.m_bounds)
            , m_normalNodeCount(other.m_normalNodeCount)
            , m_leafNodeCount(other.m_leafNodeCount)
            , m_voxelCount(other.m_voxelCount)
        {
            // TODO copy structure
        }

        VoxelOctree::VoxelOctree(VoxelOctree&& other)
            : m_root(other.m_root)
            , m_size(other.m_size)
            , m_voxelSize(other.m_voxelSize)
            , m_bounds(other.m_bounds)
            , m_normalNodeCount(other.m_normalNodeCount)
            , m_leafNodeCount(other.m_leafNodeCount)
            , m_voxelCount(other.m_voxelCount)
        {
            other.m_root = nullptr;
        }

        VoxelOctree::~VoxelOctree()
        {
            cleanUp(m_root);
            m_root = 0;
        }

        VoxelOctree& VoxelOctree::operator=(const VoxelOctree& other)
        {
            VoxelOctree tmp(other);
            *this = std::move(tmp);
            return *this;
        }

        VoxelOctree& VoxelOctree::operator=(VoxelOctree&& other)
        {
            m_root = other.m_root;
            m_size = other.m_size;
            m_voxelSize = other.m_voxelSize;
            m_bounds = other.m_bounds;
            m_normalNodeCount = other.m_normalNodeCount;
            m_leafNodeCount = other.m_leafNodeCount;
            m_voxelCount = other.m_voxelCount;
            other.m_root = nullptr;
            return *this;
        }

        void VoxelOctree::cleanUp(const Node& node)
        {
            // Clean up children
            if (isNormalNode(node))
            {
                intptr_t* children = (intptr_t*)(node + 1);
                for (uint8_t i = 0; i < 8; ++i)
                {
                    cleanUp((Node)children[i]);
                }
                --m_normalNodeCount;
            }
            else if (isLeafNode(node))
            {
                --m_leafNodeCount;
            }

            // Clean up self
            free(node);
        }

        bool VoxelOctree::insert(uint16_t x, uint16_t y, uint16_t z, VoxelValue value)
        {
            Coordinate coord = Coordinate(x, y, z);
            return insert_internal(m_root, m_size, coord, value);
        }

        VoxelOctree::VoxelValue VoxelOctree::get(uint16_t x, uint16_t y, uint16_t z) const
        {
            Coordinate coord = Coordinate(x, y, z);
            return get_internal(m_root, m_size, coord);
        }

        bool VoxelOctree::insert_internal(Node& node, uint16_t size, Coordinate& coord, VoxelValue value)
        {
            if (isEmptyNode(node))
            {
                if (size > VOXEL_OCTREE_BRICK_SIZE)
                {
                    node = allocNormalNode();
                    ++m_normalNodeCount;
                }
                else
                {
                    node = allocLeafNode();
                    ++m_leafNodeCount;
                }
                return insert_internal(node, size, coord, value);
            }
            else if (isNormalNode(node))
            {
                // Insert voxel in a child
                uint16_t childSize = size / 2;
                Node*    childNode = getChild(node, childSize, coord);
                if (!childNode)
                {
                    return false;
                }
                return insert_internal(*childNode, childSize, coord, value);
            }
            else if (isLeafNode(node))
            {
                if (setVoxelValue(node, coord, value))
                {
                    return false;
                }
                m_voxelCount++;
                return true;
            }
            return false;
        }

        VoxelOctree::VoxelValue VoxelOctree::get_internal(const Node& node, uint16_t size, Coordinate& coord) const
        {
            if (isNormalNode(node))
            {
                uint16_t childSize = size / 2;
                Node*    childNode = getChild(node, childSize, coord);
                if (!childNode)
                {
                    return 0;
                }
                return get_internal(*childNode, childSize, coord);
            }
            else if (isLeafNode(node))
            {
                return getVoxelValue(node, coord);
            }
            return 0;
        }

        VoxelOctree::Node* VoxelOctree::getChild(const Node& node, uint16_t halfSize, Coordinate& coord) const
        {
            if (!isNormalNode(node))
            {
                return nullptr;
            }

            uint8_t childIndex = 0;
            // Least significant bit tells the z location
            if (coord.x >= halfSize)
            {
                coord.x -= halfSize;
                childIndex |= 4;
            }
            // Second least significant bit tells the y location
            if (coord.y >= halfSize)
            {
                coord.y -= halfSize;
                childIndex |= 2;
            }
            // Third least significant bit tells the x location
            if (coord.z >= halfSize)
            {
                coord.z -= halfSize;
                childIndex |= 1;
            }

            intptr_t* children = (intptr_t*)(node + 1);
            return (Node*)(children + childIndex);
        }

        VoxelOctree::Node VoxelOctree::allocNormalNode()
        {
            Node node = (Node)calloc(getNormalNodeMemory(), sizeof(uint8_t));
            // Mark type as normal node.
            node[0] = 0;
            return node;
        }

        VoxelOctree::Node VoxelOctree::allocLeafNode()
        {
            Node node = (Node)calloc(getLeafNodeMemory(), sizeof(uint8_t));
            // Mark type as leaf.
            node[0] = 1;
            return node;
        }

        uint16_t VoxelOctree::getSize() const noexcept
        {
            return m_size;
        }

        float VoxelOctree::getVoxelSize() const noexcept
        {
            return m_voxelSize;
        }

        const BoundsF& VoxelOctree::getBounds() const noexcept
        {
            return m_bounds;
        }

        uint64_t VoxelOctree::getVoxelCount() const noexcept
        {
            return m_voxelCount;
        }

        size_t VoxelOctree::getAllocatedMemory() const noexcept
        {
            return (size_t)m_normalNodeCount * getNormalNodeMemory() + (size_t)m_leafNodeCount * getLeafNodeMemory();
        }

        uint64_t VoxelOctree::getNormalNodeCount() const noexcept
        {
            return m_normalNodeCount;
        }

        uint64_t VoxelOctree::getLeafNodeCount() const noexcept
        {
            return m_leafNodeCount;
        }

        size_t VoxelOctree::getNormalNodeMemory() const noexcept
        {
            return 8 * sizeof(intptr_t) + 1;
        }

        size_t VoxelOctree::getLeafNodeMemory() const noexcept
        {
            return (VOXEL_OCTREE_BRICK_SIZE * VOXEL_OCTREE_BRICK_SIZE * VOXEL_OCTREE_BRICK_SIZE) * sizeof(VoxelValue)
                 + 1;
        }

        const VoxelOctree::Node& VoxelOctree::getRoot() const noexcept
        {
            return m_root;
        }

        bool VoxelOctree::isEmptyNode(const Node& node)
        {
            return !node;
        }

        bool VoxelOctree::isNormalNode(const Node& node)
        {
            return node && node[0] == 0;
        }

        bool VoxelOctree::isLeafNode(const Node& node)
        {
            return node && node[0] == 1;
        }

        inline bool VoxelOctree::setVoxelValue(const Node& node, const Coordinate& coord, VoxelValue value)
        {
            const auto index = ((coord.z * VOXEL_OCTREE_BRICK_SIZE + coord.y) * VOXEL_OCTREE_BRICK_SIZE + coord.x);
            auto&      currentValue = ((VoxelValue*)(node + 1))[index];
            if (currentValue != 0)
            {
                return false;
            }
            currentValue = value;
            return true;
        }

        inline VoxelOctree::VoxelValue VoxelOctree::getVoxelValue(const Node& node, const Coordinate& coord) const
        {
            const auto index = ((coord.z * VOXEL_OCTREE_BRICK_SIZE + coord.y) * VOXEL_OCTREE_BRICK_SIZE + coord.x);
            const auto currentValue = ((VoxelValue*)(node + 1))[index];
            return currentValue != 0;
        }
    }  // namespace voxel
}  // namespace cg
