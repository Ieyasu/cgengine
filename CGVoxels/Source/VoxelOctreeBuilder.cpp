#include "CGMath/IntersectionTesting.h"
#include "CGVoxels/VoxelOctree.h"

namespace cg
{
    using math::triBoxOverlap;

    namespace voxel
    {
        inline void findMinMax(float x0, float x1, float x2, float& min, float& max)
        {
            min = max = x0;
            if (x1 < min)
                min = x1;
            if (x1 > max)
                max = x1;
            if (x2 < min)
                min = x2;
            if (x2 > max)
                max = x2;
        }

        VoxelOctree::BuildConfig::BuildConfig()
            : m_resolution(VOXEL_OCTREE_BRICK_SIZE)
        {
        }

        VoxelOctree::BuildConfig::BuildConfig(const TriangleMesh* mesh, unsigned short resolution)
            : m_resolution(resolution)
        {
            m_meshes.push_back(mesh);
        }

        VoxelOctree::BuildConfig::BuildConfig(const std::vector<const TriangleMesh*>& meshes, unsigned short resolution)
            : m_meshes(meshes)
            , m_resolution(resolution)
        {
        }

        const std::vector<const TriangleMesh*>& VoxelOctree::BuildConfig::getMeshes() const
        {
            return m_meshes;
        }

        unsigned short VoxelOctree::BuildConfig::getResolution() const noexcept
        {
            return m_resolution;
        }

        std::unique_ptr<VoxelOctree> VoxelOctree::Builder::build(const BuildConfig& buildConfig) const
        {
            const auto& meshes = buildConfig.getMeshes();
            if (meshes.empty() || !meshes.front())
            {
                return std::unique_ptr<VoxelOctree>(nullptr);
            }

            auto bounds = meshes.front()->getBounds();

            for (auto mesh : meshes)
            {
                if (!mesh)
                {
                    return std::unique_ptr<VoxelOctree>(nullptr);
                }
                bounds.encapsulate(mesh->getBounds());
            }

            const auto voxelSize = (bounds.getMax() - bounds.getMin()).max() / buildConfig.getResolution();
            // Create octree to hold the voxels
            auto octree = std::make_unique<VoxelOctree>(buildConfig.getResolution(), voxelSize, bounds.getMin());

            for (const auto& mesh : meshes)
            {
                for (size_t i = 0; i < mesh->getSubMeshCount(); ++i)
                {
                    auto                    color = mesh->getMaterial(i).getUniformVector4("_MainColor") * 255;
                    VoxelOctree::VoxelValue voxelValue = (((uint8_t)color.x) << 24) | (((uint8_t)color.y) << 16)
                                                       | (((uint8_t)color.z) << 8) | (uint8_t)color.w;
                    addGeometry(octree, mesh->getPositions(), mesh->getTriangles(i), voxelValue);
                }
            }

            return octree;
        }

        void VoxelOctree::Builder::addGeometry(
                std::unique_ptr<VoxelOctree>& octree,
                const std::vector<Vector3f>&  vertices,
                const std::vector<Triangle>&  triangles,
                VoxelOctree::VoxelValue       voxelValue) const
        {
            const auto& origin = octree->getBounds().getMin();
            const auto  octreeSize = octree->getSize();
            const auto  voxelSize = octree->getVoxelSize();
            const auto  inverseGridSize = 1.0f / (octreeSize * voxelSize);
            // Get the voxel half size for triangle - AABB intersection testing
            const auto halfSize = Vector3f(voxelSize / 2.0f);

            Vector3f bmin;
            Vector3f bmax;
            Vector3i iterationStart;
            Vector3i iterationEnd;
            Vector3f voxelCenter;

            // Voxelize
            for (auto it = triangles.begin(); it != triangles.end(); ++it)
            {
                const auto& triangle = (*it);
                const auto& vertex1 = vertices[triangle.x];
                const auto& vertex2 = vertices[triangle.y];
                const auto& vertex3 = vertices[triangle.z];

                // Get the bounds of the triangle
                findMinMax(vertex1.x, vertex2.x, vertex3.x, bmin.x, bmax.x);
                findMinMax(vertex1.y, vertex2.y, vertex3.y, bmin.y, bmax.y);
                findMinMax(vertex1.z, vertex2.z, vertex3.z, bmin.z, bmax.z);

                // Get the iteration start pos in the grid
                iterationStart.x = (int)(octreeSize * (bmin.x - origin.x) * inverseGridSize);
                iterationStart.y = (int)(octreeSize * (bmin.y - origin.y) * inverseGridSize);
                iterationStart.z = (int)(octreeSize * (bmin.z - origin.z) * inverseGridSize);

                // Clamp start position
                if (iterationStart.x < 0)
                    iterationStart.x = 0;
                if (iterationStart.y < 0)
                    iterationStart.y = 0;
                if (iterationStart.z < 0)
                    iterationStart.z = 0;

                // Get the iteration end pos in the grid
                iterationEnd.x = (int)(octreeSize * (bmax.x - origin.x) * inverseGridSize);
                iterationEnd.y = (int)(octreeSize * (bmax.y - origin.y) * inverseGridSize);
                iterationEnd.z = (int)(octreeSize * (bmax.z - origin.z) * inverseGridSize);

                // Clamp end position
                if (iterationEnd.x >= octreeSize)
                    iterationEnd.x = octreeSize - 1;
                if (iterationEnd.y >= octreeSize)
                    iterationEnd.y = octreeSize - 1;
                if (iterationEnd.z >= octreeSize)
                    iterationEnd.z = octreeSize - 1;

                voxelCenter.z = origin.z + (iterationStart.z + 0.5f) * voxelSize;
                const float voxelY = origin.y + (iterationStart.y + 0.5f) * voxelSize;
                const float voxelX = origin.x + (iterationStart.x + 0.5f) * voxelSize;
                for (int z = iterationStart.z; z <= iterationEnd.z; ++z, voxelCenter.z += voxelSize)
                {
                    voxelCenter.y = voxelY;
                    for (int y = iterationStart.y; y <= iterationEnd.y; ++y, voxelCenter.y += voxelSize)
                    {
                        voxelCenter.x = voxelX;
                        for (int x = iterationStart.x; x <= iterationEnd.x; ++x, voxelCenter.x += voxelSize)
                        {
                            if (triBoxOverlap(
                                        voxelCenter.getData(),
                                        halfSize.getData(),
                                        vertex1.getData(),
                                        vertex2.getData(),
                                        vertex3.getData()))
                            {
                                octree->insert((uint16_t)x, (uint16_t)y, (uint16_t)z, voxelValue);
                            }
                        }
                    }
                }
            }
        }
    }  // namespace voxel
}  // namespace cg
