#include <limits>

#include "CGVoxels/VoxelOctree.h"

namespace cg
{
    namespace voxel
    {
        inline float frac(float a)
        {
            return a < 0 ? (a + (int)a) : (a - (int)a);
        }

        template <class T>
        inline T min(T a, T b, T c)
        {
            if (a < b)
            {
                if (a < c)
                    return a;
                return c;
            }
            else if (b < c)
                return b;
            return c;
        }

        template <class T>
        inline T max(T a, T b, T c)
        {
            if (a > b)
            {
                if (a > c)
                    return a;
                return c;
            }
            else if (b > c)
                return b;
            return c;
        }

        bool VoxelOctree::Raytracer::raytrace(const VoxelOctree& octree, const Ray& r, RayHit& rayHit) const
        {
            return raytrace(octree, r, rayHit, std::numeric_limits<float>::infinity());
        }

        bool VoxelOctree::Raytracer::raytrace(
                const VoxelOctree& octree,
                const Ray&         r,
                RayHit&            rayHit,
                float              maxDistance) const
        {
            const auto& octreeBounds = octree.getBounds();
            const auto& octreeOrigin = octreeBounds.getMin();
            const auto  octreeSize = octree.getSize();
            const float inverseVoxelSize = 1.0f / octree.getVoxelSize();

            // Shift ray to a coordinate system where the octree's origin is at (0, 0, 0) and the voxel size is 1.
            // This will simplify calculations  when we reach a leaf node and have to do a regular grid traversal.
            auto ray = Ray((r.origin - octreeOrigin) * inverseVoxelSize, r.direction);

            // Change directions of ray to positive and switch origins positions accordingly.
            unsigned char a = 0;
            for (int i = 0; i < 3; ++i)
            {
                if (ray.direction[i] < -0.00001f)
                {
                    a |= (4 >> i);

                    ray.direction[i] = -ray.direction[i];
                    ray.origin[i] = octreeSize - ray.origin[i];
                }
                else if (ray.direction[i] < 0.00001f)
                {
                    ray.direction[i] = 0.00001f;
                }
            }

            // Calculate the min and max t-values of the ray in respect to the octree's bounding box.
            const float rayDirInverseX = 1.0f / ray.direction.x;
            const float rayDirInverseY = 1.0f / ray.direction.y;
            const float rayDirInverseZ = 1.0f / ray.direction.z;
            const float tx0 = -ray.origin.x * rayDirInverseX;
            const float ty0 = -ray.origin.y * rayDirInverseY;
            const float tz0 = -ray.origin.z * rayDirInverseZ;
            const float tx1 = (octreeSize - ray.origin.x) * rayDirInverseX;
            const float ty1 = (octreeSize - ray.origin.y) * rayDirInverseY;
            const float tz1 = (octreeSize - ray.origin.z) * rayDirInverseZ;

            // Ray misses octree.
            if (max(tx0, ty0, tz0) > min(tx1, ty1, tz1))
            {
                return false;
            }

            // Scale maxDistance to the simple coordinate system.
            maxDistance = maxDistance * inverseVoxelSize;

            // Traverse the tree.
            if (raytrace_internal(ray, rayHit, octree.getRoot(), maxDistance, tx0, ty0, tz0, tx1, ty1, tz1, a))
            {
                rayHit.t /= inverseVoxelSize;
                return true;
            }

            return false;
        }

        bool VoxelOctree::Raytracer::raytrace_internal(
                const Ray&               ray,
                RayHit&                  rayHit,
                const VoxelOctree::Node& node,
                float                    maxDistance,
                float                    tx0,
                float                    ty0,
                float                    tz0,
                float                    tx1,
                float                    ty1,
                float                    tz1,
                unsigned char            a) const
        {
            if (VoxelOctree::isEmptyNode(node))
            {
                return false;
            }

            if (tx1 < 0 || ty1 < 0 || tz1 < 0)
            {
                return false;
            }

            if (VoxelOctree::isLeafNode(node))
            {
                return raytrace_leaf_internal(ray, rayHit, node, maxDistance, tx0, ty0, tz0, a);
            }

            float txm = 0.5f * (tx0 + tx1);
            float tym = 0.5f * (ty0 + ty1);
            float tzm = 0.5f * (tz0 + tz1);

            auto children = (VoxelOctree::Node*)(node + 1);
            int  currentNode = getFirstNode(tx0, ty0, tz0, txm, tym, tzm);
            do
            {
                switch (currentNode)
                {
                    case 0:
                        if (raytrace_internal(ray, rayHit, children[a], maxDistance, tx0, ty0, tz0, txm, tym, tzm, a))
                        {
                            return true;
                        }
                        currentNode = getNewNode(4, 2, 1, txm, tym, tzm);
                        break;
                    case 1:
                        if (raytrace_internal(
                                    ray,
                                    rayHit,
                                    children[1 ^ a],
                                    maxDistance,
                                    tx0,
                                    ty0,
                                    tzm,
                                    txm,
                                    tym,
                                    tz1,
                                    a))
                        {
                            return true;
                        }
                        currentNode = getNewNode(5, 3, 8, txm, tym, tz1);
                        break;
                    case 2:
                        if (raytrace_internal(
                                    ray,
                                    rayHit,
                                    children[2 ^ a],
                                    maxDistance,
                                    tx0,
                                    tym,
                                    tz0,
                                    txm,
                                    ty1,
                                    tzm,
                                    a))
                        {
                            return true;
                        }
                        currentNode = getNewNode(6, 8, 3, txm, ty1, tzm);
                        break;
                    case 3:
                        if (raytrace_internal(
                                    ray,
                                    rayHit,
                                    children[3 ^ a],
                                    maxDistance,
                                    tx0,
                                    tym,
                                    tzm,
                                    txm,
                                    ty1,
                                    tz1,
                                    a))
                        {
                            return true;
                        }
                        currentNode = getNewNode(7, 8, 8, txm, ty1, tz1);
                        break;
                    case 4:
                        if (raytrace_internal(
                                    ray,
                                    rayHit,
                                    children[4 ^ a],
                                    maxDistance,
                                    txm,
                                    ty0,
                                    tz0,
                                    tx1,
                                    tym,
                                    tzm,
                                    a))
                        {
                            return true;
                        }
                        currentNode = getNewNode(8, 6, 5, tx1, tym, tzm);
                        break;
                    case 5:
                        if (raytrace_internal(
                                    ray,
                                    rayHit,
                                    children[5 ^ a],
                                    maxDistance,
                                    txm,
                                    ty0,
                                    tzm,
                                    tx1,
                                    tym,
                                    tz1,
                                    a))
                        {
                            return true;
                        }
                        currentNode = getNewNode(8, 7, 8, tx1, tym, tz1);
                        break;
                    case 6:
                        if (raytrace_internal(
                                    ray,
                                    rayHit,
                                    children[6 ^ a],
                                    maxDistance,
                                    txm,
                                    tym,
                                    tz0,
                                    tx1,
                                    ty1,
                                    tzm,
                                    a))
                        {
                            return true;
                        }
                        currentNode = getNewNode(8, 8, 7, tx1, ty1, tzm);
                        break;
                    case 7:
                        if (raytrace_internal(
                                    ray,
                                    rayHit,
                                    children[7 ^ a],
                                    maxDistance,
                                    txm,
                                    tym,
                                    tzm,
                                    tx1,
                                    ty1,
                                    tz1,
                                    a))
                        {
                            return true;
                        }
                        currentNode = 8;
                        break;
                }
            } while (currentNode < 8);

            return false;
        }

        uint8_t VoxelOctree::Raytracer::getNewNode(
                unsigned char x,
                unsigned char y,
                unsigned char z,
                float         txm,
                float         tym,
                float         tzm) const
        {
            if (txm < tym)
            {
                if (txm < tzm)
                    return x;
                else
                    return z;
            }
            else if (tym < tzm)
                return y;
            return z;
        }

        uint8_t VoxelOctree::Raytracer::getFirstNode(float tx0, float ty0, float tz0, float txm, float tym, float tzm)
                const
        {
            unsigned char first = 0;

            if (tx0 > ty0)
            {
                if (tx0 > tz0)
                {
                    // Entry plane YZ
                    if (tym < tx0)
                        first |= 2;
                    if (tzm < tx0)
                        first |= 1;
                    return first;
                }
            }
            else if (ty0 > tz0)
            {
                // Entry plane XZ
                if (txm < ty0)
                    first |= 4;
                if (tzm < ty0)
                    first |= 1;
                return first;
            }

            // Entry plane XY
            if (txm < tz0)
                first |= 4;
            if (tym < tz0)
                first |= 2;
            return first;
        }

        bool VoxelOctree::Raytracer::raytrace_leaf_internal(
                const Ray&               ray,
                RayHit&                  rayHit,
                const VoxelOctree::Node& node,
                float                    maxDistance,
                float                    tx0,
                float                    ty0,
                float                    tz0,
                unsigned char            a) const
        {
            const float tmin = max(tx0, ty0, tz0) + 0.001f;

            const float startPosX = ray.origin.x + ray.direction.x * tmin;
            const float startPosY = ray.origin.y + ray.direction.y * tmin;
            const float startPosZ = ray.origin.z + ray.direction.z * tmin;

            const float tDeltaX = 1.0f / ray.direction.x;
            const float tDeltaY = 1.0f / ray.direction.y;
            const float tDeltaZ = 1.0f / ray.direction.z;

            float tMaxX = (1.0f - frac(startPosX)) * tDeltaX;
            float tMaxY = (1.0f - frac(startPosY)) * tDeltaY;
            float tMaxZ = (1.0f - frac(startPosZ)) * tDeltaZ;

            int  xIncrement = 1;
            int  yIncrement = 1;
            int  zIncrement = 1;
            auto endX = VOXEL_OCTREE_BRICK_SIZE;
            auto endY = VOXEL_OCTREE_BRICK_SIZE;
            auto endZ = VOXEL_OCTREE_BRICK_SIZE;
            int  x = (int)(startPosX - (ray.origin.x + ray.direction.x * tx0));
            int  y = (int)(startPosY - (ray.origin.y + ray.direction.y * ty0));
            int  z = (int)(startPosZ - (ray.origin.z + ray.direction.z * tz0));

            if (a & 4)
            {
                x = (VOXEL_OCTREE_BRICK_SIZE - 1) - x;
                xIncrement = -1;
                endX = -1;
            }

            if (a & 2)
            {
                y = (VOXEL_OCTREE_BRICK_SIZE - 1) - y;
                yIncrement = -1;
                endY = -1;
            }

            if (a & 1)
            {
                z = (VOXEL_OCTREE_BRICK_SIZE - 1) - z;
                zIncrement = -1;
                endZ = -1;
            }

            if (x < 0 || x >= VOXEL_OCTREE_BRICK_SIZE || y < 0 || y >= VOXEL_OCTREE_BRICK_SIZE || z < 0
                || z >= VOXEL_OCTREE_BRICK_SIZE)
            {
                return false;
            }

            while (true)
            {
                const auto index = ((z * VOXEL_OCTREE_BRICK_SIZE + y) * VOXEL_OCTREE_BRICK_SIZE + x);
                const auto currentValue = ((uint32_t*)(node + 1))[index];

                if (currentValue != 0)
                {
                    const float tHit = tmin + min(tMaxX, tMaxY, tMaxZ);
                    if (tHit > maxDistance)
                    {
                        return false;
                    }
                    rayHit.t = tHit;
                    rayHit.color.x = (uint8_t)(currentValue >> 24);
                    rayHit.color.y = (uint8_t)(currentValue >> 16);
                    rayHit.color.z = (uint8_t)(currentValue >> 8);
                    rayHit.color.w = (uint8_t)currentValue;
                    return true;
                }

                if (tMaxX < tMaxY)
                {
                    if (tMaxX < tMaxZ)
                    {
                        x += xIncrement;
                        tMaxX += tDeltaX;
                        if (x == endX)
                            return false;
                    }
                    else
                    {
                        z += zIncrement;
                        tMaxZ += tDeltaZ;
                        if (z == endZ)
                            return false;
                    }
                }
                else if (tMaxY < tMaxZ)
                {
                    y += yIncrement;
                    tMaxY += tDeltaY;
                    if (y == endY)
                        return false;
                }
                else
                {
                    z += zIncrement;
                    tMaxZ += tDeltaZ;
                    if (z == endZ)
                        return false;
                }
            }
        }
    }  // namespace voxel
}  // namespace cg
