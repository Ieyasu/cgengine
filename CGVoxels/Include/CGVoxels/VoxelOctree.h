#pragma once

#include <cstdint>
#include "CGMath/Vector3.h"
#include "CGMath/Bounds.h"
#include "CGCore/Mesh/TriangleMesh.h"
#include "CGCore/Raytracing/AccelerationStructure.h"

namespace cg
{
	using math::Vector3;
	using math::Vector3i;
	using math::Vector3f;
	using math::BoundsF;

	using core::Ray;
	using core::RayHit;
	using core::TriangleMesh;
	using core::Triangle;
	using core::AccelerationStructure;
	using core::AccelerationStructureBuilder;
	using core::AccelerationStructureRaytracer;

	namespace voxel
	{
		// Octree nodes are represented with a pointer to the node data.
		// Each node holds different kind of data depending on what kind of node it is.
		// Memory is allocated to different node types as follows:
		// - Empty node without children:
		//		No memory allocated.
		// - Node with children:
		//		The header is a byte with value of 0.
		//		The payload is 8 pointers to the children nodes - a pointer is null if a child is an empty node.
		// - Leaf nodes:
		//		The header is a byte with value of 1.
		//		The payload (BRICK_SIZE^3/8) bytes used to describe (BRICK_SIZE^3) voxels.
		//		There are 2 bits reserved for each voxel. 
		//		00 means there is no voxel in the respective position.
		//		01 means there is a obscured voxel in the respective position.
		//		11 means there is a visited (previously visible) voxel in the respective position.

		// The leaves of the tree hold (VOXEL_OCTREE_BRICK_SIZE^3) voxels in compressed format (described above).
		// Must be a power of two. 
		#define VOXEL_OCTREE_BRICK_SIZE 1

		// Optimized tree for visibility calculations with voxels
		class VoxelOctree : public AccelerationStructure
		{

		public:
			using Node = uint8_t*;
			using VoxelValue = uint32_t;

			struct BuildConfig
			{
			public:
				BuildConfig();
				BuildConfig(const TriangleMesh* mesh, unsigned short resolution);
				BuildConfig(const std::vector<const TriangleMesh*>& meshes, unsigned short resolution);

				const std::vector<const TriangleMesh*>& getMeshes() const;
				unsigned short getResolution() const noexcept;

			private:
				std::vector<const TriangleMesh*> m_meshes;
				unsigned short m_resolution = 0;
			};

			class Builder : public AccelerationStructureBuilder<VoxelOctree, BuildConfig>
			{
			public:
				std::unique_ptr<VoxelOctree> build(const BuildConfig& buildConfig) const override;
			private:
				void addGeometry(std::unique_ptr<VoxelOctree>& octree, const std::vector<Vector3f>& vertices, const std::vector<Triangle>& triangles, VoxelOctree::VoxelValue voxelValue = ~0) const;
			};

			class Raytracer : public AccelerationStructureRaytracer<VoxelOctree>
			{
			public:
				// Raytrace the octree.
				bool raytrace(const VoxelOctree& octree, const Ray& ray, RayHit& rayHit) const override;
				bool raytrace(const VoxelOctree& octree, const Ray& ray, RayHit& rayHit, float maxDistance) const override;

			private:
				// Raytrace the octree's normal nodes, implemented by using the algorithm presented in
				// 'An Efficient Parametric Algorithm for Octree Traversal'
				// J.Revelles & C.Urena & M.Lastra.
				bool raytrace_internal(const Ray& ray, RayHit& rayHit, const VoxelOctree::Node& node, float maxDistance, float tx0, float ty0, float tz0, float tx1, float ty1, float tz1, unsigned char a) const;
				// Raycast the regular leaf grid, implemented by using the algorithm presented in
				// 'A Fast Voxel Traversal Algorithm for Ray Tracing'
				//  by John Amanatides & Andrew Woo.
				bool raytrace_leaf_internal(const Ray& ray, RayHit& rayHit, const VoxelOctree::Node& node, float maxDistance, float tx0, float ty0, float tz0, unsigned char a) const;

				// Helpers for raycast_internal
				uint8_t getFirstNode(float tx0, float ty0, float tz0, float txm, float tym, float tzm) const;
				uint8_t getNewNode(unsigned char x, unsigned char y, unsigned char z, float txm, float tym, float tzm) const;
			};

			VoxelOctree(uint16_t size, float voxelSize, const Vector3f& origin);
			VoxelOctree(const VoxelOctree& voxelOctree);
			VoxelOctree(VoxelOctree&& voxelOctree);
			~VoxelOctree();

			VoxelOctree& operator=(const VoxelOctree& voxelOctree);
			VoxelOctree& operator=(VoxelOctree&& voxelOctree);

			// Inserts a voxel to coordinate (x, y, z).
			bool insert(uint16_t x, uint16_t y, uint16_t z, VoxelValue value);
			// Gets the data associated with the voxel in coordinate (x, y, z).
			VoxelValue get(uint16_t x, uint16_t y, uint16_t z) const;

			// Gets the size of the octree in voxels. Always a power of two.
			uint16_t getSize() const noexcept;
			// Gets the world size of a single voxel.
			float getVoxelSize() const noexcept;
			// Gets the bounds of the octree. 
			const BoundsF& getBounds() const noexcept;
			// Gets the number of voxels in the octree.
			uint64_t getVoxelCount() const noexcept;
			// Gets the number normal non-leaf nodes in the tree.
			uint64_t getNormalNodeCount() const noexcept;
			// Gets the number of leaf nodes in the tree. If brick size is 1, this is the same as m_voxelCount.
			uint64_t getLeafNodeCount() const noexcept;
			// Gets the memory allocated for all nodes, in bytes.
			size_t getAllocatedMemory() const noexcept;
			const Node& getRoot() const noexcept;

			// Helpers for checking node type.
			static bool isEmptyNode(const Node& node);
			static bool isNormalNode(const Node& node);
			static bool isLeafNode(const Node& node);

		private:
			using Coordinate = Vector3<uint16_t>;

			// The root node of the octree. Follows the allocation scheme described above.
			Node m_root;
			// The size of the octree in voxels. Always a power of two.
			uint16_t m_size;
			// The world size of a single voxel.
			float m_voxelSize;
			// The bounds of the octree.
			BoundsF m_bounds;
			// Number of normal non-leaf nodes in the tree.
			uint64_t m_normalNodeCount;
			// Number of leaf nodes in the tree. If brick size is 1, this is the same as m_voxelCount.
			uint64_t m_leafNodeCount;
			// Number of voxels in the octree.
			uint64_t m_voxelCount;

			// Release all allocated memory.
			void cleanUp(const Node& node);
			// Internal recursive method for inserting a voxel to a specific coordinate.
			bool insert_internal(Node& node, uint16_t size, Coordinate& coord, VoxelValue value);
			// Internal recursive method for getting voxel data from the voxel in a specific coordinate.
			VoxelValue get_internal(const Node& node, uint16_t size, Coordinate& coord) const;

			// Gets the child node where the given coordinate belongs to.
			Node* getChild(const Node& node, uint16_t halfSize, Coordinate& coord) const;

			// Allocation for different node types.
			Node allocNormalNode();
			Node allocLeafNode();
			size_t getNormalNodeMemory() const noexcept;
			size_t getLeafNodeMemory() const noexcept;
			bool setVoxelValue(const Node& node, const Coordinate& coord, VoxelValue value);
			VoxelValue getVoxelValue(const Node& node, const Coordinate& coord) const;
		};
	}
}