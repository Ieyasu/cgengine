# CGEngine

CGEngine contains some old code for a simple game engine and a voxel raytracer. The project is divided into several subprojects:

* CGCore for core engine functionality
* CGEngine for a sandbox for testing
* CGEntity for a simple entity component system
* CGMath for math classes (vectors, matrices, quaternions, etc.)
* CGUtility for common utility functions
* CGVoxels for a voxel raytracer

The voxel raytracer doesn't have any fancy shading or anything, it is simply proof of concept of sparse voxel octree building and fast intersection testing. For fancier pathtracing stuff, see https://github.com/Ieyasu/valo

## Getting started

Prequisites for building the project:

* OpenGL
* GLFW
* glfw3
* OpenMP (optional)

Run the following commands (on linux):

```bash
> mkdir build
> cd build
> cmake ..
> make
> cp -r ../CGEngine/Meshes .
> cp -r ../CGEngine/Shaders .
> ./cgengine
```

This should open a window with an (ugly) voxel raytraced model of the stanford bunny in it. To raytrace other models, simply pass the .obj file as a commandline parameter to the program.

![](resources/car.jpg)
*A car consisting of 1.4M voxels rendered with the voxel raytracer.*
