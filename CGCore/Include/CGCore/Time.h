#pragma once

#include <chrono>

namespace cg
{
	namespace core
	{
		using std::chrono::high_resolution_clock;
		using std::chrono::time_point;

		class Time
		{
		public:
			Time();
			Time(float maxDeltaTime);

			void initialize();
			void update();
			void setMaxDeltaTime(float maximumDeltaTime); 

			float getTime() const;
			float getDeltaTime() const;
			float getMaxDeltaTime() const;
			unsigned int getFrameCount() const;

		private:
			float m_deltaTime;
			float m_maxDeltaTime;
			float m_timeSinceInitialization;
			unsigned int m_frameCount;

			time_point<high_resolution_clock> m_startTime;
			time_point<high_resolution_clock> m_lastTime;
		};
	}
}