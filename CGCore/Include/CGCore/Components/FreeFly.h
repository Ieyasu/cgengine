#pragma once

#include "CGEntity/Component.h"

namespace cg
{
	using entity::Component;

	namespace core
	{
		class FreeFly : public Component
		{
		public:
			void setMovementSpeed(float movementSpeed);
			void setRotationSpeed(float rotationSpeed);

			float getMovementSpeed() const;
			float getRotationSpeed() const;

		private:

			float m_movementSpeed;
			float m_rotationSpeed;
		};
	}
}