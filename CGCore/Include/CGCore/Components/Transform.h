#pragma once

#include "CGEntity/Component.h"
#include "CGMath/Matrix.h"
#include "CGMath/Quaternion.h"
#include "CGMath/Vector2.h"
#include "CGMath/Vector3.h"
#include "CGMath/Vector4.h"
#include "CGUtility/CachedObject.h"

namespace cg
{
    using entity::Component;
    using math::Matrix4f;
    using math::QuaternionF;
    using math::Vector2f;
    using math::Vector3f;
    using math::Vector4f;
    using util::CachedObject;

    namespace core
    {
        class Transform : public Component
        {
        public:
            Transform();

            void translate(const Vector3f& translation);
            void setLocalPosition(const Vector3f& position);
            void setLocalPosition(float x, float y, float z);
            void setLocalScale(const Vector3f& scale);
            void setLocalRotation(const QuaternionF& rotation);
            void setLocalRotation(const Vector3f& eulerAngles);
            void setLocalRotation(float eulerX, float eulerY, float eulerZ);
            void setLocalRotationRad(const Vector3f& eulerAngles);
            void setLocalRotationRad(float eulerX, float eulerY, float eulerZ);

            Vector3f        getUp() const;
            Vector3f        getRight() const;
            Vector3f        getForward() const;
            const Vector3f& getPosition() const;

            const Vector3f&    getLocalPosition() const;
            const Vector3f&    getLocalScale() const;
            const QuaternionF& getLocalRotation() const;

            const Matrix4f& getLocalRotationMatrix() const;
            const Matrix4f& getLocalToWorldMatrix() const;
            const Matrix4f& getWorldToLocalMatrix() const;
            const Matrix4f& getLocalToWorldMatrixTranspose() const;
            const Matrix4f& getWorldToLocalMatrixTranspose() const;

            Vector3f transformPoint(const Vector3f& point) const;
            Vector3f transformDirection(const Vector3f& direction) const;
            Vector3f transformPointInverse(const Vector3f& point) const;
            Vector3f transformDirectionInverse(const Vector3f& direction) const;

        private:
            Vector3f    m_scale;
            Vector3f    m_position;
            QuaternionF m_rotation;

            // Matrices are cached and computed only when the transformation has changed.
            CachedObject<Matrix4f, Transform> m_localRotationMatrix;
            CachedObject<Matrix4f, Transform> m_localToWorldMatrix;
            CachedObject<Matrix4f, Transform> m_worldToLocalMatrix;
            CachedObject<Matrix4f, Transform> m_localToWorldMatrixTranspose;
            CachedObject<Matrix4f, Transform> m_worldToLocalMatrixTranspose;

            void setDirty();
            void recalculateLocalRotationMatrix(Matrix4f& matrix) const;
            void recalculateLocalToWorldMatrix(Matrix4f& matrix) const;
            void recalculateWorldToLocalMatrix(Matrix4f& matrix) const;
            void recalculateWorldToLocalMatrixTranspose(Matrix4f& matrix) const;
            void recalculateLocalToWorldMatrixTranspose(Matrix4f& matrix) const;
        };
    }  // namespace core
}  // namespace cg
