#pragma once

#include "Transform.h"
#include "../Raytracing/Ray.h"
#include "CGUtility/CachedObject.h"
#include "CGEntity/Component.h"
#include "CGMath/Matrix.h"
#include "CGMath/Vector2.h"

namespace cg
{
	using math::Matrix4f;
	using math::Vector2f;
	using util::CachedObject;
	using core::Ray;
	using entity::Component;

	namespace core
	{
		class Camera : public Component
		{
		public:
			Camera();
			Camera(float nearPlane, float farPlane, float aspectRatio);

			virtual void setAspectRatio(float aspectRatio);
			virtual void setNearPlane(float nearPlane);
			virtual void setFarPlane(float farPlane);
			void setProjectionMatrix(const Matrix4f& projectionMatrix);
			void setProjectionMatrix(const Matrix4f& projectionMatrix, const Matrix4f& projectionMatrixInverse);
			void setViewMatrix(const Transform& transform);

			float getAspectRatio() const;
			float getNearPlane() const;
			float getFarPlane() const;
			const Matrix4f& getProjectionMatrix() const;
			const Matrix4f& getProjectionMatrixInverse() const;
			const Matrix4f& getViewMatrix() const;
			const Matrix4f& getViewMatrixInverse() const;
			Matrix4f getWorldToCameraMatrix() const;
			Matrix4f getCameraToWorldMatrix() const;

			void viewPortPointToRay(Ray& ray, const Vector2f& point) const;
			void viewPortPointToRay(Ray& ray, float pointX, float pointY) const;
			Vector3f viewPortPointToPosition(const Vector3f& point) const;
			virtual Vector3f viewPortPointToPosition(float pointX, float pointY, float distanceFromCamera) const;
			Vector3f viewPortPointToDirection(const Vector2f& point) const;
			virtual Vector3f viewPortPointToDirection(float pointX, float pointY) const;

		private:
			float m_nearPlane;
			float m_farPlane;
			float m_aspectRatio;

			Matrix4f m_projectionMatrix;
			Matrix4f m_projectionMatrixInverse;
			Matrix4f m_viewMatrix;
			Matrix4f m_viewMatrixInverse;

			CachedObject<Matrix4f, Camera> m_worldToCameraMatrix;
			CachedObject<Matrix4f, Camera> m_cameraToWorldMatrix;

			void setDirty();
			void recalculateWorldToCameraMatrix(Matrix4f& matrixOut) const;
			void recalculateCameraToWorldMatrix(Matrix4f& matrixOut) const;
		};
	}
}