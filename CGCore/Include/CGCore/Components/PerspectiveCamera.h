#pragma once

#include "Camera.h"

namespace cg
{
	namespace core
	{
		class PerspectiveCamera : public Camera
		{
		public:
			PerspectiveCamera();

			void setFovVertical(float fovV);
			void setFovVerticalRad(float fovV);
			void setFovHorizontal(float fovH);
			void setFovHorizontalRad(float fovH);

			float getFovVertical() const;
			float getFovHorizontal() const;

			void calculateProjectionMatrix();

		private:
			float m_fovVertical;
		};
	}
}