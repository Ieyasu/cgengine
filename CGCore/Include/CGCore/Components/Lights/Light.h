#pragma once

#include <vector>
#include "CGMath/Vector3.h"
#include "CGEntity/Component.h"
#include "../Transform.h"

namespace cg
{
	using math::Vector3f;
	using entity::Component;

	namespace core
	{
		class Light : public Component
		{
		public:
			void setIntensity(float intensity);
			void setRange(float range);

			float getIntensity() const;
			float getRange() const;

		private:
			float intensity;
			float range;
		};
	}
}