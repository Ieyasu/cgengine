#pragma once

#include <memory>
#include "CGEntity/Component.h"
#include "../Mesh/TriangleMesh.h"

namespace cg
{
	using core::TriangleMesh;
	using entity::Component;

	namespace core
	{
		class MeshRenderer : public Component
		{
		public:
			void setMesh(std::shared_ptr<TriangleMesh> sharedMesh);
			const TriangleMesh* getMesh() const;

		private:
			std::shared_ptr<TriangleMesh> m_sharedMesh;
		};
	}
}