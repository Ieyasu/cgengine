#pragma once

#include "Camera.h"

namespace cg
{
	namespace core
	{
		class OrthographicCamera : public Camera
		{
		public:
			OrthographicCamera();

			void setWidth(float width);
			void setHeight(float height);

			float getWidth() const;
			float getHeight() const;

			void calculateProjectionMatrix();

		private:
			float m_height;
		};
	}
}