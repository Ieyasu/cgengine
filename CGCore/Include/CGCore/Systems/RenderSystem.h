#pragma once
#include <memory>
#include "CGEntity/World.h"
#include "../Components/Transform.h"
#include "../Components/PerspectiveCamera.h"
#include "../Components/OrthographicCamera.h"
#include "../Window.h"
#include "CGEntity.h"

namespace cg
{
	using entity::System;
	using entity::Handle;
	using entity::Entity;

	namespace core
	{
		class RenderSystem : public System
		{
		public:
			RenderSystem();
			Handle<Entity> getCameraEntity() const;

		protected:
			virtual void onInitialize() override;
			virtual void onUpdate() override;
			virtual void onDestroy() override;
			virtual void render() = 0;

		protected:
			Camera* m_activeCamera;

			Handle<Entity> m_cameraEntity;
			Handle<Transform> m_cameraTransform;
			Handle<OrthographicCamera> m_orthoCamera;
			Handle<PerspectiveCamera> m_perspectiveCamera;
			bool m_useOrthoCamera;
		};
	}
}