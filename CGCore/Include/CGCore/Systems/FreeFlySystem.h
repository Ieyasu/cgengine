#pragma once

#include "../Components/FreeFly.h"
#include "../Components/Transform.h"
#include "CGEntity.h"
#include "CGMath/Quaternion.h"
#include "CGMath/Vector2.h"
#include "CGMath/Vector3.h"

namespace cg
{
    using entity::Entity;
    using entity::Handle;
    using entity::System;
    using math::QuaternionF;
    using math::Vector2f;
    using math::Vector3f;

    namespace core
    {
        class FreeFlySystem : public System
        {
        public:
            FreeFlySystem() = default;
            FreeFlySystem(Handle<Entity> entity);

        protected:
            void onUpdate() override;

        private:
            Handle<Entity>    m_entity;
            Handle<FreeFly>   m_freeFly;
            Handle<Transform> m_transform;

            bool        m_isRotating;
            Vector2f    m_dragStartPos;
            QuaternionF m_dragStartRotation;

            void rotate();
            void move();
        };
    }  // namespace core
}  // namespace cg
