#pragma once

#include <memory>

#include "../Components/Lights/PointLight.h"
#include "../Context.h"
#include "../Material/Material.h"
#include "../Material/Shader.h"
#include "CGUtility/IO/LoggerConsole.h"
#include "RenderSystem.h"

namespace cg
{
    using core::Material;
    using core::PointLight;
    using core::Shader;
    using entity::System;

    namespace core
    {
        using namespace context;

        template <class Structure, class Raytracer>
        class RaytraceSystem : public RenderSystem
        {
        public:
            RaytraceSystem();
            RaytraceSystem(std::unique_ptr<Structure> structure, std::unique_ptr<Raytracer> raytracer);
            RaytraceSystem(RaytraceSystem&& raytraceSystem);
            RaytraceSystem(const RaytraceSystem& raytraceSystem) = delete;
            RaytraceSystem& operator=(RaytraceSystem&& raytraceSystem);
            RaytraceSystem& operator=(const RaytraceSystem& raytraceSystem) = delete;

        protected:
            virtual void onInitialize() override;
            virtual void onDestroy() override;
            virtual void render() override;

        private:
            std::unique_ptr<Structure> m_structure;
            std::unique_ptr<Raytracer> m_raytracer;

            std::vector<GLubyte> m_imageBuffer;
            GLuint               m_pboIds[2];
            GLuint               m_textureId;

            GLuint   m_vao;
            GLuint   m_vbo;
            Material m_material;

            void raytrace(GLubyte* destinationBuffer);
        };

        template <class Structure, class Raytracer>
        RaytraceSystem<Structure, Raytracer>::RaytraceSystem()
            : m_structure(nullptr)
            , m_raytracer(nullptr)
        {
        }

        template <class Structure, class Raytracer>
        RaytraceSystem<Structure, Raytracer>::RaytraceSystem(
                std::unique_ptr<Structure> structure,
                std::unique_ptr<Raytracer> raytracer)
            : m_structure(std::move(structure))
            , m_raytracer(std::move(raytracer))
        {
        }

        template <class Structure, class Raytracer>
        RaytraceSystem<Structure, Raytracer>::RaytraceSystem(RaytraceSystem&& other)
        {
            m_structure.swap(other.m_structure);
            m_raytracer.swap(other.m_raytracer);
        }

        template <class Structure, class Raytracer>
        RaytraceSystem<Structure, Raytracer>& RaytraceSystem<Structure, Raytracer>::operator=(RaytraceSystem&& other)
        {
            m_structure.swap(other.m_structure);
            m_raytracer.swap(other.m_raytracer);
        }

        template <class Structure, class Raytracer>
        void RaytraceSystem<Structure, Raytracer>::onInitialize()
        {
            RenderSystem::onInitialize();

            GLfloat vertices[] = {
                    -1,
                    -1,
                    1,
                    -1,
                    -1,
                    1,
                    1,
                    1,
            };

            glGenVertexArrays(1, &m_vao);
            glGenBuffers(1, &m_vbo);

            // 1. Bind Vertex Array Object
            glBindVertexArray(m_vao);
            // 2. Copy our vertices array in a buffer for OpenGL to use
            glBindBuffer(GL_ARRAY_BUFFER, m_vbo);
            glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
            // 3. Then set our vertex attributes pointers
            glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 2 * sizeof(GLfloat), (GLvoid*)0);
            glEnableVertexAttribArray(0);
            glBindBuffer(GL_ARRAY_BUFFER, 0);
            // 4. Unbind the VAO
            glBindVertexArray(0);

            // Load shader.
            m_material = Material(Shader("Shaders/vert.glsl", "Shaders/frag.glsl"));

            // Get window size.
            const auto     windowWidth = getWindow().getWidth();
            const auto     windowHeight = getWindow().getHeight();
            constexpr auto channelCount = 4;
            const auto     dataSize = channelCount * windowWidth * windowHeight;

            // Create an OpenGL texture to render the raytraced image to.
            glActiveTexture(GL_TEXTURE0);
            glGenTextures(1, &m_textureId);
            glBindTexture(GL_TEXTURE_2D, m_textureId);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
            glBindTexture(GL_TEXTURE_2D, 0);

            // Create pixel buffer objects (PBO's).
            // These allow faster transfer between the GPU and main memory.
            glGenBuffersARB(2, m_pboIds);
            glBindBufferARB(GL_PIXEL_UNPACK_BUFFER_ARB, m_pboIds[0]);
            glBufferDataARB(GL_PIXEL_UNPACK_BUFFER_ARB, (GLsizeiptrARB)dataSize, nullptr, GL_STREAM_DRAW_ARB);
            glBindBufferARB(GL_PIXEL_UNPACK_BUFFER_ARB, m_pboIds[1]);
            glBufferDataARB(GL_PIXEL_UNPACK_BUFFER_ARB, (GLsizeiptrARB)dataSize, nullptr, GL_STREAM_DRAW_ARB);
            glBindBufferARB(GL_PIXEL_UNPACK_BUFFER_ARB, 0);

            // Resize buffer.
            m_imageBuffer.resize(dataSize, 0);
        }

        template <class Structure, class Raytracer>
        void RaytraceSystem<Structure, Raytracer>::raytrace(GLubyte* destinationBuffer)
        {
            const auto windowWidth = getWindow().getWidth();
            const auto windowHeight = getWindow().getHeight();
            const auto farPlane = m_activeCamera->getFarPlane();
            auto       ray = Ray();
            auto       hit = RayHit();

            auto pointLights = m_world->getAllComponentsOfType<PointLight>();

            auto begin = std::chrono::high_resolution_clock::now();
#pragma omp parallel for schedule(dynamic, 10) private(ray, hit)
            for (auto y = 0; y < windowHeight; ++y)
            {
                const auto yIndex = y * windowWidth;
                for (auto x = 0; x < windowWidth; ++x)
                {
                    const auto index = 4 * (yIndex + x);

                    m_activeCamera->viewPortPointToRay(ray, (float)x / windowWidth, 1.0f - (float)y / windowHeight);
                    if (m_raytracer->raytrace(*m_structure, ray, hit, farPlane))
                    {
                        float lightIntensity = 0.1f;

                        // Trace shadow rays.
                        for (const auto& _ : *pointLights)
                        {
                            const auto& lightSample = Vector3f(0.0f, 1.0f, 0.0f);
                            ray.origin = (hit.t - 0.01f) * ray.direction + ray.origin;
                            ray.direction = lightSample - ray.origin;
                            ray.direction.normalize();

                            if (!m_raytracer->raytrace(*m_structure, ray, hit))
                            {
                                lightIntensity = 1.0f;
                            }
                        }

                        destinationBuffer[index] = (GLubyte)(hit.color.x * lightIntensity);
                        destinationBuffer[index + 1] = (GLubyte)(hit.color.y * lightIntensity);
                        destinationBuffer[index + 2] = (GLubyte)(hit.color.z * lightIntensity);
                        destinationBuffer[index + 3] = (GLubyte)(hit.color.w * lightIntensity);
                    }
                    else
                    {
                        destinationBuffer[index] = 0;
                        destinationBuffer[index + 1] = 0;
                        destinationBuffer[index + 2] = 0;
                        destinationBuffer[index + 3] = 0;
                    }
                }
            }
            auto end = std::chrono::high_resolution_clock::now();
            CG_LOG("Raytracing took "
                   + std::to_string(std::chrono::duration_cast<std::chrono::milliseconds>(end - begin).count())
                   + " ms.");
        }

        template <class Structure, class Raytracer>
        void RaytraceSystem<Structure, Raytracer>::render()
        {
            constexpr auto channelCount = 4;
            const auto     windowWidth = getWindow().getWidth();
            const auto     windowHeight = getWindow().getHeight();
            const auto     dataSize = channelCount * windowWidth * windowHeight;

            // "index" is used to copy pixels from a PBO to a texture object
            // "nextIndex" is used to update pixels in the other PBO
            static auto pboIndex = 0;
            pboIndex = (pboIndex + 1) % 2;
            const auto pboNextIndex = (pboIndex + 1) % 2;

            // Resize buffer.
            m_imageBuffer.resize(dataSize, 0);

            // Bind texture and resize it to window size.
            glBindTexture(GL_TEXTURE_2D, m_textureId);
            glTexImage2D(
                    GL_TEXTURE_2D,
                    0,
                    GL_RGBA8,
                    (GLsizei)windowWidth,
                    (GLsizei)windowHeight,
                    0,
                    GL_RGBA,
                    GL_UNSIGNED_BYTE,
                    (GLvoid*)m_imageBuffer.data());
            glBindBufferARB(GL_PIXEL_UNPACK_BUFFER_ARB, m_pboIds[pboIndex]);
            glViewport(0, 0, windowWidth, windowHeight);

            // copy pixels from PBO to texture object
            // Use offset instead of pointer.
            glTexSubImage2D(
                    GL_TEXTURE_2D,
                    0,
                    0,
                    0,
                    (GLsizei)windowWidth,
                    (GLsizei)windowHeight,
                    GL_RGBA,
                    GL_UNSIGNED_BYTE,
                    0);

            // bind PBO to update texture source
            glBindBufferARB(GL_PIXEL_UNPACK_BUFFER_ARB, m_pboIds[pboNextIndex]);

            // Note that glMapBufferARB() causes sync issue.
            // If GPU is working with this buffer, glMapBufferARB() will wait(stall)
            // until GPU to finish its job. To avoid waiting (idle), you can call
            // first glBufferDataARB() with NULL pointer before glMapBufferARB().
            // If you do that, the previous data in PBO will be discarded and
            // glMapBufferARB() returns a new allocated pointer immediately
            // even if GPU is still working with the previous data.
            glBufferDataARB(GL_PIXEL_UNPACK_BUFFER_ARB, (GLsizeiptrARB)dataSize, nullptr, GL_STREAM_DRAW_ARB);

            // map the buffer object into client's memory
            auto ptr = (GLubyte*)glMapBufferARB(GL_PIXEL_UNPACK_BUFFER_ARB, GL_WRITE_ONLY_ARB);
            if (ptr)
            {
                // update data directly on the mapped buffer
                raytrace(ptr);
                // release the mapped buffer
                glUnmapBufferARB(GL_PIXEL_UNPACK_BUFFER_ARB);
            }

            // it is good idea to release PBOs with ID 0 after use.
            // Once bound with 0, all pixel operations are back to normal ways.
            glBindBufferARB(GL_PIXEL_UNPACK_BUFFER_ARB, 0);

            // Draw.
            m_material.use();
            // m_material.setUniformTexture2D(m_textureId, "texture");
            glBindVertexArray(m_vao);
            glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
            glBindVertexArray(0);
        }

        template <class Structure, class Raytracer>
        void RaytraceSystem<Structure, Raytracer>::onDestroy()
        {
            RenderSystem::onDestroy();

            // Delete texture.
            glDeleteTextures(1, &m_textureId);
            glDeleteVertexArrays(1, &m_vao);
            glDeleteBuffers(1, &m_vbo);

            // Delete the PBO's.
            glDeleteBuffersARB(2, m_pboIds);
        }
    }  // namespace core
}  // namespace cg
