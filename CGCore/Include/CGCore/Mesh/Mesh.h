#pragma once

#include <vector>

#include "CGMath/Bounds.h"
#include "CGMath/Vector2.h"
#include "CGMath/Vector3.h"

namespace cg
{
    using math::BoundsF;
    using math::Vector2f;
    using math::Vector3;
    using math::Vector3f;

    namespace core
    {
        typedef unsigned int         VertexIndex;
        typedef Vector3<VertexIndex> Triangle;

        struct Face
        {
        public:
            void                            setIndices(const std::vector<VertexIndex>& indices);
            const std::vector<VertexIndex>& getIndices() const;

        private:
            std::vector<VertexIndex> m_indices;
        };

        class Mesh
        {
        public:
            virtual ~Mesh() = default;

            void clear();

            void beginEdit();
            void endEdit();

            // virtual void recalculateNormals() = 0;
            void normalize();

            void addPosition(const Vector3f& position);
            void addNormal(const Vector3f& normal);
            void addUV(const Vector2f& uv);
            void setPosition(const Vector3f& position, size_t i);
            void setNormal(const Vector3f& normal, size_t i);
            void setUV(const Vector2f& uv, size_t i);
            void setPositions(const std::vector<Vector3f>& positions);
            void setNormals(const std::vector<Vector3f>& normals);
            void setUVs(const std::vector<Vector2f>& uvs);

            const std::vector<Vector3f>& getPositions() const;
            const std::vector<Vector3f>& getNormals() const;
            const std::vector<Vector2f>& getUVs() const;
            const BoundsF&               getBounds() const;

        protected:
            std::vector<Vector3f> m_positions;
            std::vector<Vector3f> m_normals;
            std::vector<Vector2f> m_uvs;

            BoundsF m_bounds;
            bool    m_inEditMode;

            virtual void clearSubMeshes() = 0;
            virtual bool areSubMeshesValid() const = 0;
            void         checkEditMode();
            void         recalculateBounds();
        };
    }  // namespace core
}  // namespace cg
