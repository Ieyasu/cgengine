#pragma once

#include <vector>
#include "Mesh.h"
#include "../Material/Material.h"

namespace cg
{
	namespace core
	{

		class TriangleMesh : public Mesh
		{
			class SubMesh
			{
			public:
				Material material;
				std::vector<Triangle> triangles;
			};

		public:
			void addTriangle(const Triangle& triangle, size_t subMesh);
			void addTriangles(const std::vector<Triangle>& triangles, size_t subMesh);
			void setTriangle(const Triangle& triangle, size_t subMesh, size_t i);
			void setTriangles(const std::vector<Triangle>& triangles, size_t subMesh);
			void setMaterial(const Material& material, size_t subMesh);

			size_t getSubMeshCount() const;
			const Material& getMaterial(size_t subMesh) const;
			const std::vector<Triangle>& getTriangles(size_t subMesh) const;

		protected:
			void clearSubMeshes() override;
			bool areSubMeshesValid() const override;

		private:
			std::vector<SubMesh> m_subMeshes;

			void resizeSubMeshes(size_t n);
		};
	}
}