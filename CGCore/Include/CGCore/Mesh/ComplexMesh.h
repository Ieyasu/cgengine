#pragma once
#include <vector>
#include <memory>
#include "TriangleMesh.h"

namespace cg
{
	namespace core
	{
		class ComplexMesh : public Mesh
		{
			class SubMesh
			{
			public:
				Material material;
				std::vector<Face> faces;
			};

		public:
			void addFace(const Face& face, size_t subMesh);
			void addFaces(const std::vector<Face>& faces, size_t subMesh);
			void setFace(const Face& face, size_t subMesh, size_t i);
			void setFaces(const std::vector<Face>& faces, size_t subMesh);
			void setMaterial(const Material& material, size_t subMesh);

			size_t getSubMeshCount() const;
			const Material& getMaterial(size_t subMesh) const;
			const std::vector<Face>& getFaces(size_t subMesh) const;

			std::unique_ptr<TriangleMesh> triangulate() const;

		protected:
			void clearSubMeshes() override;
			bool areSubMeshesValid() const override;

		private:
			std::vector<SubMesh> m_subMeshes;

			void resizeSubMeshes(size_t n);
		};
	}
}