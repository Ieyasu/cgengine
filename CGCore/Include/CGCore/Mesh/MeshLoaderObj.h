#pragma once

#include <memory>
#include <string>
#include <vector>
#include <sstream>
#include <fstream>
#include "ComplexMesh.h"

namespace cg
{
	using math::Vector3i;

	namespace core
	{
		class MeshLoaderObj
		{
		public:
			std::unique_ptr<ComplexMesh> loadComplexMesh(const std::string& filepath) const;
			std::unique_ptr<TriangleMesh> loadTriangleMesh(const std::string& filepath) const;

		private:
			bool readVector2f(const char* line, Vector2f& vecOut) const;
			bool readVector3f(const char* line, Vector3f& vecOut) const;
			bool readFace(std::string& line, std::vector<Vector3i>& faceOut) const;
		};
	}
}