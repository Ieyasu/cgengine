#pragma once
#define GLEW_STATIC
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <memory>
#include "Time.h"
#include "Window.h"
#include "Input/InputMapper.h"

namespace cg
{
	namespace core
	{
		namespace context
		{
			bool initialize(int windowWidth, int windowHeight, const std::string& windowTitle);
			void uninitialize();
			void update();

			bool isAvailable();
			const Time& getTime();
			const Window& getWindow();
			const InputMapper& getInput();
		}
	}
}