#pragma once

#define GLEW_STATIC
#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <string>

#define CG_SHADER_ERROR_DISPLAY_LENGTH 512

namespace cg
{
    namespace core
    {
        class Shader
        {
        public:
            using Program = GLuint;

            Shader();
            Shader(Shader&& other);
            Shader(const Shader& other);
            Shader(const std::string& vertexFilepath, const std::string& fragmentFilepath);
            ~Shader();

            Shader& operator=(Shader&& other);
            Shader& operator=(const Shader& other);

            bool    compile(const std::string& vertexFilepath, const std::string& fragmentFilepath);
            Program getProgram() const;

        private:
            Program     m_shaderProgram;
            std::string m_vertexShaderFilepath;
            std::string m_fragmentShaderFilepath;

            bool linkShaders(GLuint shaderProgram, GLuint vertexShader, GLuint fragmentShader);
            bool compileShader(GLuint shader, const std::string& shaderSource);
            bool loadShader(const std::string& filepath, std::string& source);
        };
    }  // namespace core
}  // namespace cg
