#pragma once

#include <map>
#include "CGMath/Vector2.h"
#include "CGMath/Vector3.h"
#include "CGMath/Vector4.h"
#include "Shader.h"

namespace cg
{
	using math::Vector2f;
	using math::Vector3f;
	using math::Vector4f;

	namespace core
	{
		class Material
		{
		public:
			Material();
			Material(Shader shader);
			Material(const std::string& name);

			void use();
			void setToDefault();

			void setUniformFloat(float val, const std::string& name);
			void setUniformVector2(const Vector2f& vec, const std::string& name);
			void setUniformVector3(const Vector3f& vec, const std::string& name);
			void setUniformVector4(const Vector4f& vec, const std::string& name);
			void setUniformTexture2D(GLuint textureId, const std::string& name);

			float getUniformFloat(const std::string& name) const;
			const Vector2f& getUniformVector2(const std::string& name) const;
			const Vector3f& getUniformVector3(const std::string& name) const;
			const Vector4f& getUniformVector4(const std::string& name) const;
			GLuint getUniformTexture2D(const std::string& name) const;

			const Shader& getShader() const;
			const std::string& getName() const;

		private:
			std::string m_name;
			Shader m_shader;

			std::map<std::string, float> m_uniformFloats;
			std::map<std::string, Vector2f> m_uniformVector2s;
			std::map<std::string, Vector3f> m_uniformVector3s;
			std::map<std::string, Vector4f> m_uniformVector4s;
			std::map<std::string, GLuint> m_uniformTexture2Ds;

			static std::string generateNewName();
		};
	}
}