#pragma once

#include <vector>
#include <memory>
#include "Material.h"
#include "CGMath/Vector3.h"
#include "CGUtility/IO/FileBuffer.h"

namespace cg
{
	using math::Vector3f;
	using util::FileBuffer;

	namespace core
	{
		class MaterialLoaderMtl
		{
		public:
			std::vector<std::unique_ptr<Material>> loadMaterials(const std::string& filepath) const;

		private:
			bool readVector3f(const char* line, Vector3f& vecOut) const;
			bool readFloat(const char* line, float& valOut) const;
		};
	}
}