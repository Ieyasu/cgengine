#pragma once

#include <memory>
#include "Ray.h"

namespace cg
{
	namespace core
	{
		class AccelerationStructure
		{
		public:
			virtual ~AccelerationStructure() = default;
		};

		template<class Structure, class BuildConfig>
		class AccelerationStructureBuilder
		{
		public:
			virtual ~AccelerationStructureBuilder() = default;
			virtual std::unique_ptr<Structure> build(const BuildConfig& buildConfig) const = 0;
		};

		template<class Structure>
		class AccelerationStructureRaytracer
		{
		public:
			virtual ~AccelerationStructureRaytracer() = default;
			virtual bool raytrace(const Structure& structure, const Ray& ray, RayHit& hitOut) const = 0;
			virtual bool raytrace(const Structure& structure, const Ray& ray, RayHit& hitOut, float maxDistance) const = 0;
		};
	}
}