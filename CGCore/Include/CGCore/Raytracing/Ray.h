#pragma once

#include <cstdint>
#include "CGMath/Vector3.h"
#include "CGMath/Vector4.h"

namespace cg
{
	using math::Vector3f;
	using math::Vector4;

	namespace core
	{
		using Color32 = Vector4<uint8_t>;

		class Ray
		{
		public:
			Vector3f origin;
			Vector3f direction;

			Ray();
			Ray(const Vector3f &origin, const Vector3f &direction);
			Ray(float origX, float origY, float origZ, float dirX, float dirY, float dirZ);

			Vector3f calculatePos(float t) const;
		};

		class RayHit
		{
		public:
			float t;
			Color32 color;
		};
	}
}
