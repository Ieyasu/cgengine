#pragma once

#define GLEW_STATIC
#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <map>
#include <queue>
#include <set>
#include <vector>

#include "CGCore/Window.h"
#include "CGMath/Vector2.h"
#include "Keycode.h"

namespace cg
{
    using math::Vector2f;

    namespace core
    {
        class InputMapper
        {
            enum class ButtonState
            {
                FREE,
                PRESSED,
                HELD,
                RELEASED
            };

        public:
            bool initialize(const Window& window);
            void update();

            Vector2f getMousePosition() const;
            bool     isPressed(Keycode keycode) const;
            bool     isHeld(Keycode keycode) const;
            bool     isReleased(Keycode keycode) const;
            bool     isPressed(const std::string& inputId) const;
            bool     isHeld(const std::string& inputId) const;
            bool     isReleased(const std::string& inputId) const;

        private:
            const Window*                              m_window;
            std::map<Keycode, ButtonState>             m_buttonStates;
            std::map<Keycode, std::queue<ButtonState>> m_buttonStateBuffer;

            void readInputBuffer();
            void mouseCallback(GLFWwindow* window, int button, int action, int mods);
            void keyCallback(GLFWwindow* window, int key, int scancode, int action, int mode);
            void updateButtonState(int key, int action);
            bool isButtonState(Keycode keycode, ButtonState buttonState) const;

            // Static registering for GLFW input events.
            static std::set<const Window*> s_windows;
            static std::set<InputMapper*>  s_inputMappers;
            static void                    registerWindow(const Window& window);
            static void                    registerInputMapper(InputMapper& inputMapper);
            static void                    unregisterInputMapper(InputMapper& inputMapper);
            static void                    globalMouseCallback(GLFWwindow* window, int button, int action, int mods);
            static void globalKeyCallback(GLFWwindow* window, int key, int scancode, int action, int mode);
        };
    }  // namespace core
}  // namespace cg
