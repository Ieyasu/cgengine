#pragma once

#define GLEW_STATIC
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <string>

#define CG_GLFW_CONTEXT_VERSION_MINOR 3
#define CG_GLFW_CONTEXT_VERSION_MAJOR 3

namespace cg
{
	namespace core
	{
		class Window
		{
		public:
			Window();
			Window(int width, int height, const std::string& title, bool fullScreen);
			~Window();

			bool open(int width, int height, const std::string& title, bool fullScreen);

			bool isOpen() const;
			int getWidth() const;
			int getHeight() const;
			float getAspectRatio() const;
			bool isFullScreen() const;
			const std::string& getTitle() const;
			GLFWwindow* getRawPtr() const;

		private:
			std::string m_title;
			bool m_isFullScreen;
			GLFWwindow* m_glWindow;
		};
	}
}