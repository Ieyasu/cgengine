#include "CGCore/Components/Lights/Light.h"

namespace cg
{
    namespace core
    {
        void Light::setIntensity(float val)
        {
            this->intensity = val;
        }

        void Light::setRange(float val)
        {
            this->range = val;
        }

        float Light::getIntensity() const
        {
            return intensity;
        }

        float Light::getRange() const
        {
            return range;
        }
    }  // namespace core
}  // namespace cg
