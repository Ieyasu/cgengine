#include "CGCore/Components/Transform.h"

namespace cg
{
    namespace core
    {
        Transform::Transform()
            : m_scale(1.0f)
            , m_position(0.0f)
            , m_rotation()
            , m_localRotationMatrix(&Transform::recalculateLocalRotationMatrix)
            , m_localToWorldMatrix(&Transform::recalculateLocalToWorldMatrix)
            , m_worldToLocalMatrix(&Transform::recalculateWorldToLocalMatrix)
            , m_localToWorldMatrixTranspose(&Transform::recalculateLocalToWorldMatrixTranspose)
            , m_worldToLocalMatrixTranspose(&Transform::recalculateWorldToLocalMatrixTranspose)
        {
            setDirty();
        }

        void Transform::translate(const Vector3f& translation)
        {
            m_position += translation;
            setDirty();
        }

        void Transform::setLocalPosition(const Vector3f& position)
        {
            m_position = position;
            setDirty();
        }

        void Transform::setLocalPosition(float x, float y, float z)
        {
            setLocalPosition(Vector3f(x, y, z));
        }

        void Transform::setLocalScale(const Vector3f& scale)
        {
            m_scale = scale;
            setDirty();
        }

        void Transform::setLocalRotation(const QuaternionF& rotation)
        {
            m_rotation = rotation;
            setDirty();
        }

        void Transform::setLocalRotation(const Vector3f& eulerAngles)
        {
            setLocalRotation(eulerAngles.x, eulerAngles.y, eulerAngles.z);
        }

        void Transform::setLocalRotation(float eulerX, float eulerY, float eulerZ)
        {
            m_rotation.fromEuler(eulerX, eulerY, eulerZ);
            setDirty();
        }

        void Transform::setLocalRotationRad(const Vector3f& eulerAngles)
        {
            setLocalRotationRad(eulerAngles.x, eulerAngles.y, eulerAngles.z);
        }

        void Transform::setLocalRotationRad(float eulerX, float eulerY, float eulerZ)
        {
            m_rotation.fromEulerRad(eulerX, eulerY, eulerZ);
            setDirty();
        }

        Vector3f Transform::getUp() const
        {
            const Matrix4f& matrix = m_worldToLocalMatrixTranspose.get(*this);
            return matrix.getColumn(1);
        }

        Vector3f Transform::getRight() const
        {
            const Matrix4f& matrix = m_worldToLocalMatrixTranspose.get(*this);
            return matrix.getColumn(0);
        }

        Vector3f Transform::getForward() const
        {
            const Matrix4f& matrix = m_worldToLocalMatrixTranspose.get(*this);
            return -matrix.getColumn(2);
        }

        const Vector3f& Transform::getPosition() const
        {
            return m_position;
        }

        const Vector3f& Transform::getLocalPosition() const
        {
            return m_position;
        }

        const Vector3f& Transform::getLocalScale() const
        {
            return m_scale;
        }

        const QuaternionF& Transform::getLocalRotation() const
        {
            return m_rotation;
        }

        const Matrix4f& Transform::getLocalRotationMatrix() const
        {
            return m_localRotationMatrix.get(*this);
        }

        const Matrix4f& Transform::getLocalToWorldMatrix() const
        {
            return m_localToWorldMatrix.get(*this);
        }

        const Matrix4f& Transform::getWorldToLocalMatrix() const
        {
            return m_worldToLocalMatrix.get(*this);
        }

        const Matrix4f& Transform::getLocalToWorldMatrixTranspose() const
        {
            return m_localToWorldMatrixTranspose.get(*this);
        }

        const Matrix4f& Transform::getWorldToLocalMatrixTranspose() const
        {
            return m_localToWorldMatrixTranspose.get(*this);
        }

        Vector3f Transform::transformPoint(const Vector3f& point) const
        {
            return m_localToWorldMatrix.get(*this) * Vector4f(point.x, point.y, point.z, 1.0f);
        }

        Vector3f Transform::transformDirection(const Vector3f& direction) const
        {
            return m_worldToLocalMatrixTranspose.get(*this) * Vector3f(direction.x, direction.y, direction.z);
        }

        Vector3f Transform::transformPointInverse(const Vector3f& point) const
        {
            return m_worldToLocalMatrix.get(*this) * Vector4f(point.x, point.y, point.z, 1.0f);
        }

        Vector3f Transform::transformDirectionInverse(const Vector3f& direction) const
        {
            return m_localToWorldMatrixTranspose.get(*this) * Vector3f(direction.x, direction.y, direction.z);
        }

        // Functions for calculating cached matrices.

        void Transform::setDirty()
        {
            m_localRotationMatrix.setDirty();
            m_localToWorldMatrix.setDirty();
            m_worldToLocalMatrix.setDirty();
            m_worldToLocalMatrixTranspose.setDirty();
            m_localToWorldMatrixTranspose.setDirty();
        }

        void Transform::recalculateLocalRotationMatrix(Matrix4f& matrixOut) const
        {
            // Make sure the rotation is a unit quaternion.
            const QuaternionF& normalizedRotation = m_rotation.getNormalized();
            matrixOut = normalizedRotation.getMatrix();
        }

        void Transform::recalculateLocalToWorldMatrix(Matrix4f& matrixOut) const
        {
            const Matrix4f& rotationMatrix = m_localRotationMatrix.get(*this);
            matrixOut.setRow(
                    0,
                    m_scale.x * rotationMatrix[0],
                    m_scale.y * rotationMatrix[1],
                    m_scale.z * rotationMatrix[2],
                    m_position.x);
            matrixOut.setRow(
                    1,
                    m_scale.x * rotationMatrix[4],
                    m_scale.y * rotationMatrix[5],
                    m_scale.z * rotationMatrix[6],
                    m_position.y);
            matrixOut.setRow(
                    2,
                    m_scale.x * rotationMatrix[8],
                    m_scale.y * rotationMatrix[9],
                    m_scale.z * rotationMatrix[10],
                    m_position.z);
            matrixOut.setRow(3, 0, 0, 0, 1);
        }

        void Transform::recalculateWorldToLocalMatrix(Matrix4f& matrixOut) const
        {
            const Matrix4f& rotationMatrix = m_localRotationMatrix.get(*this);
            const float     invScaleX = 1.0f / m_scale.x;
            const float     invScaleY = 1.0f / m_scale.y;
            const float     invScaleZ = 1.0f / m_scale.z;
            matrixOut.setColumn(
                    0,
                    invScaleX * rotationMatrix[0],
                    invScaleY * rotationMatrix[1],
                    invScaleZ * rotationMatrix[2],
                    0);
            matrixOut.setColumn(
                    1,
                    invScaleX * rotationMatrix[4],
                    invScaleY * rotationMatrix[5],
                    invScaleZ * rotationMatrix[6],
                    0);
            matrixOut.setColumn(
                    2,
                    invScaleX * rotationMatrix[8],
                    invScaleY * rotationMatrix[9],
                    invScaleZ * rotationMatrix[10],
                    0);
            matrixOut.setColumn(
                    3,
                    -m_position.x * matrixOut[0] - m_position.y * matrixOut[1] - m_position.z * matrixOut[2],
                    -m_position.x * matrixOut[4] - m_position.y * matrixOut[5] - m_position.z * matrixOut[6],
                    -m_position.x * matrixOut[8] - m_position.y * matrixOut[9] - m_position.z * matrixOut[10],
                    1);
        }

        void Transform::recalculateWorldToLocalMatrixTranspose(Matrix4f& matrixOut) const
        {
            matrixOut = m_worldToLocalMatrix.get(*this).getTranspose();
        }

        void Transform::recalculateLocalToWorldMatrixTranspose(Matrix4f& matrixOut) const
        {
            matrixOut = m_localToWorldMatrix.get(*this).getTranspose();
        }
    }  // namespace core
}  // namespace cg
