#include "CGCore/Components/MeshRenderer.h"

namespace cg
{
    namespace core
    {
        void MeshRenderer::setMesh(std::shared_ptr<TriangleMesh> sharedMesh)
        {
            m_sharedMesh = sharedMesh;
        }

        const TriangleMesh* MeshRenderer::getMesh() const
        {
            return m_sharedMesh.get();
        }
    }  // namespace core
}  // namespace cg
