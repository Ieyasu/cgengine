#include "CGCore/Components/Camera.h"

#include "CGUtility/IO/LoggerConsole.h"

namespace cg
{
    namespace core
    {
        Camera::Camera()
            : Camera(0.1f, 100.0f, 16.0f / 9.0f)
        {
        }

        Camera::Camera(float nearPlane, float farPlane, float aspectRatio)
            : m_nearPlane(nearPlane)
            , m_farPlane(farPlane)
            , m_aspectRatio(aspectRatio)
            , m_worldToCameraMatrix(&Camera::recalculateWorldToCameraMatrix)
            , m_cameraToWorldMatrix(&Camera::recalculateCameraToWorldMatrix)
        {
            setDirty();
        }

        void Camera::setAspectRatio(float aspectRatio)
        {
            m_aspectRatio = aspectRatio;
            setDirty();
        }

        void Camera::setNearPlane(float nearPlane)
        {
            m_nearPlane = nearPlane;
            setDirty();
        }

        void Camera::setFarPlane(float farPlane)
        {
            m_farPlane = farPlane;
            setDirty();
        }

        void Camera::setProjectionMatrix(const Matrix4f& projectionMatrix)
        {
            m_projectionMatrix = projectionMatrix;
            m_projectionMatrixInverse = projectionMatrix.getInverse();
            setDirty();
        }

        void Camera::setProjectionMatrix(const Matrix4f& projectionMatrix, const Matrix4f& projectionMatrixInverse)
        {
            m_projectionMatrix = projectionMatrix;
            m_projectionMatrixInverse = projectionMatrixInverse;
            setDirty();
        }

        void Camera::setViewMatrix(const Transform& transform)
        {
            const Matrix4f& rotationMatrix = transform.getLocalRotationMatrix();
            const Vector3f& position = transform.getPosition();

            m_viewMatrix.setColumn(0, rotationMatrix[0], rotationMatrix[1], rotationMatrix[2], 0);
            m_viewMatrix.setColumn(1, rotationMatrix[4], rotationMatrix[5], rotationMatrix[6], 0);
            m_viewMatrix.setColumn(2, rotationMatrix[8], rotationMatrix[9], rotationMatrix[10], 0);
            m_viewMatrix.setColumn(
                    3,
                    -position.x * rotationMatrix[0] - position.y * rotationMatrix[1] - position.z * rotationMatrix[2],
                    -position.x * rotationMatrix[4] - position.y * rotationMatrix[5] - position.z * rotationMatrix[6],
                    -position.x * rotationMatrix[8] - position.y * rotationMatrix[9] - position.z * rotationMatrix[10],
                    1);

            m_viewMatrixInverse.setRow(0, rotationMatrix[0], rotationMatrix[1], rotationMatrix[2], position.x);
            m_viewMatrixInverse.setRow(1, rotationMatrix[4], rotationMatrix[5], rotationMatrix[6], position.y);
            m_viewMatrixInverse.setRow(2, rotationMatrix[8], rotationMatrix[9], rotationMatrix[10], position.z);
            m_viewMatrixInverse.setRow(3, 0, 0, 0, 1);

            setDirty();
        }

        float Camera::getAspectRatio() const
        {
            return m_aspectRatio;
        }

        float Camera::getNearPlane() const
        {
            return m_nearPlane;
        }

        float Camera::getFarPlane() const
        {
            return m_farPlane;
        }

        const Matrix4f& Camera::getProjectionMatrix() const
        {
            return m_projectionMatrix;
        }

        const Matrix4f& Camera::getProjectionMatrixInverse() const
        {
            return m_projectionMatrixInverse;
        }

        const Matrix4f& Camera::getViewMatrix() const
        {
            return m_viewMatrix;
        }

        const Matrix4f& Camera::getViewMatrixInverse() const
        {
            return m_viewMatrixInverse;
        }

        Matrix4f Camera::getWorldToCameraMatrix() const
        {
            return m_worldToCameraMatrix.get(*this);
        }

        Matrix4f Camera::getCameraToWorldMatrix() const
        {
            return m_cameraToWorldMatrix.get(*this);
        }

        void Camera::viewPortPointToRay(Ray& ray, const Vector2f& point) const
        {
            return viewPortPointToRay(ray, point.x, point.y);
        }

        void Camera::viewPortPointToRay(Ray& ray, float pointX, float pointY) const
        {
            const auto& projectionMatrix = getProjectionMatrix();
            const auto& cameraToWorldMatrix = m_cameraToWorldMatrix.get(*this);
            const float near = getNearPlane();
            const float ndcX = (2.0f * pointX - 1.0f);
            const float ndcY = (1.0f - pointY * 2.0f);

            // We need the w to invert possible projection scaling.
            const float z1 = -projectionMatrix[10] * near + projectionMatrix[11];
            const float w1 = -projectionMatrix[14] * near + projectionMatrix[15];
            const float ndcX1 = ndcX * w1;
            const float ndcY1 = ndcY * w1;
            const float z2 = -projectionMatrix[10];
            const float w2 = -projectionMatrix[14];
            const float ndcX2 = ndcX * w2;
            const float ndcY2 = ndcY * w2;

            // Calculate origin.
            ray.origin.x = cameraToWorldMatrix[0] * ndcX1 + cameraToWorldMatrix[1] * ndcY1 + cameraToWorldMatrix[2] * z1
                         + cameraToWorldMatrix[3] * w1;
            ray.origin.y = cameraToWorldMatrix[4] * ndcX1 + cameraToWorldMatrix[5] * ndcY1 + cameraToWorldMatrix[6] * z1
                         + cameraToWorldMatrix[7] * w1;
            ray.origin.z = cameraToWorldMatrix[8] * ndcX1 + cameraToWorldMatrix[9] * ndcY1
                         + cameraToWorldMatrix[10] * z1 + cameraToWorldMatrix[11] * w1;

            // Calculate direction.
            ray.direction.x = cameraToWorldMatrix[0] * ndcX2 + cameraToWorldMatrix[1] * ndcY2
                            + cameraToWorldMatrix[2] * z2 + cameraToWorldMatrix[3] * w2;
            ray.direction.y = cameraToWorldMatrix[4] * ndcX2 + cameraToWorldMatrix[5] * ndcY2
                            + cameraToWorldMatrix[6] * z2 + cameraToWorldMatrix[7] * w2;
            ray.direction.z = cameraToWorldMatrix[8] * ndcX2 + cameraToWorldMatrix[9] * ndcY2
                            + cameraToWorldMatrix[10] * z2 + cameraToWorldMatrix[11] * w2;
        }

        Vector3f Camera::viewPortPointToPosition(const Vector3f& point) const
        {
            return viewPortPointToPosition(point.x, point.y, point.z);
        }

        Vector3f Camera::viewPortPointToDirection(const Vector2f& point) const
        {
            return viewPortPointToDirection(point.x, point.y);
        }

        Vector3f Camera::viewPortPointToPosition(float pointX, float pointY, float distanceFromCamera) const
        {
            const auto& projectionMatrix = getProjectionMatrix();
            const float near = getNearPlane();

            distanceFromCamera = -distanceFromCamera - near;
            // We need the w to invert possible projection scaling.
            const float z = projectionMatrix[10] * distanceFromCamera + projectionMatrix[11];
            const float w = projectionMatrix[14] * distanceFromCamera + projectionMatrix[15];

            // Transform viewport point into normalized device coordinates.
            const auto ndc = Vector4f((2.0f * pointX - 1.0f) * w, (1.0f - pointY * 2.0f) * w, z, w);
            return m_cameraToWorldMatrix.get(*this) * ndc;
        }

        Vector3f Camera::viewPortPointToDirection(float pointX, float pointY) const
        {
            const auto& projectionMatrix = getProjectionMatrix();

            const float z = -projectionMatrix[10];
            const float w = -projectionMatrix[14];

            // Transform viewport point into normalized device coordinates.
            const auto ndc = Vector4f((2.0f * pointX - 1.0f) * w, (1.0f - pointY * 2.0f) * w, z, w);
            return m_cameraToWorldMatrix.get(*this) * ndc;
        }

        void Camera::setDirty()
        {
            m_worldToCameraMatrix.setDirty();
            m_cameraToWorldMatrix.setDirty();
        }

        void Camera::recalculateWorldToCameraMatrix(Matrix4f& matrixOut) const
        {
            matrixOut = m_projectionMatrix * m_viewMatrix;
        }

        void Camera::recalculateCameraToWorldMatrix(Matrix4f& matrixOut) const
        {
            matrixOut = m_viewMatrixInverse * m_projectionMatrixInverse;
        }
    }  // namespace core
}  // namespace cg
