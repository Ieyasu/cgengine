#include "CGCore/Components/OrthographicCamera.h"

namespace cg
{
    namespace core
    {
        OrthographicCamera::OrthographicCamera()
            : Camera(0.1f, 100.0f, 16.0f / 9.0f)
        {
            setWidth(100.0f);
        }

        void OrthographicCamera::setWidth(float width)
        {
            m_height = width / getAspectRatio();
        }

        void OrthographicCamera::setHeight(float height)
        {
            m_height = height;
        }

        float OrthographicCamera::getWidth() const
        {
            return m_height * getAspectRatio();
        }

        float OrthographicCamera::getHeight() const
        {
            return m_height;
        }

        void OrthographicCamera::calculateProjectionMatrix()
        {
            const float width = getWidth();
            const float height = getHeight();
            const float near = getNearPlane();
            const float far = getFarPlane();

            const float nminusf = near - far;
            const float nplusf = near + far;
            const float div = 1.0f / (near - far);

            auto projectionMatrix = Matrix4f(
                    1.0f / width,
                    0,
                    0,
                    0,
                    0,
                    1.0f / height,
                    0,
                    0,
                    0,
                    0,
                    2.0f * div,
                    nplusf * div,
                    0,
                    0,
                    0,
                    1);

            auto projectionMatrixInverse = Matrix4f(
                    width,
                    0,
                    0,
                    0,
                    0,
                    height,
                    0,
                    0,
                    0,
                    0,
                    nminusf * 0.5f,
                    -nplusf * 0.5f,
                    0,
                    0,
                    0,
                    1);

            setProjectionMatrix(projectionMatrix, projectionMatrixInverse);
        }
    }  // namespace core
}  // namespace cg
