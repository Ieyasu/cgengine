#include "CGCore/Components/PerspectiveCamera.h"

#include "CGMath/Common.h"

namespace cg
{
    using math::angleToRadian;

    namespace core
    {
        PerspectiveCamera::PerspectiveCamera()
            : Camera(0.1f, 100.0f, 16.0f / 9.0f)
        {
            setFovHorizontal(60.0f);
        }

        void PerspectiveCamera::setFovVertical(float fovV)
        {
            setFovVerticalRad(angleToRadian(fovV));
        }

        void PerspectiveCamera::setFovVerticalRad(float fovV)
        {
            if (fovV >= CG_PI || fovV <= 0)
            {
                return;
            }

            m_fovVertical = fovV;
        }

        void PerspectiveCamera::setFovHorizontal(float fovH)
        {
            setFovHorizontalRad(angleToRadian(fovH));
        }

        void PerspectiveCamera::setFovHorizontalRad(float fovH)
        {
            if (fovH >= CG_PI || fovH <= 0)
            {
                return;
            }

            m_fovVertical = 2 * atan(tan(fovH / 2.0f) / getAspectRatio());
        }

        float PerspectiveCamera::getFovVertical() const
        {
            return m_fovVertical;
        }

        float PerspectiveCamera::getFovHorizontal() const
        {
            return 2 * atan(tan(m_fovVertical / 2.0f) * getAspectRatio());
        }

        void PerspectiveCamera::calculateProjectionMatrix()
        {
            const float fovTanH = tan(getFovHorizontal() / 2.0f);
            const float fovTanV = tan(getFovVertical() / 2.0f);
            const float near = getNearPlane();
            const float far = getFarPlane();

            const float nminusf = near - far;
            const float nplusf = near + far;
            const float nf2 = 2.0f * near * far;
            const float div1 = 1.0f / nminusf;
            const float div2 = 1.0f / nf2;

            auto projectionMatrix = Matrix4f(
                    1.0f / fovTanH,
                    0,
                    0,
                    0,
                    0,
                    1.0f / fovTanV,
                    0,
                    0,
                    0,
                    0,
                    nplusf * div1,
                    nf2 * div1,
                    0,
                    0,
                    -1,
                    0);

            auto projectionMatrixInverse = Matrix4f(
                    fovTanH,
                    0,
                    0,
                    0,
                    0,
                    fovTanV,
                    0,
                    0,
                    0,
                    0,
                    0,
                    -1,
                    0,
                    0,
                    nminusf * div2,
                    nplusf * div2);

            setProjectionMatrix(projectionMatrix, projectionMatrixInverse);
        }
    }  // namespace core
}  // namespace cg
