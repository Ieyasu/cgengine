#include "CGCore/Context.h"

#include "CGUtility/IO/LoggerConsole.h"

namespace cg
{
    namespace core
    {
        namespace context
        {
            Time        time;
            Window      window;
            InputMapper inputMapper;
            bool        initialized = false;

            bool initialize(int windowWidth, int windowHeight, const std::string& windowTitle)
            {
                if (initialized)
                {
                    CG_LOG_WARNING("CoreContext already initialized.")
                    return true;
                }

                // Initialize GLFW.
                glfwInit();

                // Open Window (wrapper for GLFWwindow).
                if (!window.open(windowWidth, windowHeight, windowTitle, false))
                {
                    CG_LOG_ERROR("Failed to open Window.");
                    return false;
                }

                // Initialize input.
                if (!inputMapper.initialize(window))
                {
                    CG_LOG_ERROR("Failed to initialize Input.");
                    return false;
                }

                // Initialize GLEW.
                glewExperimental = GL_TRUE;
                if (glewInit() != GLEW_OK)
                {
                    CG_LOG_ERROR("Failed to initialize GLEW.");
                    uninitialize();
                    return false;
                }

                time.initialize();
                initialized = true;

                CG_LOG_VERBOSE("Initialized CoreContext.");
                return true;
            }

            void uninitialize()
            {
                if (!initialized)
                {
                    CG_LOG_WARNING("Can't uninitialize an uninitialized CoreContext.")
                    return;
                }

                glfwTerminate();
                initialized = false;

                CG_LOG_VERBOSE("Uninitialized CoreContext.");
            }

            void update()
            {
                if (!initialized)
                {
                    CG_LOG_ERROR("Can't update CoreContext before it's initialized.")
                    return;
                }

                time.update();
                inputMapper.update();
            }

            bool isAvailable()
            {
                return window.isOpen();
            }

            const Time& getTime()
            {
                return time;
            }

            const Window& getWindow()
            {
                return window;
            }

            const InputMapper& getInput()
            {
                return inputMapper;
            }
        }  // namespace context
    }      // namespace core
}  // namespace cg
