#include "CGCore/Mesh/ComplexMesh.h"

#include <cassert>

#include "CGUtility/IO/LoggerConsole.h"

namespace cg
{
    namespace core
    {
        void ComplexMesh::clearSubMeshes()
        {
            checkEditMode();

            m_subMeshes.clear();
        }

        void ComplexMesh::addFace(const Face& face, size_t subMesh)
        {
            checkEditMode();

            resizeSubMeshes(subMesh + 1);
            auto& subMeshFaces = m_subMeshes[subMesh].faces;
            subMeshFaces.push_back(face);
        }

        void ComplexMesh::addFaces(const std::vector<Face>& faces, size_t subMesh)
        {
            checkEditMode();

            resizeSubMeshes(subMesh + 1);
            auto& subMeshFaces = m_subMeshes[subMesh].faces;
            subMeshFaces.insert(subMeshFaces.end(), faces.begin(), faces.end());
        }

        void ComplexMesh::setFace(const Face& face, size_t subMesh, size_t i)
        {
            checkEditMode();

            resizeSubMeshes(subMesh + 1);
            auto& subMeshFaces = m_subMeshes[subMesh].faces;
            assert(i < subMeshFaces.size());
            subMeshFaces[i] = face;
        }

        void ComplexMesh::setFaces(const std::vector<Face>& faces, size_t subMesh)
        {
            checkEditMode();

            resizeSubMeshes(subMesh + 1);
            auto& subMeshFaces = m_subMeshes[subMesh].faces;
            subMeshFaces = faces;
        }

        void ComplexMesh::setMaterial(const Material& material, size_t subMesh)
        {
            checkEditMode();

            resizeSubMeshes(subMesh + 1);
            m_subMeshes[subMesh].material = material;
        }

        size_t ComplexMesh::getSubMeshCount() const
        {
            return m_subMeshes.size();
        }

        const Material& ComplexMesh::getMaterial(size_t subMesh) const
        {
            assert(subMesh < m_subMeshes.size());
            return m_subMeshes[subMesh].material;
        }

        const std::vector<Face>& ComplexMesh::getFaces(size_t subMesh) const
        {
            assert(subMesh < m_subMeshes.size());
            return m_subMeshes[subMesh].faces;
        }

        std::unique_ptr<TriangleMesh> ComplexMesh::triangulate() const
        {
            auto triangleMesh = std::make_unique<TriangleMesh>();
            triangleMesh->beginEdit();
            triangleMesh->setPositions(m_positions);
            triangleMesh->setNormals(m_normals);
            triangleMesh->setUVs(m_uvs);

            for (size_t i = 0; i < m_subMeshes.size(); ++i)
            {
                for (const auto& face : m_subMeshes[i].faces)
                {
                    const std::vector<VertexIndex>& faceIndices = face.getIndices();
                    if (faceIndices.size() == 3)
                    {
                        triangleMesh->addTriangle(Triangle(faceIndices[0], faceIndices[1], faceIndices[2]), i);
                    }
                    else if (faceIndices.size() == 4)
                    {
                        triangleMesh->addTriangle(Triangle(faceIndices[0], faceIndices[1], faceIndices[2]), i);
                        triangleMesh->addTriangle(Triangle(faceIndices[0], faceIndices[2], faceIndices[3]), i);
                    }
                    else
                    {
                        CG_LOG_WARNING(
                                "N-gon triangulation (" + std::to_string(faceIndices.size()) + ") not yet supported.");
                    }
                }
                triangleMesh->setMaterial(m_subMeshes[i].material, i);
            }

            triangleMesh->endEdit();
            return triangleMesh;
        }

        bool ComplexMesh::areSubMeshesValid() const
        {
            for (const auto& subMesh : m_subMeshes)
            {
                for (const auto& face : subMesh.faces)
                {
                    const auto& indices = face.getIndices();

                    if (indices.size() < 3)
                    {
                        return false;
                    }
                    for (size_t i = 0; i < indices.size(); ++i)
                    {
                        if (indices[i] >= m_positions.size())
                        {
                            return false;
                        }
                    }
                }
            }
            return true;
        }

        void ComplexMesh::resizeSubMeshes(size_t n)
        {
            if (n > m_subMeshes.size())
            {
                m_subMeshes.resize(n);
            }
        }
    }  // namespace core
}  // namespace cg
