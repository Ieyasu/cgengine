#include "CGCore/Mesh/TriangleMesh.h"

#include <cassert>

namespace cg
{
    namespace core
    {
        void TriangleMesh::clearSubMeshes()
        {
            checkEditMode();

            m_subMeshes.clear();
        }

        void TriangleMesh::addTriangle(const Triangle& triangle, size_t subMesh)
        {
            checkEditMode();

            resizeSubMeshes(subMesh + 1);
            auto& subMeshTriangles = m_subMeshes[subMesh].triangles;
            subMeshTriangles.push_back(triangle);
        }

        void TriangleMesh::addTriangles(const std::vector<Triangle>& triangles, size_t subMesh)
        {
            checkEditMode();

            resizeSubMeshes(subMesh + 1);
            auto& subMeshTriangles = m_subMeshes[subMesh].triangles;
            subMeshTriangles.insert(subMeshTriangles.end(), triangles.begin(), triangles.end());
        }

        void TriangleMesh::setTriangle(const Triangle& triangle, size_t subMesh, size_t i)
        {
            checkEditMode();

            resizeSubMeshes(subMesh + 1);
            auto& subMeshTriangles = m_subMeshes[subMesh].triangles;
            assert(i < subMeshTriangles.size());
            subMeshTriangles[i] = triangle;
        }

        void TriangleMesh::setTriangles(const std::vector<Triangle>& triangles, size_t subMesh)
        {
            checkEditMode();

            resizeSubMeshes(subMesh + 1);
            auto& subMeshTriangles = m_subMeshes[subMesh].triangles;
            subMeshTriangles = triangles;
        }

        void TriangleMesh::setMaterial(const Material& material, size_t subMesh)
        {
            checkEditMode();

            resizeSubMeshes(subMesh + 1);
            m_subMeshes[subMesh].material = material;
        }

        size_t TriangleMesh::getSubMeshCount() const
        {
            return m_subMeshes.size();
        }

        const Material& TriangleMesh::getMaterial(size_t subMesh) const
        {
            assert(subMesh < m_subMeshes.size());
            return m_subMeshes[subMesh].material;
        }

        const std::vector<Triangle>& TriangleMesh::getTriangles(size_t subMesh) const
        {
            assert(subMesh < m_subMeshes.size());
            return m_subMeshes[subMesh].triangles;
        }

        bool TriangleMesh::areSubMeshesValid() const
        {
            for (const auto& subMesh : m_subMeshes)
            {
                for (const auto& triangle : subMesh.triangles)
                {
                    for (auto i = 0; i < 3; ++i)
                    {
                        if (triangle[i] >= m_positions.size())
                        {
                            return false;
                        }
                    }
                }
            }
            return true;
        }

        void TriangleMesh::resizeSubMeshes(size_t n)
        {
            if (n > m_subMeshes.size())
            {
                m_subMeshes.resize(n);
            }
        }
    }  // namespace core
}  // namespace cg
