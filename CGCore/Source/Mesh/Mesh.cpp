#include "CGCore/Mesh/Mesh.h"

#include <cassert>
#include <stdexcept>

namespace cg
{
    using math::Vector2f;
    using math::Vector3f;

    namespace core
    {
        void Face::setIndices(const std::vector<VertexIndex>& indices)
        {
            m_indices = indices;
        }

        const std::vector<VertexIndex>& Face::getIndices() const
        {
            return m_indices;
        }

        void Mesh::beginEdit()
        {
            m_inEditMode = true;
        }

        void Mesh::endEdit()
        {
            recalculateBounds();
            if (!areSubMeshesValid())
            {
                clearSubMeshes();
            }

            m_normals.resize(m_positions.size(), Vector3f(0, 1, 0));
            m_uvs.resize(m_positions.size(), Vector2f(0.0f));

            m_inEditMode = false;
        }

        void Mesh::clear()
        {
            checkEditMode();

            m_positions.clear();
            m_normals.clear();
            m_uvs.clear();
            clearSubMeshes();
        }

        void Mesh::normalize()
        {
            checkEditMode();

            Vector3f scale = Vector3f(1.0f / (m_bounds.getSize().max()));
            Vector3f translation = -m_bounds.getCenter();

            for (auto& position : m_positions)
            {
                position += translation;
                position *= scale;
            }
        }

        void Mesh::addPosition(const Vector3f& position)
        {
            checkEditMode();

            m_positions.push_back(position);
        }

        void Mesh::addNormal(const Vector3f& normal)
        {
            checkEditMode();

            m_normals.push_back(normal);
        }

        void Mesh::addUV(const Vector2f& uv)
        {
            checkEditMode();

            m_uvs.push_back(uv);
        }

        void Mesh::setPosition(const Vector3f& position, size_t i)
        {
            assert(i < m_positions.size());

            checkEditMode();

            m_positions[i] = position;
        }

        void Mesh::setNormal(const Vector3f& normal, size_t i)
        {
            assert(i < m_normals.size());

            checkEditMode();

            m_normals[i] = normal;
        }

        void Mesh::setUV(const Vector2f& uv, size_t i)
        {
            assert(i < m_uvs.size());

            checkEditMode();

            m_uvs[i] = uv;
        }

        void Mesh::setPositions(const std::vector<Vector3f>& positions)
        {
            checkEditMode();

            m_positions = positions;
        }

        void Mesh::setNormals(const std::vector<Vector3f>& normals)
        {
            checkEditMode();

            m_normals = normals;
        }

        void Mesh::setUVs(const std::vector<Vector2f>& uvs)
        {
            checkEditMode();

            m_uvs = uvs;
        }

        const std::vector<Vector3f>& Mesh::getPositions() const
        {
            return m_positions;
        }

        const std::vector<Vector3f>& Mesh::getNormals() const
        {
            return m_normals;
        }

        const std::vector<Vector2f>& Mesh::getUVs() const
        {
            return m_uvs;
        }

        const BoundsF& Mesh::getBounds() const
        {
            return m_bounds;
        }

        void Mesh::recalculateBounds()
        {
            checkEditMode();

            Vector3f min(FLT_MAX);
            Vector3f max(FLT_MIN);

            for (size_t i = 0; i < m_positions.size(); ++i)
            {
                const Vector3f& vertexPosition = m_positions[i];
                for (int j = 0; j < 3; ++j)
                {
                    if (vertexPosition[j] < min[j])
                        min[j] = vertexPosition[j];
                    if (vertexPosition[j] > max[j])
                        max[j] = vertexPosition[j];
                }
            }

            if (min.x > max.x || min.y > max.y || min.z > max.z)
            {
                min = Vector3f(0.0f);
                max = Vector3f(0.0f);
            }

            m_bounds.setMin(min);
            m_bounds.setMax(max);
        }

        void Mesh::checkEditMode()
        {
            if (!m_inEditMode)
            {
                throw std::runtime_error("Trying to edit Mesh while not in edit mode.");
            }
        }
    }  // namespace core
}  // namespace cg
