#include "CGCore/Mesh/MeshLoaderObj.h"

#include "CGCore/Material/Material.h"
#include "CGCore/Material/MaterialLoaderMtl.h"
#include "CGUtility/IO/FileBuffer.h"
#include "CGUtility/IO/UriHelpers.h"
#include "CGUtility/StringHelpers.h"

namespace cg
{
    using util::FileBuffer;

    namespace core
    {
        std::unique_ptr<ComplexMesh> MeshLoaderObj::loadComplexMesh(const std::string& filepath) const
        {
            std::unique_ptr<ComplexMesh> complexMesh;

            FileBuffer buffer;
            if (!buffer.readFile(filepath))
            {
                return complexMesh;
            }

            using FaceIndicesList = std::vector<std::vector<Vector3i>>;
            auto folder = util::getFolder(filepath);
            auto materialLoader = MaterialLoaderMtl();
            auto materials = std::vector<std::unique_ptr<Material>>();
            auto subMeshes = std::vector<std::pair<FaceIndicesList, Material>>(0);

            Material              activeMaterial;
            std::vector<Vector3f> positions;
            std::vector<Vector3f> normals;
            std::vector<Vector2f> uvs;
            Vector3f              position;
            Vector3f              normal;
            Vector2f              uv;

            // Parse .obj file.
            while (buffer.seekNextLine())
            {
                std::string line = buffer.getLine();

                // Invalid line. Skip.
                if (line.length() < 2)
                {
                    continue;
                }

                // Parse vertex data.
                if (line[0] == 'v')
                {
                    switch (line[1])
                    {
                            // Parse vertex position.
                        case ' ':
                            if (!readVector3f(line.c_str() + 2, position))
                            {
                                return complexMesh;
                            }
                            positions.push_back(position);
                            break;
                            // Parse vertex normal.
                        case 'n':
                            if (!readVector3f(line.c_str() + 2, normal))
                            {
                                return complexMesh;
                            }
                            normals.push_back(normal);
                            break;
                            // Parse vertex uv.
                        case 't':
                            if (!readVector2f(line.c_str() + 2, uv))
                            {
                                return complexMesh;
                            }
                            uvs.push_back(uv);
                            break;
                        default:
                            break;
                    }
                }
                else if (line[0] == 'f' && line[1] == ' ')  // Parse face.
                {
                    std::vector<Vector3i> faceIndices;
                    if (!readFace(line, faceIndices))
                    {
                        return complexMesh;
                    }

                    if (subMeshes.size() == 0)
                    {
                        subMeshes.resize(1);
                        subMeshes.back().second = activeMaterial;
                    }

                    subMeshes.back().first.push_back(faceIndices);
                }
                else if ((line[0] == 'o' && line[1] == ' ') || (line[0] == 'g' && line[1] == ' '))
                {
                    subMeshes.resize(subMeshes.size() + 1);
                    subMeshes.back().second = activeMaterial;
                }
                else if (line.compare(0, 7, "mtllib ", 0, 7) == 0)
                {
                    auto matFilepath = util::trim(line.substr(7));
                    auto matlib = materialLoader.loadMaterials(folder + "/" + matFilepath);
                    materials.reserve(materials.size() + matlib.size());
                    for (auto& material : matlib)
                    {
                        materials.push_back(std::move(material));
                    }
                }
                else if (line.compare(0, 7, "usemtl ", 0, 7) == 0)
                {
                    for (auto& material : materials)
                    {
                        auto matName = util::trim(line.substr(7));
                        if (material->getName() == matName)
                        {
                            activeMaterial = *material.get();
                            if (subMeshes.back().first.size() == 0)
                            {
                                subMeshes.back().second = activeMaterial;
                            }
                            break;
                        }
                    }
                }
            }

            std::vector<Vector3f> filledNormals(positions.size(), Vector3f(0, 1, 0));
            std::vector<Vector2f> filledUVs(positions.size());

            complexMesh = std::make_unique<ComplexMesh>();
            complexMesh->beginEdit();

            for (size_t m = 0; m < subMeshes.size(); ++m)
            {
                const auto&       subMesh = subMeshes[m];
                std::vector<Face> faces(subMesh.first.size());

                for (size_t i = 0; i < faces.size(); ++i)
                {
                    const std::vector<Vector3i>& faceIndices = subMesh.first[i];
                    std::vector<VertexIndex>     face(faceIndices.size());

                    for (size_t j = 0; j < face.size(); ++j)
                    {
                        const Vector3i& faceIndex = faceIndices[j];

                        if (faceIndex.x < 0 || faceIndex.x >= (int)positions.size())
                        {
                            return std::unique_ptr<ComplexMesh>(nullptr);
                        }

                        if (faceIndex.y >= 0 && faceIndex.y < (int)normals.size())
                        {
                            filledNormals[faceIndex.x] = normals[faceIndex.y];
                        }

                        if (faceIndex.z >= 0 && faceIndex.z < (int)uvs.size())
                        {
                            filledUVs[faceIndex.x] = uvs[faceIndex.z];
                        }

                        face[j] = (unsigned int)faceIndex.x;
                    }
                    faces[i].setIndices(face);
                }

                complexMesh->setFaces(faces, m);
                complexMesh->setMaterial(subMesh.second, m);
            }

            complexMesh->setPositions(positions);
            complexMesh->setNormals(filledNormals);
            complexMesh->setUVs(filledUVs);
            complexMesh->endEdit();
            return complexMesh;
        }

        std::unique_ptr<TriangleMesh> MeshLoaderObj::loadTriangleMesh(const std::string& filepath) const
        {
            std::unique_ptr<ComplexMesh> complexMesh = loadComplexMesh(filepath);
            if (complexMesh == nullptr)
            {
                return std::unique_ptr<TriangleMesh>(nullptr);
            }
            return complexMesh->triangulate();
        }

        bool MeshLoaderObj::readVector2f(const char* line, Vector2f& vecOut) const
        {
            if (!line)
            {
                return false;
            }
            const int readLen = sscanf(line, "%f %f", &vecOut.x, &vecOut.y);
            return readLen > 0;
        }

        bool MeshLoaderObj::readVector3f(const char* line, Vector3f& vecOut) const
        {
            if (!line)
            {
                return false;
            }
            const int readLen = sscanf(line, "%f %f %f", &vecOut.x, &vecOut.y, &vecOut.z);
            return readLen > 0;
        }

        bool MeshLoaderObj::readFace(std::string& line, std::vector<Vector3i>& faceOut) const
        {
            // The type of index currently being parsed.
            // 0 - is position, 1 - is uvs, 2 is normals.
            int indexType = 0;

            // The indices read for each type
            // x - position index, y - uv index, 2 - normal index.
            Vector3i indices(-1);

            for (size_t i = 0; i < line.length(); ++i)
            {
                // Skip initial white space.
                while (i < line.length() && !isdigit(line[i]))
                {
                    ++i;
                }

                // Reached end of line.
                if (i == line.length())
                {
                    return true;
                }

                const char* s = &line[i];

                // Find vertex delimiter and terminate the string there for conversion.
                while (i < line.length() - 1 && !isspace(line[i]))
                {
                    if (line[i] == '/')
                    {
                        line[i] = '\0';
                        indices[indexType] = atoi(s) - 1;
                        indexType = (indexType + 1) % 2;
                    }
                    i++;
                }

                indices[indexType] = atoi(s) - 1;

                // This time we shouldn't reach end of line...
                if (i == line.length() || indices.x < 0)
                {
                    faceOut.clear();
                    return false;
                }

                faceOut.push_back(indices);
                indices.x = indices.y = indices.z = -1;
                indexType = 0;
            }

            return true;
        }
    }  // namespace core
}  // namespace cg
