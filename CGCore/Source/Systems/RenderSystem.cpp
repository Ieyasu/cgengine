#define GLEW_STATIC
#include "CGCore/Systems/RenderSystem.h"

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include "CGCore/Context.h"
#include "CGUtility/IO/LoggerConsole.h"

namespace cg
{
    namespace core
    {
        RenderSystem::RenderSystem()
            : m_useOrthoCamera(false)
        {
        }

        void RenderSystem::onInitialize()
        {
            // Create camera.
            m_cameraEntity = m_world->createEntity();
            m_cameraTransform = m_cameraEntity->addComponent<Transform>();
            m_orthoCamera = m_cameraEntity->addComponent<OrthographicCamera>();
            m_perspectiveCamera = m_cameraEntity->addComponent<PerspectiveCamera>();
        }

        void RenderSystem::onUpdate()
        {
            if (context::getInput().isPressed(Keycode::KEY_Q))
            {
                m_useOrthoCamera = !m_useOrthoCamera;
            }

            // Configure camera.
            if (m_useOrthoCamera)
            {
                m_orthoCamera->setAspectRatio(context::getWindow().getAspectRatio());
                m_orthoCamera->setViewMatrix(*m_cameraTransform);
                m_orthoCamera->calculateProjectionMatrix();
                m_activeCamera = static_cast<Camera*>(m_orthoCamera.get());
            }
            else
            {
                m_perspectiveCamera->setAspectRatio(context::getWindow().getAspectRatio());
                m_perspectiveCamera->setViewMatrix(*m_cameraTransform);
                m_perspectiveCamera->calculateProjectionMatrix();
                m_activeCamera = static_cast<Camera*>(m_perspectiveCamera.get());
            }

            // Clear window.
            glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
            glClear(GL_COLOR_BUFFER_BIT);

            // Rendering is done in derived classes.
            render();

            // Swap front and back buffer.
            glfwSwapBuffers(context::getWindow().getRawPtr());
        }

        void RenderSystem::onDestroy()
        {
            glfwTerminate();
        }

        Handle<Entity> RenderSystem::getCameraEntity() const
        {
            return m_cameraEntity;
        }
    }  // namespace core
}  // namespace cg
