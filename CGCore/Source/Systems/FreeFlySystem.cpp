#include "CGCore/Systems/FreeFlySystem.h"

#include "CGCore/Context.h"
#include "CGUtility/IO/LoggerConsole.h"

namespace cg
{
    namespace core
    {
        FreeFlySystem::FreeFlySystem(Handle<Entity> entity)
            : m_entity(entity)
            , m_freeFly(entity->getComponent<FreeFly>())
            , m_transform(entity->getComponent<Transform>())
            , m_isRotating(false)
        {
            assert(entity.isValid());
            assert(entity->getComponent<FreeFly>().isValid());
            assert(entity->getComponent<Transform>().isValid());
        }

        void FreeFlySystem::onUpdate()
        {
            if (!m_entity.isValid() || !m_transform.isValid() || !m_freeFly.isValid())
            {
                CG_LOG_VERBOSE("FreeFlySystem: Attached FreeFly entity is not valid.");
                return;
            }

            rotate();
            move();
        }

        void FreeFlySystem::rotate()
        {
            if (context::getInput().isPressed(Keycode::KEY_MOUSE_2))
            {
                m_isRotating = true;
                m_dragStartRotation = m_transform->getLocalRotation();
                m_dragStartPos = context::getInput().getMousePosition();
            }
            else if (context::getInput().isReleased(Keycode::KEY_MOUSE_2))
            {
                m_isRotating = false;
            }

            if (!m_isRotating)
            {
                return;
            }

            const auto mouseCurrentPos = context::getInput().getMousePosition();
            const auto delta = mouseCurrentPos - m_dragStartPos;

            if (delta.x != 0 && delta.y != 0)
            {
                auto       euler = m_dragStartRotation.toEuler();
                const auto rotationSpeed = m_freeFly->getRotationSpeed();

                euler.x -= delta.y * rotationSpeed;
                euler.y -= delta.x * rotationSpeed;
                m_transform->setLocalRotation(euler);
            }
        }

        void FreeFlySystem::move()
        {
            const auto deltaTime = context::getTime().getDeltaTime();
            const auto movementSpeed = m_freeFly->getMovementSpeed();
            const auto deltaMove = (movementSpeed * deltaTime);

            if (context::getInput().isHeld(Keycode::KEY_W))
            {
                m_transform->translate(m_transform->getForward() * deltaMove);
            }
            if (context::getInput().isHeld(Keycode::KEY_S))
            {
                m_transform->translate(-m_transform->getForward() * deltaMove);
            }
            if (context::getInput().isHeld(Keycode::KEY_A))
            {
                m_transform->translate(-m_transform->getRight() * deltaMove);
            }
            if (context::getInput().isHeld(Keycode::KEY_D))
            {
                m_transform->translate(m_transform->getRight() * deltaMove);
            }
        }
    }  // namespace core
}  // namespace cg
