#include "CGCore/Components/FreeFly.h"

namespace cg
{
    namespace core
    {
        void FreeFly::setMovementSpeed(float movementSpeed)
        {
            m_movementSpeed = movementSpeed;
        }

        void FreeFly::setRotationSpeed(float rotationSpeed)
        {
            m_rotationSpeed = rotationSpeed;
        }

        float FreeFly::getMovementSpeed() const
        {
            return m_movementSpeed;
        }

        float FreeFly::getRotationSpeed() const
        {
            return m_rotationSpeed;
        }
    }  // namespace core
}  // namespace cg
