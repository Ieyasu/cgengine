#include "CGCore/Time.h"

namespace cg
{
    namespace core
    {
        using std::chrono::duration_cast;
        using std::chrono::nanoseconds;

        Time::Time()
            : Time(1.0f / 3.0f)
        {
        }

        Time::Time(float maxDeltaTime)
            : m_maxDeltaTime(maxDeltaTime)
        {
        }

        void Time::initialize()
        {
            m_startTime = high_resolution_clock::now();
            m_lastTime = high_resolution_clock::now();
        }

        void Time::update()
        {
            auto now = high_resolution_clock::now();

            m_frameCount++;
            m_deltaTime = duration_cast<nanoseconds>(now - m_lastTime).count() / 1000000000.0f;
            m_timeSinceInitialization = duration_cast<nanoseconds>(now - m_startTime).count() / 1000000000.0f;

            m_lastTime = now;

            if (m_deltaTime > m_maxDeltaTime)
            {
                m_deltaTime = m_maxDeltaTime;
            }
        }

        void Time::setMaxDeltaTime(float maxDeltaTime)
        {
            m_maxDeltaTime = maxDeltaTime;
        }

        float Time::getTime() const
        {
            return m_timeSinceInitialization;
        }

        float Time::getDeltaTime() const
        {
            return m_deltaTime;
        }

        float Time::getMaxDeltaTime() const
        {
            return m_maxDeltaTime;
        }

        unsigned int Time::getFrameCount() const
        {
            return m_frameCount;
        }
    }  // namespace core
}  // namespace cg
