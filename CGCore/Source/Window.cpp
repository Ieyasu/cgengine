#include "CGCore/Window.h"

#include <cassert>

#include "CGUtility/IO/LoggerConsole.h"

namespace cg
{
    namespace core
    {
        Window::Window()
            : m_title("")
            , m_isFullScreen(false)
            , m_glWindow(nullptr)
        {
        }

        Window::Window(int width, int height, const std::string& title, bool fullScreen)
            : Window()
        {
            open(width, height, title, fullScreen);
        }

        Window::~Window()
        {
            assert(m_glWindow != nullptr);

            glfwDestroyWindow(m_glWindow);
            m_glWindow = nullptr;
        }

        bool Window::open(int width, int height, const std::string& title, bool fullScreen)
        {
            assert(width > 0);
            assert(height > 0);
            assert(title != "");

            if (m_glWindow)
            {
                CG_LOG_ERROR("Can't open Window. Already open.");
                return false;
            }

            // Set window hints.
            glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, CG_GLFW_CONTEXT_VERSION_MAJOR);
            glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, CG_GLFW_CONTEXT_VERSION_MINOR);
            glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
            glfwWindowHint(GLFW_RESIZABLE, GL_TRUE);

            // Create window.
            m_glWindow = glfwCreateWindow(width, height, title.c_str(), fullScreen ? nullptr : nullptr, nullptr);
            if (m_glWindow == nullptr)
            {
                CG_LOG_ERROR("Failed to create GLFW window.");
                return false;
            }

            m_isFullScreen = fullScreen;
            m_title = title;

            glfwMakeContextCurrent(m_glWindow);

            // Set viewport.
            glViewport(0, 0, width, height);
            return true;
        }

        bool Window::isOpen() const
        {
            return m_glWindow != nullptr && !glfwWindowShouldClose(m_glWindow);
        }

        bool Window::isFullScreen() const
        {
            assert(m_glWindow != nullptr);

            return m_isFullScreen;
        }

        int Window::getWidth() const
        {
            assert(m_glWindow != nullptr);

            int width, height;
            glfwGetWindowSize(m_glWindow, &width, &height);
            return width;
        }

        int Window::getHeight() const
        {
            assert(m_glWindow != nullptr);

            int width, height;
            glfwGetWindowSize(m_glWindow, &width, &height);
            return height;
        }

        float Window::getAspectRatio() const
        {
            return (float)getWidth() / (float)getHeight();
        }

        const std::string& Window::getTitle() const
        {
            assert(m_glWindow != nullptr);

            return m_title;
        }

        GLFWwindow* Window::getRawPtr() const
        {
            return m_glWindow;
        }
    }  // namespace core
}  // namespace cg
