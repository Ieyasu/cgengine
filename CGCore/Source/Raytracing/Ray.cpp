#include "CGCore/Raytracing/Ray.h"

namespace cg
{
    using math::Vector3f;

    namespace core
    {
        Ray::Ray()
        {
        }

        Ray::Ray(const Vector3f& origin, const Vector3f& direction)
            : origin(origin)
            , direction(direction)
        {
        }

        Ray::Ray(float origX, float origY, float origZ, float dirX, float dirY, float dirZ)
            : origin(origX, origY, origZ)
            , direction(dirX, dirY, dirZ)
        {
        }

        Vector3f Ray::calculatePos(float t) const
        {
            return origin + direction * t;
        }
    }  // namespace core
}  // namespace cg
