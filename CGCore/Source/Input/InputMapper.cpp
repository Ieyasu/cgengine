#include "CGCore/Input/InputMapper.h"

#include <cassert>

#include "CGUtility/IO/LoggerConsole.h"

namespace cg
{
    namespace core
    {
        bool InputMapper::initialize(const Window& window)
        {
            registerWindow(window);
            registerInputMapper(*this);
            m_window = &window;
            return true;
        }

        void InputMapper::update()
        {
            readInputBuffer();
            glfwPollEvents();
        }

        void InputMapper::readInputBuffer()
        {
            for (auto& pair : m_buttonStateBuffer)
            {
                auto  keyCode = pair.first;
                auto& buffer = pair.second;
                auto& buttonState = m_buttonStates[keyCode];

                if (buffer.size() != 0)
                {
                    // Get next buffered state.
                    buttonState = buffer.front();
                    buffer.pop();
                }
                else if (buttonState == ButtonState::PRESSED)
                {
                    // 'Pressed' button becomes 'held' after one frame.
                    buttonState = ButtonState::HELD;
                }
                else if (buttonState == ButtonState::RELEASED)
                {
                    // 'Released' button becomes 'free' after one frame.
                    buttonState = ButtonState::FREE;
                }
            }
        }

        Vector2f InputMapper::getMousePosition() const
        {
            assert(m_window != nullptr);
            assert(m_window->getRawPtr() != nullptr);

            double x, y;
            glfwGetCursorPos(m_window->getRawPtr(), &x, &y);
            return Vector2f((float)x, (float)y);
        }

        bool InputMapper::isPressed(Keycode keycode) const
        {
            return isButtonState(keycode, ButtonState::PRESSED);
        }

        bool InputMapper::isHeld(Keycode keycode) const
        {
            return isButtonState(keycode, ButtonState::HELD);
        }

        bool InputMapper::isReleased(Keycode keycode) const
        {
            return isButtonState(keycode, ButtonState::RELEASED);
        }

        bool InputMapper::isButtonState(Keycode keycode, ButtonState buttonState) const
        {
            auto it = m_buttonStates.find(keycode);
            if (it == m_buttonStates.end())
            {
                CG_LOG_VERBOSE("No button with keycode " + std::to_string((int)keycode) + " found.");
                return false;
            }

            return it->second == buttonState;
        }

        void InputMapper::mouseCallback(GLFWwindow* window, int button, int action, int /*mods*/)
        {
            assert(m_window != nullptr);

            // Only receive events from own Window.
            if (window != m_window->getRawPtr())
            {
                return;
            }

            int key = (button + GLFW_KEY_LAST + 1);
            updateButtonState(key, action);
        }

        void InputMapper::keyCallback(GLFWwindow* window, int key, int /*scancode*/, int action, int /*mode*/)
        {
            assert(m_window != nullptr);

            // Only receive events from own Window.
            if (window != m_window->getRawPtr())
            {
                return;
            }

            updateButtonState(key, action);
        }

        void InputMapper::updateButtonState(int key, int action)
        {
            auto& buffer = m_buttonStateBuffer[(Keycode)key];

            switch (action)
            {
                case GLFW_PRESS:
                    buffer.push(ButtonState::PRESSED);
                    break;
                case GLFW_RELEASE:
                    buffer.push(ButtonState::RELEASED);
                    break;
            }
        }

        // Static functions for GLFW input registering.
        std::set<InputMapper*>  InputMapper::s_inputMappers;
        std::set<const Window*> InputMapper::s_windows;

        void InputMapper::registerWindow(const Window& window)
        {
            // Only register for a window once.
            auto it = s_windows.find(&window);
            if (it == s_windows.end())
            {
                glfwSetMouseButtonCallback(window.getRawPtr(), globalMouseCallback);
                glfwSetKeyCallback(window.getRawPtr(), globalKeyCallback);
                s_windows.insert(&window);
                CG_LOG_VERBOSE("Registered window " + window.getTitle() + " to send input events.");
            }
        }

        void InputMapper::registerInputMapper(InputMapper& inputMapper)
        {
            s_inputMappers.insert(&inputMapper);
            CG_LOG_VERBOSE("Registered InputMapper to receive input events.");
        }

        void InputMapper::unregisterInputMapper(InputMapper& inputMapper)
        {
            auto it = s_inputMappers.find(&inputMapper);
            if (it != s_inputMappers.end())
            {
                s_inputMappers.erase(it);
                CG_LOG_VERBOSE("Unregistered InputMapper from receiving input events.");
            }
        }

        void InputMapper::globalMouseCallback(GLFWwindow* window, int button, int action, int mods)
        {
            for (auto inputMapper : s_inputMappers)
            {
                assert(inputMapper != nullptr);
                inputMapper->mouseCallback(window, button, action, mods);
            }
        }

        void InputMapper::globalKeyCallback(GLFWwindow* window, int key, int scancode, int action, int mode)
        {
            for (auto inputMapper : s_inputMappers)
            {
                assert(inputMapper != nullptr);
                inputMapper->keyCallback(window, key, scancode, action, mode);
            }
        }
    }  // namespace core
}  // namespace cg
