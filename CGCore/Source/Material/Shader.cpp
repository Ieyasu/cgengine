#include "CGCore/Material/Shader.h"

#include <vector>

#include "CGUtility/IO/FileBuffer.h"
#include "CGUtility/IO/LoggerConsole.h"

namespace cg
{
    using util::FileBuffer;

    namespace core
    {
        Shader::Shader()
            : m_shaderProgram(0)
        {
        }

        Shader::Shader(Shader&& other)
        {
            m_shaderProgram = other.m_shaderProgram;
            other.m_shaderProgram = 0;
            m_vertexShaderFilepath = std::string(std::move(other.m_vertexShaderFilepath));
            m_fragmentShaderFilepath = std::string(std::move(other.m_fragmentShaderFilepath));
        }

        Shader::Shader(const Shader& other)
        {
            compile(other.m_vertexShaderFilepath, other.m_fragmentShaderFilepath);
        }

        Shader& Shader::operator=(const Shader& other)
        {
            Shader tmp(other);
            *this = std::move(tmp);
            return *this;
        }

        Shader& Shader::operator=(Shader&& other)
        {
            m_shaderProgram = other.m_shaderProgram;
            other.m_shaderProgram = 0;
            m_vertexShaderFilepath = std::string(std::move(other.m_vertexShaderFilepath));
            m_fragmentShaderFilepath = std::string(std::move(other.m_fragmentShaderFilepath));
            return *this;
        }

        Shader::Shader(const std::string& vertexFilepath, const std::string& fragmentFilepath)
            : Shader()
        {
            compile(vertexFilepath, fragmentFilepath);
        }

        Shader::~Shader()
        {
            glDeleteProgram(m_shaderProgram);
            m_shaderProgram = 0;
        }

        Shader::Program Shader::getProgram() const
        {
            return m_shaderProgram;
        }

        bool Shader::compile(const std::string& vertexFilepath, const std::string& fragmentFilepath)
        {
            m_vertexShaderFilepath = vertexFilepath;
            m_fragmentShaderFilepath = fragmentFilepath;

            if (m_vertexShaderFilepath == "" || m_fragmentShaderFilepath == "")
            {
                return false;
            }

            // Create and compile shaders.
            GLuint vertexShader = glCreateShader(GL_VERTEX_SHADER);
            bool   vertexCompileSuccess = compileShader(vertexShader, vertexFilepath);
            GLuint fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
            bool   fragmentCompileSuccess = compileShader(fragmentShader, fragmentFilepath);

            // Delete old program and create new.
            glDeleteProgram(m_shaderProgram);
            m_shaderProgram = glCreateProgram();
            bool linkSuccess = linkShaders(m_shaderProgram, vertexShader, fragmentShader);

            // Delete shaders, don't need them after program is linked.
            glDeleteShader(vertexShader);
            glDeleteShader(fragmentShader);

            // If failed, delete program.
            if (!vertexCompileSuccess || !fragmentCompileSuccess || !linkSuccess)
            {
                glDeleteProgram(m_shaderProgram);
                m_shaderProgram = 0;
                m_vertexShaderFilepath = "";
                m_fragmentShaderFilepath = "";
                return false;
            }

            return true;
        }

        bool Shader::linkShaders(GLuint shaderProgram, GLuint vertexShader, GLuint fragmentShader)
        {
            // Link shaders to shader program.
            glAttachShader(shaderProgram, vertexShader);
            glAttachShader(shaderProgram, fragmentShader);
            glLinkProgram(shaderProgram);
            glDetachShader(shaderProgram, vertexShader);
            glDetachShader(shaderProgram, fragmentShader);

            // Check for linking errors.
            GLint success = 0;
            glGetProgramiv(shaderProgram, GL_LINK_STATUS, &success);
            if (!success)
            {
                // Initialize error log buffer.
                GLint errorLogLength = 0;
                glGetProgramiv(shaderProgram, GL_INFO_LOG_LENGTH, &errorLogLength);
                std::vector<GLchar> errorLog(errorLogLength + 1);

                glGetProgramInfoLog(shaderProgram, errorLogLength, nullptr, &errorLog[0]);
                CG_LOG_ERROR("Shader linking failed: " + std::string(errorLog.data()));
                return false;
            }

            // Validate.
            success = 0;
            glValidateProgram(shaderProgram);
            glGetProgramiv(shaderProgram, GL_VALIDATE_STATUS, &success);
            if (!success)
            {
                // Initialize error log buffer.
                GLint errorLogLength = 0;
                glGetProgramiv(shaderProgram, GL_INFO_LOG_LENGTH, &errorLogLength);
                std::vector<GLchar> errorLog(errorLogLength + 1);

                glGetProgramInfoLog(shaderProgram, errorLogLength, nullptr, &errorLog[0]);
                CG_LOG_ERROR("Shader validation failed.");
                return false;
            }

            return true;
        }

        bool Shader::compileShader(GLuint shader, const std::string& filepath)
        {
            std::string shaderSource = "";

            // Load shader.
            if (!loadShader(filepath, shaderSource))
            {
                CG_LOG_ERROR("Loading shader from file '" + filepath + "' failed.");
                return false;
            }

            // Compile shader.
            const auto source = shaderSource.c_str();
            glShaderSource(shader, 1, &source, nullptr);
            glCompileShader(shader);

            // Check for errors.
            GLint success = 0;
            glGetShaderiv(shader, GL_COMPILE_STATUS, &success);
            if (!success)
            {
                GLint errorLogLength = 0;
                glGetProgramiv(shader, GL_INFO_LOG_LENGTH, &errorLogLength);
                std::vector<GLchar> errorLog(errorLogLength + 1);

                glGetShaderInfoLog(shader, errorLogLength, nullptr, &errorLog[0]);
                CG_LOG_ERROR("Shader compilation from file '" + filepath + "' failed: " + std::string(errorLog.data()));
                return false;
            }

            return true;
        }

        bool Shader::loadShader(const std::string& filepath, std::string& source)
        {
            auto fileBuffer = FileBuffer();
            bool success = fileBuffer.readFile(filepath);
            if (!success)
            {
                return false;
            }
            source = fileBuffer.getAll();
            return true;
        }
    }  // namespace core
}  // namespace cg
