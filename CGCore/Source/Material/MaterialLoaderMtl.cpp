#include "CGCore/Material/MaterialLoaderMtl.h"

#include <cctype>

#include "CGMath/Common.h"
#include "CGUtility/StringHelpers.h"

namespace cg
{
    using math::clamp;

    namespace core
    {
        std::vector<std::unique_ptr<Material>> MaterialLoaderMtl::loadMaterials(const std::string& filepath) const
        {
            /*
             * Not handled:
             * 	- Tf: filter
             *	- Ka: ambient color
             *	- Ni: index of refraction
             *	- illum: illumination mode
             *	- sharpness: sharpness of reflections
             *	- Textures
             */

            std::vector<std::unique_ptr<Material>> materials;
            Vector3f                               diffuseColor;
            Vector3f                               specularColor;
            float                                  specularExponent = 0.0f;
            float                                  transparency = 1.0f;

            FileBuffer buffer;
            if (!buffer.readFile(filepath))
            {
                return materials;
            }

            while (buffer.seekNextLine())
            {
                std::string line = util::ltrim(buffer.getLine());

                // Invalid line. Skip.
                if (line.length() < 2)
                {
                    continue;
                }

                if (line.compare(0, 7, "newmtl ", 0, 7) == 0)
                {
                    Vector3f diffuseColor = Vector3f(1.0f);
                    Vector3f specularColor = Vector3f(1.0f);

                    auto name = util::trim(line.substr(7));
                    materials.push_back(std::move(std::make_unique<Material>(name)));
                }
                else if (line[0] == 'K')
                {
                    switch (line[1])
                    {
                            // Parse diffuse color.
                        case 'd':
                            if (!readVector3f(line.c_str() + 2, diffuseColor))
                            {
                                return materials;
                            }
                            materials.back()->setUniformVector4(
                                    Vector4f(diffuseColor.x, diffuseColor.y, diffuseColor.z, transparency),
                                    "_MainColor");
                            break;
                            // Parse specular color.
                        case 's':
                            if (!readVector3f(line.c_str() + 2, specularColor))
                            {
                                return materials;
                            }
                            materials.back()->setUniformVector4(
                                    Vector4f(specularColor.x, specularColor.y, specularColor.z, 1.0f),
                                    "_SpecularColor");
                            break;
                        default:
                            break;
                    }
                }
                else if ((line[0] == 'd' && line[1] == ' ') || (line[0] == 'T' && line[1] == 'r'))
                {
                    // Parse transparency.
                    if (!readFloat(line.c_str() + 2, transparency))
                    {
                        return materials;
                    }
                    transparency = clamp(transparency, 0.0f, 1.0f);
                    materials.back()->setUniformVector4(
                            Vector4f(diffuseColor.x, diffuseColor.y, diffuseColor.z, transparency),
                            "_MainColor");
                }
                else if (line[0] == 'N' && line[1] == 's')
                {
                    // Parse specular exponent.
                    if (!readFloat(line.c_str() + 2, specularExponent))
                    {
                        return materials;
                    }
                    specularExponent = clamp(specularExponent, 0.0f, 1000.0f);
                    materials.back()->setUniformFloat(specularExponent, "_SpecularExponent");
                }
            }

            return materials;
        }

        bool MaterialLoaderMtl::readFloat(const char* line, float& valOut) const
        {
            if (!line)
            {
                return false;
            }
            const int readLen = sscanf(line, "%f", &valOut);
            return readLen > 0;
        }

        bool MaterialLoaderMtl::readVector3f(const char* line, Vector3f& vecOut) const
        {
            if (!line)
            {
                return false;
            }
            const int readLen = sscanf(line, "%f %f %f", &vecOut.x, &vecOut.y, &vecOut.z);
            return readLen > 0;
        }
    }  // namespace core
}  // namespace cg
