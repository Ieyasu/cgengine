#include "CGCore/Material/Material.h"

#include "CGUtility/IO/LoggerConsole.h"

namespace cg
{
    namespace core
    {
        Material::Material()
            : m_name(generateNewName())
        {
            setToDefault();
        }

        Material::Material(Shader shader)
            : m_name(generateNewName())
            , m_shader(shader)
        {
        }

        Material::Material(const std::string& name)
            : m_name(name)
        {
            setToDefault();
        }

        void Material::setToDefault()
        {
            m_uniformFloats.clear();
            m_uniformVector2s.clear();
            m_uniformVector3s.clear();
            m_uniformVector4s.clear();
            m_uniformTexture2Ds.clear();
            setUniformVector4(Vector4f(0.9f, 0.9f, 0.9f, 1.0f), "_MainColor");
        }

        void Material::use()
        {
            glUseProgram(m_shader.getProgram());
        }

        void Material::setUniformFloat(float val, const std::string& name)
        {
            m_uniformFloats[name] = val;
        }

        void Material::setUniformVector2(const Vector2f& vec, const std::string& name)
        {
            m_uniformVector2s[name] = vec;
        }

        void Material::setUniformVector3(const Vector3f& vec, const std::string& name)
        {
            m_uniformVector3s[name] = vec;
        }

        void Material::setUniformVector4(const Vector4f& vec, const std::string& name)
        {
            m_uniformVector4s[name] = vec;
        }

        void Material::setUniformTexture2D(GLuint textureId, const std::string& name)
        {
            m_uniformTexture2Ds[name] = textureId;
        }

        float Material::getUniformFloat(const std::string& name) const
        {
            return m_uniformFloats.find(name)->second;
        }

        const Vector2f& Material::getUniformVector2(const std::string& name) const
        {
            return m_uniformVector2s.find(name)->second;
        }

        const Vector3f& Material::getUniformVector3(const std::string& name) const
        {
            return m_uniformVector3s.find(name)->second;
        }

        const Vector4f& Material::getUniformVector4(const std::string& name) const
        {
            return m_uniformVector4s.find(name)->second;
        }

        GLuint Material::getUniformTexture2D(const std::string& name) const
        {
            return m_uniformTexture2Ds.find(name)->second;
        }

        const Shader& Material::getShader() const
        {
            return m_shader;
        }

        const std::string& Material::getName() const
        {
            return m_name;
        }

        std::string Material::generateNewName()
        {
            static int nameIndex = 0;
            return "Material" + std::to_string(nameIndex++);
        }
    }  // namespace core
}  // namespace cg
