#pragma once

#include <string>
#include <algorithm>
#include <functional> 
#include <cctype>

namespace cg
{
	namespace util
	{
		inline std::string ltrim(const std::string& str) 
		{
			auto it = str.begin();
			for (auto end = str.end(); it != end; ++it)
			{
				if (!std::isspace(*it) && *it != '\0')
				{
					break;
				}
			}
			return std::string(it, str.end());
		}

		inline std::string rtrim(const std::string& str) 
		{
			int i = (int)str.size() - 1;
			for (; i >= 0; --i)
			{
				if (!std::isspace(str[i]) && str[i] != '\0')
				{
					break;
				}
			}
			return std::string(str.begin(), str.begin() + i + 1);
		}

		inline std::string trim(const std::string& str) 
		{
			return ltrim(rtrim(str));
		}
	}
}