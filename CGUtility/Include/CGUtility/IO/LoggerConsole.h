#pragma once

#include "Logger.h"

namespace cg
{
    namespace util
    {
        class LoggerConsole : public Logger
        {
        public:
            LoggerConsole() = default;
            LoggerConsole(LogPriority logPriority);
            virtual void log(const std::string& str, LogPriority logPriority) override;
            virtual void logLine(const std::string& str, LogPriority logPriority) override;
        };

    }  // namespace util
}  // namespace cg

// #ifdef CG_DEBUG
static cg::util::LoggerConsole GlobalLoggerConsole = cg::util::LoggerConsole();
#define CG_LOG_PREFIX \
    (std::string(__FILE__) + ", " + std::string(__func__) + "(), line " + std::to_string(__LINE__) + ":\n")
#define CG_LOG(STR) (GlobalLoggerConsole.logLine((STR), cg::util::LogPriority::NORMAL));
#define CG_LOG_WARNING(STR) (GlobalLoggerConsole.logLine(CG_LOG_PREFIX + (STR), cg::util::LogPriority::WARNING));
#define CG_LOG_ERROR(STR) (GlobalLoggerConsole.logLine(CG_LOG_PREFIX + (STR), cg::util::LogPriority::ERROR));
#define CG_LOG_DEBUG(STR) (GlobalLoggerConsole.logLine(CG_LOG_PREFIX + (STR), cg::util::LogPriority::DEBUG));
#define CG_LOG_VERBOSE(STR) (GlobalLoggerConsole.logLine((STR), cg::util::LogPriority::VERBOSE));
// #else
// 	#define CG_LOG_PREFIX
// 	#define CG_LOG(STR)
// 	#define CG_LOG_WARNING(STR)
// 	#define CG_LOG_ERROR(STR)
// 	#define CG_LOG_DEBUG(STR)
// 	#define CG_LOG_VERBOSE(STR)
// #endif
