#pragma once

#include <string>

namespace cg
{
	namespace util
	{
		enum class LogPriority : int
		{
			VERBOSE = 0,
			NORMAL = 1,
			WARNING = 2,
			ERROR = 4,
			DEBUG = 5,
		};

		class Logger
		{
		public:
			Logger();
			virtual ~Logger() = default;
			virtual void log(const std::string& str, LogPriority logPriority = LogPriority::NORMAL) = 0;
			virtual void logLine(const std::string& str, LogPriority logPriority = LogPriority::NORMAL) = 0;

			void setLogPriority(LogPriority logPriority);
			LogPriority getLogPriority() const;

		protected:
			std::string getTimeInfo() const;

		private:
			LogPriority m_logPriority;
		};
	}
}