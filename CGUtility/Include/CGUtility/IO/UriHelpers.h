#pragma once

#include <string>

namespace cg
{
    namespace util
    {
        inline std::string getFileName(const std::string& filepath)
        {
            for (int i = (int)filepath.size() - 1; i >= 0; --i)
            {
                if (filepath[i] == '/' || filepath[i] == '\\')
                {
                    if (static_cast<size_t>(i) == filepath.size() - 1)
                    {
                        return "";
                    }
                    return filepath.substr(i + 1, filepath.size() - i - 1);
                }
            }
            return filepath;
        }

        inline std::string getFolder(const std::string& filepath)
        {
            for (int i = (int)filepath.size() - 1; i >= 0; --i)
            {
                if (filepath[i] == '/' || filepath[i] == '\\')
                {
                    return filepath.substr(0, i);
                }
            }
            return "";
        }
    }  // namespace util
}  // namespace cg
