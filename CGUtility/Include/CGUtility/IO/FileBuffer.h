#pragma once

#include <memory>
#include <string>

namespace cg
{
	namespace util
	{
		class FileBuffer
		{
		public:

			FileBuffer();

			void clear();
			bool readFile(const std::string& filepath);
			bool seekNextLine();
			std::string getLine() const;
			const std::string& getAll() const;

		private:
			// The raw file data.
			std::string m_buffer;
			// Window for the part of the buffer currently under inspection.
			size_t m_seekerStart;
			size_t m_seekerEnd;
		};
	}
}