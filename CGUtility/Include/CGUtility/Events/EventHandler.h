#pragma once

#include <map>
#include <cstdint>
#include "EventCallback.h"

namespace cg
{
	namespace util
	{
		template<class Sender, class... EventArgs>
		class EventHandler
		{
		public:
			using SubscriptionHandle = uint64_t;
			using CallbackList = std::map<SubscriptionHandle, BaseEventCallback<Sender, EventArgs...>*>;

			EventHandler();
			// Copying or moving the EventHandlers is not allowed.
			EventHandler(EventHandler&&) = delete;
			EventHandler(const EventHandler&) = delete;
			EventHandler& operator=(EventHandler&&) = delete;
			EventHandler& operator=(const EventHandler&) = delete;

			inline void notify(const Sender& sender, EventArgs&&... event)
			{
				for (auto it = m_callbacks.begin(); it != m_callbacks.end(); ++it)
				{
					it->second->invoke(sender, std::forward<EventArgs>(event)...);
				}
			}

			inline SubscriptionHandle subscribe(void(*function)(const Sender&, EventArgs&&...))
			{
				m_handleCounter++;
				m_callbacks.insert(make_pair(m_handleCounter, new FunctionEventCallback<Sender, EventArgs...>(function)));
				return m_handleCounter;
			}

			template<class Subscriber>
			inline SubscriptionHandle subscribe(Subscriber& subscriber, void (Subscriber::*memberFunction)(const Sender&, EventArgs&&...))
			{
				m_handleCounter++;
				m_callbacks.insert(make_pair(m_handleCounter, new MemberFunctionEventCallback<Sender, Subscriber, EventArgs...>(&subscriber, memberFunction)));
				return m_handleCounter;
			}

			inline void unsubscribe(SubscriptionHandle handle)
			{
				if (m_callbacks.find(handle) != m_callbacks.end())
				{
					m_callbacks.erase(handle);
				}
			}

		private:
			// List is used so that iterators aren't invalidated on inserts/deletions.
			CallbackList m_callbacks;
			SubscriptionHandle m_handleCounter;
		};

		template<class Sender, class... EventArgs>
		EventHandler<Sender, EventArgs...>::EventHandler() :
			m_handleCounter(0),
			m_callbacks()
		{
		}
	}
}

