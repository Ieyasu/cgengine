#pragma once

namespace cg
{
	namespace util
	{
		template<class Sender, class... EventArgs>
		class BaseEventCallback
		{
		public:
			virtual ~BaseEventCallback() = default;
			virtual void invoke(const Sender& sender, EventArgs&&... args) = 0;
		};

		template<class Sender, class... EventArgs>
		class FunctionEventCallback : public BaseEventCallback<Sender, EventArgs...>
		{
		public:
			typedef void(*Function)(const Sender&, EventArgs&&...);

			FunctionEventCallback(Function function) :
				m_function(function)
			{
			};

			void invoke(const Sender& sender, EventArgs&&... args) override
			{
				if (!m_function)
				{
					return;
				}
				(*m_function)(sender, std::forward<EventArgs>(args)...);
			}
		private:
			Function m_function;
		};

		template<class Sender, class Subscriber, class... EventArgs>
		class MemberFunctionEventCallback : public BaseEventCallback<Sender, EventArgs...>
		{
		public:
			typedef void (Subscriber::*MemberFunction)(const Sender&, EventArgs&&...);

			MemberFunctionEventCallback(Subscriber* instance, MemberFunction memberFunction) :
				m_instance(instance),
				m_memberFunction(memberFunction)
			{
			};

			void invoke(const Sender& sender, EventArgs&&... args) override
			{
				if (!m_instance || !m_memberFunction)
				{
					return;
				}
				(m_instance->*m_memberFunction)(sender, std::forward<EventArgs>(args)...);
			}

		private:
			Subscriber* m_instance;
			MemberFunction m_memberFunction;
		};
	}
}