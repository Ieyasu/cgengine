#pragma once

namespace cg
{
    namespace util
    {
        template <class V, class T, class F = void (T::*)(V&) const>
        class CachedObject
        {
        public:
            CachedObject(F evaluate)
                : m_dirty(true)
                , m_evaluate(evaluate)
            {
            }

            inline void setDirty() const
            {
                m_dirty = true;
            }

            inline const V& get(const T& evaluator) const
            {
                if (m_dirty)
                {
                    (evaluator.*m_evaluate)(m_value);
                    m_dirty = false;
                }
                return m_value;
            }

            inline const V* operator->() const
            {
                return *get();
            }

        private:
            mutable V    m_value;
            mutable bool m_dirty;
            F            m_evaluate;
        };
    }  // namespace util
}  // namespace cg
