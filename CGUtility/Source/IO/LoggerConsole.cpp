#include "CGUtility/IO/LoggerConsole.h"

#include <iostream>

namespace cg
{
    namespace util
    {
        LoggerConsole::LoggerConsole(LogPriority logPriority)
        {
            setLogPriority(logPriority);
        }

        void LoggerConsole::log(const std::string& str, LogPriority logPriority)
        {
            if (logPriority < getLogPriority())
            {
                return;
            }
            std::cout << getTimeInfo() << " " << str;
        }

        void LoggerConsole::logLine(const std::string& str, LogPriority logPriority)
        {
            if (logPriority < getLogPriority())
            {
                return;
            }
            std::cout << getTimeInfo() << " " << str << std::endl;
        }
    }  // namespace util
}  // namespace cg
