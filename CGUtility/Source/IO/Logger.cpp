#include "CGUtility/IO/Logger.h"

#include <chrono>

namespace cg
{
    namespace util
    {
        Logger::Logger()
            : m_logPriority(LogPriority::NORMAL)
        {
        }

        void Logger::setLogPriority(LogPriority logPriority)
        {
            m_logPriority = logPriority;
        }

        LogPriority Logger::getLogPriority() const
        {
            return m_logPriority;
        }

        std::string Logger::getTimeInfo() const
        {
            // auto time_t = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
            return "";
        }
    }  // namespace util
}  // namespace cg
