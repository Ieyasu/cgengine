#include "CGUtility/IO/FileBuffer.h"

namespace cg
{
    namespace util
    {
        FileBuffer::FileBuffer()
        {
            clear();
        }

        void FileBuffer::clear()
        {
            m_buffer.clear();
            m_seekerStart = 0;
            m_seekerEnd = 0;
        }

        bool FileBuffer::readFile(const std::string& filepath)
        {
            clear();

            FILE* fp = fopen(filepath.c_str(), "rb");

            // Unable to open file
            if (!fp)
            {
                return false;
            }

            // Initialize buffer size.
            fseek(fp, 0, SEEK_END);
            long bufferSize = ftell(fp);
            fseek(fp, 0, SEEK_SET);
            m_buffer.resize(bufferSize, '\0');

            // Read file to buffer.
            size_t readLen = fread(&m_buffer[0], 1, bufferSize, fp);
            fclose(fp);

            if ((long)readLen != bufferSize)
            {
                clear();
                return false;
            }

            return true;
        }

        bool FileBuffer::seekNextLine()
        {
            // We are already at the end of the file.
            if (m_seekerEnd >= m_buffer.length())
            {
                return false;
            }

            // Previous line's end is new line's start.
            m_seekerStart = m_seekerEnd;

            // Increment m_seekerEnd until we reach a newline marker.
            while (m_seekerEnd < m_buffer.length())
            {
                if (m_buffer[m_seekerEnd++] == '\n')
                {
                    m_buffer[m_seekerEnd - 1] = '\0';
                    return true;
                }
            }

            // End of file.
            m_seekerEnd = m_buffer.length();
            return true;
        }

        std::string FileBuffer::getLine() const
        {
            if (m_seekerStart >= m_buffer.length() || m_seekerEnd > m_buffer.length())
            {
                return std::string(0);
            }
            return std::string(&m_buffer[m_seekerStart], m_seekerEnd - m_seekerStart);
        }

        const std::string& FileBuffer::getAll() const
        {
            return m_buffer;
        }
    }  // namespace util
}  // namespace cg
