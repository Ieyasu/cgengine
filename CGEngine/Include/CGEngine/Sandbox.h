#pragma once

#include "CGCore/Window.h"
#include "CGEntity/World.h"

namespace cg
{
    using core::Window;
    using entity::World;

    namespace engine
    {
        class Sandbox
        {
        public:
            bool intialize(const std::string& filename);
            void update();

        private:
            World m_world;
        };
    }  // namespace engine
}  // namespace cg
