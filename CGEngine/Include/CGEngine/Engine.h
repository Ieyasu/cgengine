#pragma once

#include "CGCore/Context.h"
#include "Sandbox.h"

namespace cg
{
    namespace engine
    {
        class Engine
        {
        public:
            Engine();
            ~Engine();

            bool initialize(const std::string& filename);
            void uninitialize();
            void run();
            void stop();

        private:
            Sandbox m_sandbox;

            bool m_initialized;
            bool m_running;

            void update();
        };
    }  // namespace engine
}  // namespace cg
