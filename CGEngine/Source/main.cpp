#ifdef _WIN32
#define _CRTDBG_MAP_ALLOC
#include <crtdbg.h>
#include <stdlib.h>
#endif

#include <iostream>

#include "CGEngine/Engine.h"
using cg::engine::Engine;

int main(int argc, char** argv)
{
#ifdef _WIN32
    // Detect memory leaks
    _CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);
#endif

    auto filename = std::string("Meshes/bunny.obj");
    if (argc == 2)
    {
        filename = argv[1];
    }

    Engine engine {};
    if (!engine.initialize(filename))
    {
        // Failed to initialize engine.
        std::cout << "Failed to initialize" << std::endl;
        return -1;
    }

    // Main loop
    std::cout << "Running" << std::endl;
    engine.run();

    // Release resources.
    std::cout << "Uninitializing" << std::endl;
    engine.uninitialize();

    return 0;
}
