#include "CGEngine/Engine.h"

#include "CGUtility/IO/LoggerConsole.h"

namespace cg
{
    namespace engine
    {
        Engine::Engine()
            : m_initialized(false)
            , m_running(false)
        {
        }

        Engine::~Engine()
        {
            uninitialize();
        }

        bool Engine::initialize(const std::string& filename)
        {
            if (m_initialized)
            {
                CG_LOG_WARNING("Engine already initialized");
                return true;
            }

            if (!core::context::initialize(640, 480, "CGEngine"))
            {
                CG_LOG_ERROR("Failed to initialize engine context.");
                uninitialize();
                return false;
            }

            if (!m_sandbox.intialize(filename))
            {
                CG_LOG_ERROR("Failed to initialize Sandbox.");
                uninitialize();
                return false;
            }

            m_initialized = true;
            CG_LOG_VERBOSE("Initialized Engine.");
            return true;
        }

        void Engine::uninitialize()
        {
            core::context::uninitialize();
            m_initialized = false;
            CG_LOG_VERBOSE("Uninitialized Engine.");
        }

        void Engine::run()
        {
            if (!m_initialized)
            {
                CG_LOG_ERROR("Failed to start Engine. Not initialized.");
            }

            if (m_running)
            {
                CG_LOG_WARNING("Failed to start Engine. Already running.");
                return;
            }

            m_running = true;

            // Main loop.
            while (m_running && core::context::isAvailable())
            {
                update();
            }

            stop();
        }

        void Engine::update()
        {
            // Update context first.
            core::context::update();

            // Update the world (includes rendering).
            m_sandbox.update();
        }

        void Engine::stop()
        {
            if (!m_running)
            {
                CG_LOG_WARNING("Failed to stop Engine. Not running.");
                return;
            }

            m_running = false;
        }
    }  // namespace engine
}  // namespace cg
