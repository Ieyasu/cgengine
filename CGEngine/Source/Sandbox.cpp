#include "CGEngine/Sandbox.h"

#include <chrono>
#include <memory>

#include "CGCore/Components/FreeFly.h"
#include "CGCore/Components/MeshRenderer.h"
#include "CGCore/Components/OrthographicCamera.h"
#include "CGCore/Components/PerspectiveCamera.h"
#include "CGCore/Components/Transform.h"
#include "CGCore/Mesh/MeshLoaderObj.h"
#include "CGCore/Systems/FreeFlySystem.h"
#include "CGCore/Systems/RaytraceSystem.h"
#include "CGUtility/IO/LoggerConsole.h"
#include "CGVoxels/VoxelOctree.h"

namespace cg
{
    using core::FreeFly;
    using core::FreeFlySystem;
    using core::MeshLoaderObj;
    using core::MeshRenderer;
    using core::OrthographicCamera;
    using core::PerspectiveCamera;
    using core::RaytraceSystem;
    using core::Transform;
    using voxel::VoxelOctree;

    namespace engine
    {
        bool Sandbox::intialize(const std::string& filename)
        {
            auto meshLoader = MeshLoaderObj();

            auto begin = std::chrono::high_resolution_clock::now();
            auto mesh = meshLoader.loadTriangleMesh(filename);
            if (!mesh)
            {
                CG_LOG_ERROR("Failed to initialize Sandbox. Couldn't load Mesh.")
                return false;
            }
            auto end = std::chrono::high_resolution_clock::now();
            CG_LOG("Mesh loaded in "
                   + std::to_string(std::chrono::duration_cast<std::chrono::milliseconds>(end - begin).count())
                   + "ms!");

            begin = std::chrono::high_resolution_clock::now();
            mesh->beginEdit();
            mesh->normalize();
            mesh->endEdit();
            end = std::chrono::high_resolution_clock::now();
            CG_LOG("Mesh normalized in "
                   + std::to_string(std::chrono::duration_cast<std::chrono::milliseconds>(end - begin).count())
                   + "ms!");

            // Build Octree
            begin = std::chrono::high_resolution_clock::now();
            const auto                   builder = VoxelOctree::Builder();
            const auto                   buildConfig = VoxelOctree::BuildConfig(mesh.get(), 512);
            std::unique_ptr<VoxelOctree> octree = builder.build(buildConfig);
            end = std::chrono::high_resolution_clock::now();

            CG_LOG("Octree built in "
                   + std::to_string(std::chrono::duration_cast<std::chrono::milliseconds>(end - begin).count())
                   + "ms!");
            CG_LOG("Allocated memory " + std::to_string(octree->getAllocatedMemory() / (1024 * 1024)) + "MB.");
            CG_LOG("Voxel count " + std::to_string(octree->getVoxelCount()));

            // Add and configure the RenderSystem.
            auto renderSystem = std::make_shared<RaytraceSystem<VoxelOctree, VoxelOctree::Raytracer>>(
                    std::move(octree),
                    std::make_unique<VoxelOctree::Raytracer>());
            m_world.addSystem(renderSystem);
            auto cameraEntity = renderSystem->getCameraEntity();

            auto cameraTransform = cameraEntity->getComponent<Transform>();
            cameraTransform->setLocalRotation(0, 0, 0);
            cameraTransform->setLocalPosition(0, 0, 2.5f);

            auto pcamera = cameraEntity->getComponent<PerspectiveCamera>();
            pcamera->setAspectRatio((float)16 / 9);
            pcamera->setNearPlane(0.2f);
            pcamera->setFarPlane(3.0f);
            pcamera->setFovHorizontal(60.0f);

            auto ocamera = cameraEntity->getComponent<OrthographicCamera>();
            ocamera->setAspectRatio((float)16 / 9);
            ocamera->setNearPlane(0.2f);
            ocamera->setFarPlane(3.0f);
            ocamera->setHeight(1.0f);

            // Add point lights.

            auto pointLightEntity = m_world.createEntity();
            pointLightEntity->addComponent<Transform>();
            auto pointLight = pointLightEntity->addComponent<PointLight>();
            pointLight->setIntensity(10.0f);
            pointLight->setRange(10.0f);

            // Add an configure the FreeFlySystem.
            auto freeFly = cameraEntity->addComponent<FreeFly>();
            freeFly->setMovementSpeed(2.0f);
            freeFly->setRotationSpeed(0.25f);

            auto freeFlySystem = std::make_shared<FreeFlySystem>(cameraEntity);
            m_world.addSystem(freeFlySystem);

            // Add a MeshRenderer.
            auto entity = m_world.createEntity();
            auto meshRenderer = entity->addComponent<MeshRenderer>();
            meshRenderer->setMesh(std::shared_ptr<TriangleMesh>(std::move(mesh)));

            /*
            int n = 2000000;

            begin = std::chrono::high_resolution_clock::now();
            std::vector<Entity> entities1(n);
            end = std::chrono::high_resolution_clock::now();
            auto s = "1 Created in " + std::to_string(std::chrono::duration_cast<std::chrono::milliseconds>(end -
            begin).count()) + "ms!\n"; CG_LOG(s.c_str());

            begin = std::chrono::high_resolution_clock::now();
            World testWorld;
            std::vector<Handle<Entity>> entities2 = testWorld.createEntities(n);
            end = std::chrono::high_resolution_clock::now();
            s = "2 Created in " + std::to_string(std::chrono::duration_cast<std::chrono::milliseconds>(end -
            begin).count()) + "ms!\n"; CG_LOG(s.c_str());

            std::vector<Transform*> components;
            begin = std::chrono::high_resolution_clock::now();
            for (int i = 0; i < n; ++i)
            {
                components.push_back(new Transform());
            }
            end = std::chrono::high_resolution_clock::now();
            s = "1 Component added in " + std::to_string(std::chrono::duration_cast<std::chrono::milliseconds>(end -
            begin).count()) + "ms!\n"; CG_LOG(s.c_str());

            begin = std::chrono::high_resolution_clock::now();
            for (int i = 0; i < n; ++i)
            {
                entities2[i]->addComponent<Transform>();
            }
            end = std::chrono::high_resolution_clock::now();
            s = "2 Component added in " + std::to_string(std::chrono::duration_cast<std::chrono::milliseconds>(end -
            begin).count()) + "ms!\n"; CG_LOG(s.c_str());

            int counter = 0;
            begin = std::chrono::high_resolution_clock::now();
            for (int i = 0; i < 1; ++i)
            {
                for (const auto& transform : components)
                {
                    counter += (int)transform->getPosition().x;
                }
            }
            end = std::chrono::high_resolution_clock::now();
            s = "1 Looped in " + std::to_string(std::chrono::duration_cast<std::chrono::milliseconds>(end -
            begin).count()) + "ms!\n"; CG_LOG(s.c_str());

            begin = std::chrono::high_resolution_clock::now();
            const auto& transforms = *testWorld.getAllComponentsOfType<Transform>();
            for (int i = 0; i < 1; ++i)
            {
                for (const auto& transform : transforms)
                {
                    counter += (int)transform.getPosition().x;
                }
            }
            end = std::chrono::high_resolution_clock::now();
            s = "2 Looped in " + std::to_string(std::chrono::duration_cast<std::chrono::milliseconds>(end -
            begin).count()) + "ms!\n"; CG_LOG(s.c_str());

            begin = std::chrono::high_resolution_clock::now();
            for (int i = 0; i < 1; ++i)
            {
                for (const auto& entity : entities2)
                {
                    entity->getComponent<Transform>()->getPosition().x;
                }
            }
            end = std::chrono::high_resolution_clock::now();
            s = "3 Looped in " + std::to_string(std::chrono::duration_cast<std::chrono::milliseconds>(end -
            begin).count()) + "ms!\n"; CG_LOG(s.c_str()); s = "Bin " + std::to_string(counter) + "\n";
            CG_LOG(s.c_str());

            begin = std::chrono::high_resolution_clock::now();
            entities1.clear();
            for (size_t i = 0; i < components.size(); ++i)
            {
                delete components[i];
            }
            components.clear();
            end = std::chrono::high_resolution_clock::now();
            s = "1 Destroyed in " + std::to_string(std::chrono::duration_cast<std::chrono::milliseconds>(end -
            begin).count()) + "ms!\n"; CG_LOG(s.c_str());

            begin = std::chrono::high_resolution_clock::now();
            for (auto& entity : entities2)
            {
                entity->destroy();
            }
            end = std::chrono::high_resolution_clock::now();
            s = "2 Destroyed in " + std::to_string(std::chrono::duration_cast<std::chrono::milliseconds>(end -
            begin).count()) + "ms!\n"; CG_LOG(s.c_str();)*/

            return true;
        }

        void Sandbox::update()
        {
            m_world.update();
        }
    }  // namespace engine
}  // namespace cg
