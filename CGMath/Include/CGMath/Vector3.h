#pragma once

#include "Vector.h"

namespace cg
{
    namespace math
    {
        template <class T>
        class Vector3 : public VectorN<T, Vector3<T>, 3>
        {
        public:
            T& x;
            T& y;
            T& z;

            Vector3<T>()
                : x(this->m_data[0])
                , y(this->m_data[1])
                , z(this->m_data[2])
            {
                this->m_data[0] = 0;
                this->m_data[1] = 0;
                this->m_data[2] = 0;
            }

            explicit Vector3<T>(T val)
                : x(this->m_data[0])
                , y(this->m_data[1])
                , z(this->m_data[2])
            {
                this->m_data[0] = val;
                this->m_data[1] = val;
                this->m_data[2] = val;
            }

            Vector3<T>(T x, T y, T z)
                : x(this->m_data[0])
                , y(this->m_data[1])
                , z(this->m_data[2])
            {
                this->m_data[0] = x;
                this->m_data[1] = y;
                this->m_data[2] = z;
            }

            Vector3<T>(const Vector3<T>& other)
                : x(this->m_data[0])
                , y(this->m_data[1])
                , z(this->m_data[2])
            {
                this->m_data[0] = other.x;
                this->m_data[1] = other.y;
                this->m_data[2] = other.z;
            }

            explicit Vector3<T>(const float data[3])
                : x(this->m_data[0])
                , y(this->m_data[1])
                , z(this->m_data[2])
            {
                this->m_data[0] = data[0];
                this->m_data[1] = data[1];
                this->m_data[2] = data[2];
            }

            template <class T2, class S2, unsigned int U2>
            Vector3<T>(const VectorN<T2, S2, U2>& other)
                : x(this->m_data[0])
                , y(this->m_data[1])
                , z(this->m_data[2])
            {
                for (size_t i = 0; i < U2 && i < 3; ++i)
                {
                    this->m_data[i] = (T)other[i];
                }
                for (size_t i = U2; i < 3; ++i)
                {
                    this->m_data[i] = (T)0;
                }
            }

            inline Vector3<T>& operator=(const Vector3<T>& other)
            {
                x = other.x;
                y = other.y;
                z = other.z;
                return *this;
            }

            inline T min() const
            {
                if (x < y)
                {
                    return x < z ? x : z;
                }
                return y < z ? y : z;
            }

            inline T max() const
            {
                if (x > y)
                {
                    return x > z ? x : z;
                }
                return y > z ? y : z;
            }

            inline Vector3<T> cross(const Vector3<T>& other) const
            {
                return Vector3<T>(y * other.z - z * other.y, z * other.x - x * other.z, x * other.y - y * other.x);
            }

            inline static Vector3<T> getUp()
            {
                return Vector3<T>(0, 1, 0);
            }

            inline static Vector3<T> getRight()
            {
                return Vector3<T>(1, 0, 0);
            }

            inline static Vector3<T> getForward()
            {
                return Vector3<T>(0, 0, -1);
            }
        };

        typedef Vector3<double> Vector3d;
        typedef Vector3<float>  Vector3f;
        typedef Vector3<int>    Vector3i;
    }  // namespace math
}  // namespace cg
