#pragma once

#include "Vector3.h"
#include "Vector4.h"
#include <string>

namespace cg
{
	namespace math
	{
		template <class T>
		class Matrix4
		{
		public:
			Matrix4<T>()
			{
				for (int i = 0; i < 16; ++i)
				{
					m_data[i] = 0;
				}
			}

			Matrix4<T>(T val)
			{
				for (int i = 0; i < 16; ++i)
				{
					m_data[i] = val;
				}
			}

			Matrix4<T>(T m00, T m01, T m02, T m03,
					   T m10, T m11, T m12, T m13,
					   T m20, T m21, T m22, T m23,
					   T m30, T m31, T m32, T m33)
			{
				m_data[0] = m00;
				m_data[1] = m01;
				m_data[2] = m02;
				m_data[3] = m03;
				m_data[4] = m10;
				m_data[5] = m11;
				m_data[6] = m12;
				m_data[7] = m13;
				m_data[8] = m20;
				m_data[9] = m21;
				m_data[10] = m22;
				m_data[11] = m23;
				m_data[12] = m30;
				m_data[13] = m31;
				m_data[14] = m32;
				m_data[15] = m33;
			}

			inline T &operator[](int i)
			{
				return m_data[i];
			}

			inline T operator[](int i) const
			{
				return m_data[i];
			}

			inline void set(T m00, T m01, T m02, T m03,
							T m10, T m11, T m12, T m13,
							T m20, T m21, T m22, T m23,
							T m30, T m31, T m32, T m33)
			{
				m_data[0] = m00;
				m_data[1] = m01;
				m_data[2] = m02;
				m_data[3] = m03;
				m_data[4] = m10;
				m_data[5] = m11;
				m_data[6] = m12;
				m_data[7] = m13;
				m_data[8] = m20;
				m_data[9] = m21;
				m_data[10] = m22;
				m_data[11] = m23;
				m_data[12] = m30;
				m_data[13] = m31;
				m_data[14] = m32;
				m_data[15] = m33;
			}

			inline void set(int column, int row, T val)
			{
				const int index = row * 4 + column;
				m_data[index] = val;
			}

			inline void setRow(int row, const Vector4<T> &val)
			{
				setRow(row, val[0], val[1], val[2], val[3]);
			}

			inline void setRow(int row, T val1, T val2, T val3, T val4)
			{
				const int rowIndex = row * 4;
				m_data[rowIndex] = val1;
				m_data[rowIndex + 1] = val2;
				m_data[rowIndex + 2] = val3;
				m_data[rowIndex + 3] = val4;
			}

			inline void setColumn(int column, const Vector4<T> &val)
			{
				setColumn(column, val[0], val[1], val[2], val[3]);
			}

			inline void setColumn(int column, T val1, T val2, T val3, T val4)
			{
				m_data[column] = val1;
				m_data[column + 4] = val2;
				m_data[column + 8] = val3;
				m_data[column + 12] = val4;
			}

			inline void setIdentity()
			{
				set(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1);
			}

			inline bool invert()
			{
				// Based on MESA implementation of the GLU library (http://www.mesa3d.org/)

				Matrix4<T> inv;

				inv[0] = m_data[5] * m_data[10] * m_data[15] -
						 m_data[5] * m_data[11] * m_data[14] -
						 m_data[9] * m_data[6] * m_data[15] +
						 m_data[9] * m_data[7] * m_data[14] +
						 m_data[13] * m_data[6] * m_data[11] -
						 m_data[13] * m_data[7] * m_data[10];

				inv[4] = -m_data[4] * m_data[10] * m_data[15] +
						 m_data[4] * m_data[11] * m_data[14] +
						 m_data[8] * m_data[6] * m_data[15] -
						 m_data[8] * m_data[7] * m_data[14] -
						 m_data[12] * m_data[6] * m_data[11] +
						 m_data[12] * m_data[7] * m_data[10];

				inv[8] = m_data[4] * m_data[9] * m_data[15] -
						 m_data[4] * m_data[11] * m_data[13] -
						 m_data[8] * m_data[5] * m_data[15] +
						 m_data[8] * m_data[7] * m_data[13] +
						 m_data[12] * m_data[5] * m_data[11] -
						 m_data[12] * m_data[7] * m_data[9];

				inv[12] = -m_data[4] * m_data[9] * m_data[14] +
						  m_data[4] * m_data[10] * m_data[13] +
						  m_data[8] * m_data[5] * m_data[14] -
						  m_data[8] * m_data[6] * m_data[13] -
						  m_data[12] * m_data[5] * m_data[10] +
						  m_data[12] * m_data[6] * m_data[9];

				T det = m_data[0] * inv[0] + m_data[1] * inv[4] + m_data[2] * inv[8] + m_data[3] * inv[12];

				if (det == 0)
				{
					setIdentity();
				}

				inv[1] = -m_data[1] * m_data[10] * m_data[15] +
						 m_data[1] * m_data[11] * m_data[14] +
						 m_data[9] * m_data[2] * m_data[15] -
						 m_data[9] * m_data[3] * m_data[14] -
						 m_data[13] * m_data[2] * m_data[11] +
						 m_data[13] * m_data[3] * m_data[10];

				inv[5] = m_data[0] * m_data[10] * m_data[15] -
						 m_data[0] * m_data[11] * m_data[14] -
						 m_data[8] * m_data[2] * m_data[15] +
						 m_data[8] * m_data[3] * m_data[14] +
						 m_data[12] * m_data[2] * m_data[11] -
						 m_data[12] * m_data[3] * m_data[10];

				inv[9] = -m_data[0] * m_data[9] * m_data[15] +
						 m_data[0] * m_data[11] * m_data[13] +
						 m_data[8] * m_data[1] * m_data[15] -
						 m_data[8] * m_data[3] * m_data[13] -
						 m_data[12] * m_data[1] * m_data[11] +
						 m_data[12] * m_data[3] * m_data[9];

				inv[13] = m_data[0] * m_data[9] * m_data[14] -
						  m_data[0] * m_data[10] * m_data[13] -
						  m_data[8] * m_data[1] * m_data[14] +
						  m_data[8] * m_data[2] * m_data[13] +
						  m_data[12] * m_data[1] * m_data[10] -
						  m_data[12] * m_data[2] * m_data[9];

				inv[2] = m_data[1] * m_data[6] * m_data[15] -
						 m_data[1] * m_data[7] * m_data[14] -
						 m_data[5] * m_data[2] * m_data[15] +
						 m_data[5] * m_data[3] * m_data[14] +
						 m_data[13] * m_data[2] * m_data[7] -
						 m_data[13] * m_data[3] * m_data[6];

				inv[6] = -m_data[0] * m_data[6] * m_data[15] +
						 m_data[0] * m_data[7] * m_data[14] +
						 m_data[4] * m_data[2] * m_data[15] -
						 m_data[4] * m_data[3] * m_data[14] -
						 m_data[12] * m_data[2] * m_data[7] +
						 m_data[12] * m_data[3] * m_data[6];

				inv[10] = m_data[0] * m_data[5] * m_data[15] -
						  m_data[0] * m_data[7] * m_data[13] -
						  m_data[4] * m_data[1] * m_data[15] +
						  m_data[4] * m_data[3] * m_data[13] +
						  m_data[12] * m_data[1] * m_data[7] -
						  m_data[12] * m_data[3] * m_data[5];

				inv[14] = -m_data[0] * m_data[5] * m_data[14] +
						  m_data[0] * m_data[6] * m_data[13] +
						  m_data[4] * m_data[1] * m_data[14] -
						  m_data[4] * m_data[2] * m_data[13] -
						  m_data[12] * m_data[1] * m_data[6] +
						  m_data[12] * m_data[2] * m_data[5];

				inv[3] = -m_data[1] * m_data[6] * m_data[11] +
						 m_data[1] * m_data[7] * m_data[10] +
						 m_data[5] * m_data[2] * m_data[11] -
						 m_data[5] * m_data[3] * m_data[10] -
						 m_data[9] * m_data[2] * m_data[7] +
						 m_data[9] * m_data[3] * m_data[6];

				inv[7] = m_data[0] * m_data[6] * m_data[11] -
						 m_data[0] * m_data[7] * m_data[10] -
						 m_data[4] * m_data[2] * m_data[11] +
						 m_data[4] * m_data[3] * m_data[10] +
						 m_data[8] * m_data[2] * m_data[7] -
						 m_data[8] * m_data[3] * m_data[6];

				inv[11] = -m_data[0] * m_data[5] * m_data[11] +
						  m_data[0] * m_data[7] * m_data[9] +
						  m_data[4] * m_data[1] * m_data[11] -
						  m_data[4] * m_data[3] * m_data[9] -
						  m_data[8] * m_data[1] * m_data[7] +
						  m_data[8] * m_data[3] * m_data[5];

				inv[15] = m_data[0] * m_data[5] * m_data[10] -
						  m_data[0] * m_data[6] * m_data[9] -
						  m_data[4] * m_data[1] * m_data[10] +
						  m_data[4] * m_data[2] * m_data[9] +
						  m_data[8] * m_data[1] * m_data[6] -
						  m_data[8] * m_data[2] * m_data[5];

				det = (T)1 / det;

				for (auto i = 0; i < 16; i++)
				{
					m_data[i] = inv[i] * det;
				}

				return true;
			}

			inline const T *getData() const
			{
				return m_data;
			}

			inline T get(int column, int row) const
			{
				const int index = row * 4 + column;
				return m_data[index];
			}

			inline Vector4<T> getRow(int row) const
			{
				return Vector4<T>(m_data[row]);
			}

			inline Vector4<T> getColumn(int column) const
			{
				return Vector4<T>(m_data[column], m_data[column + 4], m_data[column + 8], m_data[column + 12]);
			}

			inline Matrix4<T> getIdentity() const
			{
				Matrix4<T> result;
				result.setIdentity();
				return result;
			}

			inline Matrix4<T> getTranspose() const
			{
				Matrix4<T> result;
				result[0] = m_data[0];
				result[1] = m_data[4];
				result[2] = m_data[8];
				result[3] = m_data[12];
				result[4] = m_data[1];
				result[5] = m_data[5];
				result[6] = m_data[9];
				result[7] = m_data[13];
				result[8] = m_data[2];
				result[9] = m_data[6];
				result[10] = m_data[10];
				result[11] = m_data[14];
				result[12] = m_data[3];
				result[13] = m_data[7];
				result[14] = m_data[11];
				result[15] = m_data[15];
				return result;
			}

			inline Matrix4<T> getInverse() const
			{
				Matrix4 result(*this);
				result.invert();
				return result;
			}

		private:
			T m_data[16];
		};

		template <class T>
		inline Vector3<T> operator*(const Matrix4<T> &matrix, const Vector3<T> &vector)
		{
			Vector3<T> result;
			result.x = vector.x * matrix[0] + vector.y * matrix[1] + vector.z * matrix[2];
			result.y = vector.x * matrix[4] + vector.y * matrix[5] + vector.z * matrix[6];
			result.z = vector.x * matrix[8] + vector.y * matrix[9] + vector.z * matrix[10];
			return result;
		}

		template <class T>
		inline Vector3<T> operator*(const Vector3<T> &vector, const Matrix4<T> &matrix)
		{
			Vector3<T> result;
			result.x = vector.x * matrix[0] + vector.y * matrix[4] + vector.z * matrix[8];
			result.y = vector.x * matrix[1] + vector.y * matrix[5] + vector.z * matrix[9];
			result.z = vector.x * matrix[2] + vector.y * matrix[6] + vector.z * matrix[10];
			return result;
		}

		template <class T>
		inline Vector4<T> operator*(const Matrix4<T> &matrix, const Vector4<T> &vector)
		{
			Vector4<T> result;
			result.x = vector.x * matrix[0] + vector.y * matrix[1] + vector.z * matrix[2] + vector.w * matrix[3];
			result.y = vector.x * matrix[4] + vector.y * matrix[5] + vector.z * matrix[6] + vector.w * matrix[7];
			result.z = vector.x * matrix[8] + vector.y * matrix[9] + vector.z * matrix[10] + vector.w * matrix[11];
			result.w = vector.x * matrix[12] + vector.y * matrix[13] + vector.z * matrix[14] + vector.w * matrix[15];
			return result;
		}

		template <class T>
		inline Vector4<T> operator*(const Vector4<T> &vector, const Matrix4<T> &matrix)
		{
			Vector4<T> result;
			result.x = vector.x * matrix[0] + vector.y * matrix[4] + vector.z * matrix[8] + vector.w * matrix[12];
			result.y = vector.x * matrix[1] + vector.y * matrix[5] + vector.z * matrix[9] + vector.w * matrix[13];
			result.z = vector.x * matrix[2] + vector.y * matrix[6] + vector.z * matrix[10] + vector.w * matrix[14];
			result.w = vector.x * matrix[3] + vector.y * matrix[7] + vector.z * matrix[11] + vector.w * matrix[15];
			return result;
		}

		template <class T>
		inline Matrix4<T> operator*(const Matrix4<T> &matrix1, const Matrix4<T> &matrix2)
		{
			Matrix4<T> result;
			for (auto rowIndex = 0; rowIndex <= 12; rowIndex += 4)
			{
				for (auto col = 0; col < 4; ++col)
				{
					int index = rowIndex + col;
					result[index] += matrix1[rowIndex] * matrix2[col];
					result[index] += matrix1[rowIndex + 1] * matrix2[col + 4];
					result[index] += matrix1[rowIndex + 2] * matrix2[col + 8];
					result[index] += matrix1[rowIndex + 3] * matrix2[col + 12];
				}
			}
			return result;
		}

		typedef Matrix4<double> Matrix4d;
		typedef Matrix4<float> Matrix4f;
		typedef Matrix4<int> Matrix4i;
	}
}
