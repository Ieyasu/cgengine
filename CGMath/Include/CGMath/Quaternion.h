#pragma once

#include "Matrix.h"
#include "Vector.h"
#include "Common.h"
#include <cmath>

namespace cg
{
	namespace math
	{
		template <class T>
		class Quaternion
		{
		public:
			T &w;
			T &x;
			T &y;
			T &z;

			Quaternion<T>()
				: w(m_data[0]), x(m_data[1]), y(m_data[2]), z(m_data[3])
			{
				m_data[0] = 1;
				m_data[1] = 0;
				m_data[2] = 0;
				m_data[3] = 0;
			}

			Quaternion<T>(T w, T x, T y, T z)
				: w(m_data[0]), x(m_data[1]), y(m_data[2]), z(m_data[3])
			{
				m_data[0] = w;
				m_data[1] = x;
				m_data[2] = y;
				m_data[3] = z;
			}

			Quaternion<T>(const Quaternion<T> &other)
				: w(m_data[0]), x(m_data[1]), y(m_data[2]), z(m_data[3])
			{
				m_data[0] = other.w;
				m_data[1] = other.x;
				m_data[2] = other.y;
				m_data[3] = other.z;
			}

			Quaternion<T>(T val)
				: w(m_data[0]), x(m_data[1]), y(m_data[2]), z(m_data[3])
			{
				m_data[0] = val;
				m_data[1] = val;
				m_data[2] = val;
				m_data[3] = val;
			}
			Quaternion<T>(const T data[4])
				: w(m_data[0]), x(m_data[1]), y(m_data[2]), z(m_data[3])
			{
				m_data[0] = data[0];
				m_data[1] = data[1];
				m_data[2] = data[2];
				m_data[3] = data[3];
			}

			Quaternion<T>(T w, const Vector3<T> &vec)
				: w(m_data[0]), x(m_data[1]), y(m_data[2]), z(m_data[3])
			{
				m_data[0] = w;
				m_data[1] = vec.x;
				m_data[2] = vec.y;
				m_data[3] = vec.z;
			}

			// Get the raw array data of the quaternion.
			inline const T *getData() const
			{
				return m_data;
			}

			inline Vector3<T> getXYZ() const
			{
				return Vector3<T>(m_data + 1);
			}

			inline Vector4<T> getWXYZ() const
			{
				return Vector4<T>(m_data);
			}

			inline const T &operator[](int i) const
			{
				return m_data[i];
			}

			inline T &operator[](int i)
			{
				return m_data[i];
			}

			inline Quaternion<T> &operator=(const Quaternion<T> &other)
			{
				w = other.w;
				x = other.x;
				y = other.y;
				z = other.z;
				return *this;
			}

			inline Quaternion<T> operator*(const Quaternion<T> &other) const
			{
				Quaternion<T> result;
				result.w = w * other.w - x * other.x - y * other.y - z * other.z;
				result.x = w * other.x + x * other.w + y * other.z - z * other.y;
				result.y = w * other.y - x * other.z + y * other.w + z * other.x;
				result.z = w * other.z + x * other.y - y * other.x + z * other.w;
				return result;
			}

			inline Vector3<T> operator*(const Vector3<T> &vector) const
			{
				Vector3<T> qxyz = getXYZ();
				Vector3<T> uv = qxyz.cross(vector);
				Vector3<T> uuv = qxyz.cross(uv);
				uv *= (2.0f * w);
				uuv *= 2.0f;
				return vector + uv + uuv;
			}

			inline Vector4<T> operator*(const Vector4<T> &vector) const
			{
				return getMatrix() * vector;
			}

			inline Quaternion<T> &operator*=(const Quaternion<T> &other)
			{
				const T tempX = x;
				const T tempY = y;
				const T tempZ = z;
				w = w * other.w - tempX * other.x - tempY * other.y - tempZ * other.z;
				x = w * other.x + tempX * other.w + tempY * other.z - tempZ * other.y;
				y = w * other.y - tempX * other.z + tempY * other.w + tempZ * other.x;
				z = w * other.z + tempX * other.y - tempY * other.x + tempZ * other.w;
				return *this;
			}

			inline Quaternion<T> &normalize()
			{
				const T magnitude = getMagnitude();
				w /= magnitude;
				x /= magnitude;
				y /= magnitude;
				z /= magnitude;
				return *this;
			}

			inline Quaternion &invert()
			{
				x = -x;
				y = -y;
				z = -z;
				return *this;
			}

			inline Quaternion<T> &setIdentity() const
			{
				w = 1;
				x = y = z = 0;
				return *this;
			}

			inline Quaternion<T> getNormalized() const
			{
				const T magnitude = getMagnitude();
				return Quaternion<T>(w / magnitude, x / magnitude, y / magnitude, z / magnitude);
			}

			inline Quaternion<T> getInverse() const
			{
				Quaternion<T> inverse(*this);
				inverse.invert();
				return inverse;
			}

			inline T getMagnitudeSqr() const
			{
				return w * w + x * x + y * y + z * z;
			}

			inline T getMagnitude() const
			{
				return std::sqrt(getMagnitudeSqr());
			}

			inline Matrix4<T> getMatrix() const
			{
				const T xx = 2 * x * x;
				const T yy = 2 * y * y;
				const T zz = 2 * z * z;
				const T xy = 2 * x * y;
				const T xz = 2 * x * z;
				const T yz = 2 * y * z;
				const T wx = 2 * w * x;
				const T wy = 2 * w * y;
				const T wz = 2 * w * z;

				Matrix4<T> matrix;
				matrix.set(1 - yy - zz, xy - wz, xz + wy, 0,
						   xy + wz, 1 - xx - zz, yz - wx, 0,
						   xz - wy, yz + wx, 1 - xx - yy, 0,
						   0, 0, 0, 1);
				return matrix;
			}

			inline Vector3<T> toAngleAxis() const
			{
				Vector3<T> axisAngle = toAngleAxis();
				axisAngle.x = radianToAngle(x);
				axisAngle.y = radianToAngle(y);
				axisAngle.z = radianToAngle(z);
				return axisAngle;
			}

			inline Vector4<T> toAngleAxisRad() const
			{
				if (w > 1)
				{
					normalize();
				}

				Vector4<T> axisAngle;
				T scale = std::sqrt(1 - w * w);
				if (scale < 0.00001)
				{
					axisAngle.w = 0;
					axisAngle.x = 1;
					axisAngle.y = 0;
					axisAngle.z = 0;
				}
				else
				{
					T invS = 1.0f / scale;
					axisAngle.w = 2 * std::acos(w);
					axisAngle.x = x * invS;
					axisAngle.y = y * invS;
					axisAngle.z = z * invS;
				}
				return axisAngle;
			}

			inline Vector3<T> toEuler() const
			{
				Vector3<T> euler = toEulerRad();
				euler.x = radianToAngle(euler.x);
				euler.y = radianToAngle(euler.y);
				euler.z = radianToAngle(euler.z);
				return euler;
			}

			inline Vector3<T> toEulerRad() const
			{
				T ww = w * w;
				T xx = x * x;
				T yy = y * y;
				T zz = z * z;
				T unit = xx + yy + zz + ww;
				T test = x * y + z * w;

				Vector3<T> euler;
				if (test > 0.49999 * unit)
				{
					// heading = rotation about y-axis
					euler.y = (T)(2 * std::atan2(x, w));
					// bank = rotation about x-axis
					euler.x = 0;
					// attitude = rotation about z-axis
					euler.z = (T)CG_PI_2;
				}
				if (test < -0.49999 * unit)
				{
					// heading = rotation about y-axis
					euler.y = (T)(-2 * std::atan2(x, w));
					// bank = rotation about x-axis
					euler.x = 0;
					// attitude = rotation about z-axis
					euler.z = (T)-CG_PI_2;
				}
				else
				{
					// heading = rotation about y-axis
					euler.y = (T)std::atan2(2 * (y * w - x * z), xx - yy - zz + ww);
					// bank = rotation about x-axis
					euler.x = (T)std::atan2(2 * (x * w - y * z), yy + ww - xx - zz);
					// attitude = rotation about z-axis
					euler.z = (T)std::asin(2 * test / unit);
				}
				return euler;
			}

			inline Quaternion &fromAxisAngle(T angle, const Vector3<T> &axis)
			{
				return fromAxisAngleRad(angleToRadian(angle), axis);
			}

			inline Quaternion &fromAxisAngle(const Vector4<T> &axisAngle)
			{
				return fromAxisAngleRad(angleToRadian(axisAngle.w), axisAngle);
			}

			inline Quaternion &fromAxisAngleRad(const Vector4<T> &axisAngle)
			{
				return fromAxisAngleRad(axisAngle.w, axisAngle);
			}

			Quaternion &fromAxisAngleRad(T angle, const Vector3<T> &axis)
			{
				const T halfAngle = (T)0.5 * angle;
				const T sinAngle = std::sin(halfAngle);
				w = std::cos(halfAngle);
				x = axis.x * sinAngle;
				y = axis.y * sinAngle;
				z = axis.z * sinAngle;
				return *this;
			}

			inline Quaternion &fromEuler(const Vector3<T> &euler)
			{
				return fromEulerRad(angleToRadian(euler.x), angleToRadian(euler.y), angleToRadian(euler.z));
			}

			inline Quaternion &fromEuler(T eulerX, T eulerY, T eulerZ)
			{
				return fromEulerRad(angleToRadian(eulerX), angleToRadian(eulerY), angleToRadian(eulerZ));
			}

			inline Quaternion &fromEulerRad(const Vector3<T> &euler)
			{
				return fromEulerRad(euler.x, euler.y, euler.z);
			}

			Quaternion &fromEulerRad(T eulerX, T eulerY, T eulerZ)
			{
				const T bank = (T)0.5 * eulerX;
				const T heading = (T)0.5 * eulerY;
				const T attitude = (T)0.5 * eulerZ;
				const T sb = std::sin(bank);
				const T cb = std::cos(bank);
				const T sa = std::sin(attitude);
				const T ca = std::cos(attitude);
				const T sh = std::sin(heading);
				const T ch = std::cos(heading);
				const T chca = ch * ca;
				const T shca = sh * ca;
				const T chsa = ch * sa;
				const T shsa = sh * sa;
				w = chca * cb - shsa * sb;
				x = shsa * cb + chca * sb;
				y = shca * cb + chsa * sb;
				z = chsa * cb - shca * sb;
				return *this;
			}

		private:
			T m_data[4];
		};

		typedef Quaternion<float> QuaternionF;
		typedef Quaternion<double> QuaternionD;
	}
}
