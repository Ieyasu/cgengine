#pragma once

#include "Vector.h"

namespace cg
{
    namespace math
    {
        template <class T>
        class Vector2 : public VectorN<T, Vector2<T>, 2>
        {
        public:
            T& x;
            T& y;

            Vector2<T>()
                : x(this->m_data[0])
                , y(this->m_data[1])
            {
                this->m_data[0] = 0;
                this->m_data[1] = 0;
            }

            explicit Vector2<T>(T val)
                : x(this->m_data[0])
                , y(this->m_data[1])
            {
                this->m_data[0] = val;
                this->m_data[1] = val;
            }

            Vector2<T>(T x, T y)
                : x(this->m_data[0])
                , y(this->m_data[1])
            {
                this->m_data[0] = x;
                this->m_data[1] = y;
            }

            Vector2<T>(const Vector2<T>& other)
                : x(this->m_data[0])
                , y(this->m_data[1])
            {
                this->m_data[0] = other.x;
                this->m_data[1] = other.y;
            }

            explicit Vector2<T>(const float data[2])
                : x(this->m_data[0])
                , y(this->m_data[1])
            {
                this->m_data[0] = data[0];
                this->m_data[1] = data[1];
            }

            template <class T2, class S2, unsigned int U2>
            Vector2<T>(const VectorN<T2, S2, U2>& other)
                : x(this->m_data[0])
                , y(this->m_data[1])
            {
                for (int i = 0; i < U2 && i < 2; ++i)
                {
                    this->m_data[i] = (T)other[i];
                }
                for (int i = U2; i < 2; ++i)
                {
                    this->m_data[i] = (T)0;
                }
            }

            inline Vector2<T>& operator=(const Vector2<T>& other)
            {
                x = other.x;
                y = other.y;
                return *this;
            }

            inline T min() const
            {
                return x < y ? x : y;
            }

            inline T max() const
            {
                return x > y ? x : y;
            }

            inline static Vector2<T> getUp()
            {
                return Vector2<T>(0, 1);
            }

            inline static Vector2<T> getRight()
            {
                return Vector2<T>(1, 0);
            }
        };

        typedef Vector2<double> Vector2d;
        typedef Vector2<float>  Vector2f;
        typedef Vector2<int>    Vector2i;
    }  // namespace math
}  // namespace cg
