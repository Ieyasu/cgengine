#pragma once

#include <cfloat>
#include <vector>

#include "Vector3.h"

namespace cg
{
    namespace math
    {
        template <class T>
        class Bounds
        {
        public:
            Bounds<T>();
            Bounds<T>(const Vector3<T>& min, const Vector3<T>& max);
            Bounds<T>(const std::vector<Vector3<T>>& points);

            inline void              setMin(const Vector3<T>& min);
            inline void              setMax(const Vector3<T>& max);
            inline const Vector3<T>& getMin() const;
            inline const Vector3<T>& getMax() const;
            inline const Vector3<T>& getCenter() const;
            inline const Vector3<T>& getSize() const;

            inline void encapsulate(const Vector3<T>& point);
            inline void encapsulate(const Bounds<T>& bounds);

        private:
            Vector3<T> m_min;
            Vector3<T> m_max;
            Vector3<T> m_size;
            Vector3<T> m_center;

            inline void recalculate();
        };

        template <class T>
        Bounds<T>::Bounds()
            : m_min()
            , m_max()
        {
            recalculate();
        }

        template <class T>
        Bounds<T>::Bounds(const Vector3<T>& min, const Vector3<T>& max)
            : m_min(min)
            , m_max(max)
        {
            recalculate();
        }

        template <class T>
        Bounds<T>::Bounds(const std::vector<Vector3<T>>& points)
            : m_min(FLT_MAX)
            , m_max(-FLT_MAX)
        {
            for (const auto& point : points)
            {
                for (int i = 0; i < 3; ++i)
                {
                    if (point[i] < m_min[i])
                        m_min[i] = point[i];
                    if (point[i] > m_max[i])
                        m_max[i] = point[i];
                }
            }
            for (int i = 0; i < 3; ++i)
            {
                if (m_min[i] > m_max[i])
                {
                    m_min[i] = 0;
                    m_max[i] = 0;
                }
            }
        }

        template <class T>
        inline void Bounds<T>::setMin(const Vector3<T>& min)
        {
            m_min = min;
            recalculate();
        }

        template <class T>
        inline void Bounds<T>::setMax(const Vector3<T>& max)
        {
            m_max = max;
            recalculate();
        }

        template <class T>
        inline void Bounds<T>::encapsulate(const Vector3<T>& point)
        {
            for (int i = 0; i < 3; ++i)
            {
                if (point[i] < m_min[i])
                    m_min[i] = point[i];
                if (point[i] > m_max[i])
                    m_max[i] = point[i];
            }

            recalculate();
        }

        template <class T>
        inline void Bounds<T>::encapsulate(const Bounds<T>& bounds)
        {
            for (int i = 0; i < 3; ++i)
            {
                if (bounds.m_min[i] < m_min[i])
                    m_min[i] = bounds.m_min[i];
                if (bounds.m_max[i] > m_max[i])
                    m_max[i] = bounds.m_max[i];
            }

            recalculate();
        }

        template <class T>
        inline const Vector3<T>& Bounds<T>::getMin() const
        {
            return m_min;
        }

        template <class T>
        inline const Vector3<T>& Bounds<T>::getMax() const
        {
            return m_max;
        }

        template <class T>
        inline const Vector3<T>& Bounds<T>::getCenter() const
        {
            return m_center;
        }

        template <class T>
        inline const Vector3<T>& Bounds<T>::getSize() const
        {
            return m_size;
        }

        template <class T>
        inline void Bounds<T>::recalculate()
        {
            m_center = (m_max + m_min) / 2.0f;
            m_size = m_max - m_min;
        }

        typedef Bounds<float>  BoundsF;
        typedef Bounds<double> BoundsD;
    }  // namespace math
}  // namespace cg
