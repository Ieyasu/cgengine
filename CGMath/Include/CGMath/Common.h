#pragma once

#include <cstdint>

#define CG_PI 3.14159265358979323846
#define CG_PI_2 1.57079632679489661923

namespace cg
{
	namespace math
	{
		template<class T>
		inline T angleToRadian(T angle)
		{
			return angle * (T)CG_PI / (T)180.0;
		}

		template<class T>
		inline T radianToAngle(T angle)
		{
			return angle * (T)180.0 / (T)CG_PI;
		}

		template<class T>
		inline T clamp(T val, T min, T max)
		{
			if (val < min) return min;
			if (val > max) return max;
			return val;
		}

		template<class T>
		inline T equals(T val1, T val2, T maxError)
		{
			const T diff = val1 - val2;
			return diff < maxError && diff > -maxError;
		}

		inline uint16_t clampToPowerOfTwo(uint16_t x)
		{
			x--;
			x |= x >> 1;
			x |= x >> 2;
			x |= x >> 4;
			x |= x >> 8;
			x++;
			return x;
		}

		inline uint32_t clampToPowerOfTwo(uint32_t x)
		{
			x--;
			x |= x >> 1;
			x |= x >> 2;
			x |= x >> 4;
			x |= x >> 8;
			x |= x >> 16;
			x++;
			return x;
		}

		inline uint64_t clampToPowerOfTwo(uint64_t x)
		{
			x--;
			x |= x >> 1;
			x |= x >> 2;
			x |= x >> 4;
			x |= x >> 8;
			x |= x >> 32;
			x++;
			return x;
		}
	}
}