#pragma once

#include <cmath>

namespace cg
{
    namespace math
    {
        template <class T, class S, unsigned int U>
        class VectorN
        {
        public:
            virtual ~VectorN() = default;

            inline const T* getData() const
            {
                return m_data;
            }

            inline T& operator[](size_t i)
            {
                return m_data[i];
            }

            inline T operator[](size_t i) const
            {
                return m_data[i];
            }

            inline S& operator%=(T val)
            {
                for (size_t i = 0; i < U; ++i)
                {
                    m_data[i] %= val;
                }
                return static_cast<S&>(*this);
            }

            inline S& operator/=(T val)
            {
                for (size_t i = 0; i < U; ++i)
                {
                    m_data[i] /= val;
                }
                return static_cast<S&>(*this);
            }

            inline S& operator*=(T val)
            {
                for (size_t i = 0; i < U; ++i)
                {
                    m_data[i] *= val;
                }
                return static_cast<S&>(*this);
            }

            inline S& operator%=(const S& other)
            {
                for (size_t i = 0; i < U; ++i)
                {
                    m_data[i] %= other[i];
                }
                return static_cast<S&>(*this);
            }

            inline S& operator/=(const S& other)
            {
                for (size_t i = 0; i < U; ++i)
                {
                    m_data[i] /= other[i];
                }
                return static_cast<S&>(*this);
            }

            inline S& operator*=(const S& other)
            {
                for (size_t i = 0; i < U; ++i)
                {
                    m_data[i] *= other[i];
                }
                return static_cast<S&>(*this);
            }

            inline S& operator+=(const S& other)
            {
                for (size_t i = 0; i < U; ++i)
                {
                    m_data[i] += other[i];
                }
                return static_cast<S&>(*this);
            }

            inline S& operator-=(const S& other)
            {
                for (size_t i = 0; i < U; ++i)
                {
                    m_data[i] -= other[i];
                }
                return static_cast<S&>(*this);
            }

            inline S operator-() const
            {
                S result;
                for (size_t i = 0; i < U; ++i)
                {
                    result[i] = -m_data[i];
                }
                return result;
            }

            inline S operator%(T val) const
            {
                S result;
                for (size_t i = 0; i < U; ++i)
                {
                    result[i] = m_data[i] % val;
                }
                return result;
            }

            inline S operator/(T val) const
            {
                S result;
                for (size_t i = 0; i < U; ++i)
                {
                    result[i] = m_data[i] / val;
                }
                return result;
            }

            inline S operator*(T val) const
            {
                S result;
                for (size_t i = 0; i < U; ++i)
                {
                    result[i] = m_data[i] * val;
                }
                return result;
            }

            inline S operator%(const S& other) const
            {
                S result;
                for (size_t i = 0; i < U; ++i)
                {
                    result[i] = m_data[i] % other[i];
                }
                return result;
            }

            inline S operator/(const S& other) const
            {
                S result;
                for (size_t i = 0; i < U; ++i)
                {
                    result[i] = m_data[i] / other[i];
                }
                return result;
            }

            inline S operator*(const S& other) const
            {
                S result;
                for (size_t i = 0; i < U; ++i)
                {
                    result[i] = m_data[i] * other[i];
                }
                return result;
            }

            inline S operator+(const S& other) const
            {
                S result;
                for (size_t i = 0; i < U; ++i)
                {
                    result[i] = m_data[i] + other[i];
                }
                return result;
            }

            inline S operator-(const S& other) const
            {
                S result;
                for (size_t i = 0; i < U; ++i)
                {
                    result[i] = m_data[i] - other[i];
                }
                return result;
            }

            inline void normalize()
            {
                const float magnitude = getMagnitude();
                for (size_t i = 0; i < U; ++i)
                {
                    m_data[i] /= magnitude;
                }
            }

            inline S getNormalized() const
            {
                S result = *this;
                result.normalize();
                return result;
            }

            inline T getMagnitudeSqr() const
            {
                T result = 0;
                for (size_t i = 0; i < U; ++i)
                {
                    result += m_data[i] * m_data[i];
                }
                return result;
            }

            inline T getMagnitude() const
            {
                return std::sqrt(getMagnitudeSqr());
            }

            inline static T dot(const VectorN<T, S, U>& vec1, const VectorN<T, S, U>& vec2)
            {
                T result = (T)0;
                for (size_t i = 0; i < U; ++i)
                {
                    result += vec1[i] * vec2[i];
                }
                return result;
            }

        protected:
            T m_data[U];
        };

        template <class T, class S, unsigned int U>
        inline S operator*(T val, const VectorN<T, S, U>& vec)
        {
            return vec * val;
        }

        template <class T, class S, unsigned int U>
        inline bool operator==(const VectorN<T, S, U>& lhs, const VectorN<T, S, U>& rhs)
        {
            for (size_t i = 0; i < U; ++i)
            {
                if (lhs[i] != rhs[i])
                {
                    return false;
                }
            }
            return true;
        }
    }  // namespace math
}  // namespace cg
