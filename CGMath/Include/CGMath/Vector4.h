#pragma once

#include "Vector.h"

namespace cg
{
    namespace math
    {
        template <class T>
        class Vector4 : public VectorN<T, Vector4<T>, 4>
        {
        public:
            T& x;
            T& y;
            T& z;
            T& w;

            Vector4<T>()
                : x(this->m_data[0])
                , y(this->m_data[1])
                , z(this->m_data[2])
                , w(this->m_data[3])
            {
                this->m_data[0] = 0;
                this->m_data[1] = 0;
                this->m_data[2] = 0;
                this->m_data[3] = 0;
            }

            explicit Vector4<T>(T val)
                : x(this->m_data[0])
                , y(this->m_data[1])
                , z(this->m_data[2])
                , w(this->m_data[3])
            {
                this->m_data[0] = val;
                this->m_data[1] = val;
                this->m_data[2] = val;
                this->m_data[3] = val;
            }

            Vector4<T>(T x, T y, T z, T w)
                : x(this->m_data[0])
                , y(this->m_data[1])
                , z(this->m_data[2])
                , w(this->m_data[3])
            {
                this->m_data[0] = x;
                this->m_data[1] = y;
                this->m_data[2] = z;
                this->m_data[3] = w;
            }

            Vector4<T>(const Vector4<T>& other)
                : x(this->m_data[0])
                , y(this->m_data[1])
                , z(this->m_data[2])
                , w(this->m_data[3])
            {
                this->m_data[0] = other.x;
                this->m_data[1] = other.y;
                this->m_data[2] = other.z;
                this->m_data[3] = other.w;
            }

            explicit Vector4<T>(const float data[4])
                : x(this->m_data[0])
                , y(this->m_data[1])
                , z(this->m_data[2])
                , w(this->m_data[3])
            {
                this->m_data[0] = data[0];
                this->m_data[1] = data[1];
                this->m_data[2] = data[2];
                this->m_data[3] = data[3];
            }

            template <class T2, class S2, unsigned int U2>
            Vector4<T>(const VectorN<T2, S2, U2>& other)
                : x(this->m_data[0])
                , y(this->m_data[1])
                , z(this->m_data[2])
                , w(this->m_data[3])
            {
                for (int i = 0; i < U2 && i < 4; ++i)
                {
                    this->m_data[i] = (T)other[i];
                }
                for (int i = U2; i < 4; ++i)
                {
                    this->m_data[i] = (T)0;
                }
            }

            inline Vector4<T>& operator=(const Vector4<T>& other)
            {
                x = other.x;
                y = other.y;
                z = other.z;
                w = other.w;
                return *this;
            }
        };

        typedef Vector4<double> Vector4d;
        typedef Vector4<float>  Vector4f;
        typedef Vector4<int>    Vector4i;
    }  // namespace math
}  // namespace cg
