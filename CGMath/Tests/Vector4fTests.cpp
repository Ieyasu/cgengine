#include "stdafx.h"
#include "CppUnitTest.h"
#include "Vector2.h"
#include "Vector3.h"
#include "Vector4.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;
using namespace cg::math;

namespace CGMathLibTests
{		
	TEST_CLASS(Vector4f_TestClass)
	{
	public:
		TEST_METHOD(Vector4f_ConstructorTest)
		{
			Vector4f vec1(FLT_MAX, 0.0f, -1.0f, 1.0f);
			Assert::AreEqual(FLT_MAX, vec1.x, L"Vector x-component not initialized correctly.", LINE_INFO());
			Assert::AreEqual(0.0f, vec1.y, L"Vector y-component not initialized correctly.", LINE_INFO());
			Assert::AreEqual(-1.0f, vec1.z, L"Vector z-component not initialized correctly.", LINE_INFO());
			Assert::AreEqual(1.0f, vec1.w, L"Vector w-component not initialized correctly.", LINE_INFO());

			Vector4f vec2(-2.5f);
			Assert::AreEqual(-2.5f, vec2.x, L"Vector x-component not initialized correctly.", LINE_INFO());
			Assert::AreEqual(-2.5f, vec2.y, L"Vector y-component not initialized correctly.", LINE_INFO());
			Assert::AreEqual(-2.5f, vec2.z, L"Vector z-component not initialized correctly.", LINE_INFO());
			Assert::AreEqual(-2.5f, vec2.w, L"Vector w-component not initialized correctly.", LINE_INFO());

			Vector4f vec3;
			Assert::AreEqual(0.0f, vec3.x, L"Vector x-component not initialized correctly.", LINE_INFO());
			Assert::AreEqual(0.0f, vec3.y, L"Vector y-component not initialized correctly.", LINE_INFO());
			Assert::AreEqual(0.0f, vec3.z, L"Vector z-component not initialized correctly.", LINE_INFO());
			Assert::AreEqual(0.0f, vec3.w, L"Vector w-component not initialized correctly.", LINE_INFO());

			Vector4f vec4 = Vector4f(vec1);
			Assert::AreEqual(FLT_MAX, vec4.x, L"Vector x-component not initialized correctly.", LINE_INFO());
			Assert::AreEqual(0.0f, vec4.y, L"Vector y-component not initialized correctly.", LINE_INFO());
			Assert::AreEqual(-1.0f, vec4.z, L"Vector z-component not initialized correctly.", LINE_INFO());
			Assert::AreEqual(1.0f, vec4.w, L"Vector w-component not initialized correctly.", LINE_INFO());

			Vector4f vec5 = vec2;
			Assert::AreEqual(-2.5f, vec5.x, L"Vector x-component not initialized correctly.", LINE_INFO());
			Assert::AreEqual(-2.5f, vec5.y, L"Vector y-component not initialized correctly.", LINE_INFO());
			Assert::AreEqual(-2.5f, vec5.z, L"Vector z-component not initialized correctly.", LINE_INFO());
			Assert::AreEqual(-2.5f, vec5.w, L"Vector w-component not initialized correctly.", LINE_INFO());

			const float data[4] = { 0.5f, -0.5f, FLT_MAX, FLT_MIN };
			Vector4f vec6 = Vector4f(data);
			Assert::AreEqual(0.5f, vec6.x, L"Vector x-component not initialized correctly.", LINE_INFO());
			Assert::AreEqual(-0.5f, vec6.y, L"Vector y-component not initialized correctly.", LINE_INFO());
			Assert::AreEqual(FLT_MAX, vec6.z, L"Vector z-component not initialized correctly.", LINE_INFO());
			Assert::AreEqual(FLT_MIN, vec6.w, L"Vector w-component not initialized correctly.", LINE_INFO());

			Vector4f vec7 = Vector3f(1.0f, 0.5f, 2.0f);
			Assert::AreEqual(1.0f, vec7.x, L"Vector x-component not initialized correctly.", LINE_INFO());
			Assert::AreEqual(0.5f, vec7.y, L"Vector y-component not initialized correctly.", LINE_INFO());
			Assert::AreEqual(2.0f, vec7.z, L"Vector z-component not initialized correctly.", LINE_INFO());
			Assert::AreEqual(0.0f, vec7.w, L"Vector w-component not initialized correctly.", LINE_INFO());

			Vector4f vec8 = Vector4f(Vector3f(5, -5, 10));
			Assert::AreEqual(5.0f, vec8.x, L"Vector x-component not initialized correctly.", LINE_INFO());
			Assert::AreEqual(-5.0f, vec8.y, L"Vector y-component not initialized correctly.", LINE_INFO());
			Assert::AreEqual(10.0f, vec8.z, L"Vector z-component not initialized correctly.", LINE_INFO());
			Assert::AreEqual(0.0f, vec8.w, L"Vector w-component not initialized correctly.", LINE_INFO());

			Vector4f vec9 = Vector2f(1.0f, 0.5f);
			Assert::AreEqual(1.0f, vec9.x, L"Vector x-component not initialized correctly.", LINE_INFO());
			Assert::AreEqual(0.5f, vec9.y, L"Vector y-component not initialized correctly.", LINE_INFO());
			Assert::AreEqual(0.0f, vec9.z, L"Vector z-component not initialized correctly.", LINE_INFO());
			Assert::AreEqual(0.0f, vec9.w, L"Vector w-component not initialized correctly.", LINE_INFO());

			Vector4f vec10 = Vector4f(Vector2f(5, -5));
			Assert::AreEqual(5.0f, vec10.x, L"Vector x-component not initialized correctly.", LINE_INFO());
			Assert::AreEqual(-5.0f, vec10.y, L"Vector y-component not initialized correctly.", LINE_INFO());
			Assert::AreEqual(0.0f, vec10.z, L"Vector z-component not initialized correctly.", LINE_INFO());
			Assert::AreEqual(0.0f, vec10.w, L"Vector w-component not initialized correctly.", LINE_INFO());

			Vector4f vec11 = Vector2i(2, -2);
			Assert::AreEqual(2.0f, vec11.x, L"Vector x-component not initialized correctly.", LINE_INFO());
			Assert::AreEqual(-2.0f, vec11.y, L"Vector y-component not initialized correctly.", LINE_INFO());
			Assert::AreEqual(0.0f, vec11.z, L"Vector z-component not initialized correctly.", LINE_INFO());
			Assert::AreEqual(0.0f, vec11.w, L"Vector w-component not initialized correctly.", LINE_INFO());

			Vector4f vec12 = Vector4i(2, -2, 1000, -1000);
			Assert::AreEqual(2.0f, vec12.x, L"Vector x-component not initialized correctly.", LINE_INFO());
			Assert::AreEqual(-2.0f, vec12.y, L"Vector y-component not initialized correctly.", LINE_INFO());
			Assert::AreEqual(1000.0f, vec12.z, L"Vector z-component not initialized correctly.", LINE_INFO());
			Assert::AreEqual(-1000.0f, vec12.w, L"Vector w-component not initialized correctly.", LINE_INFO());
		}

		TEST_METHOD(Vector4f_DataAccessTest)
		{
			Vector4f vec1 = Vector4f(0.5f, -0.5f, 1.0f, -1.0f);
			Assert::AreEqual(0.5f, vec1[0], L"Vector x-component not initialized correctly.", LINE_INFO());
			Assert::AreEqual(-0.5f, vec1[1], L"Vector y-component not initialized correctly.", LINE_INFO());
			Assert::AreEqual(1.0f, vec1[2], L"Vector z-component not initialized correctly.", LINE_INFO());
			Assert::AreEqual(-1.0f, vec1[3], L"Vector w-component not initialized correctly.", LINE_INFO());
			vec1[0] -= 1.0f;
			vec1[1] += 1.0f;
			vec1[2] *= 2.0f;
			vec1[3] /= 2.0f;
			Assert::AreEqual(-0.5f, vec1[0], L"Vector x-component not initialized correctly.", LINE_INFO());
			Assert::AreEqual(0.5f, vec1[1], L"Vector y-component not initialized correctly.", LINE_INFO());
			Assert::AreEqual(2.0f, vec1[2], L"Vector z-component not initialized correctly.", LINE_INFO());
			Assert::AreEqual(-0.5f, vec1[3], L"Vector w-component not initialized correctly.", LINE_INFO());
			Assert::AreEqual(-0.5f, vec1.getData()[0], L"Vector x-component not initialized correctly.", LINE_INFO());
			Assert::AreEqual(0.5f, vec1.getData()[1], L"Vector y-component not initialized correctly.", LINE_INFO());
			Assert::AreEqual(2.0f, vec1.getData()[2], L"Vector z-component not initialized correctly.", LINE_INFO());
			Assert::AreEqual(-0.5f, vec1.getData()[3], L"Vector w-component not initialized correctly.", LINE_INFO());
		}

		TEST_METHOD(Vector4f_AdditionTest)
		{
			Vector4f vec1(0, 1.0f, 2.0f, 3.0f);
			Vector4f vec2(-1.0f, 1.0f, -2.0f, 2.0f);
			Vector4f vec3 = vec1 + vec2;
			Assert::AreEqual(-1.0f, vec3.x, L"Vector x-component not calculated correctly.", LINE_INFO());
			Assert::AreEqual(2.0f, vec3.y, L"Vector y-component not calculated correctly.", LINE_INFO());
			Assert::AreEqual(0.0f, vec3.z, L"Vector z-component not calculated correctly.", LINE_INFO());
			Assert::AreEqual(5.0f, vec3.w, L"Vector w-component not calculated correctly.", LINE_INFO());

			vec3 += Vector4f(-1.0f, -1.0f, 5.0f, -5.0f);
			Assert::AreEqual(-2.0f, vec3.x, L"Vector x-component not calculated correctly.", LINE_INFO());
			Assert::AreEqual(1.0f, vec3.y, L"Vector y-component not calculated correctly.", LINE_INFO());
			Assert::AreEqual(5.0f, vec3.z, L"Vector z-component not calculated correctly.", LINE_INFO());
			Assert::AreEqual(0.0f, vec3.w, L"Vector w-component not calculated correctly.", LINE_INFO());
		}

		TEST_METHOD(Vector4f_SubtractTest)
		{
			Vector4f vec1(0, 1.0f, 2.0f, 3.0f);
			Vector4f vec2(-1.0f, 1.0f, -2.0f, 2.0f);
			Vector4f vec3 = vec1 - vec2;
			Assert::AreEqual(1.0f, vec3.x, L"Vector x-component not calculated correctly.", LINE_INFO());
			Assert::AreEqual(0.0f, vec3.y, L"Vector y-component not calculated correctly.", LINE_INFO());
			Assert::AreEqual(4.0f, vec3.z, L"Vector z-component not calculated correctly.", LINE_INFO());
			Assert::AreEqual(1.0f, vec3.w, L"Vector w-component not calculated correctly.", LINE_INFO());

			vec3 -= Vector4f(-1.0f, -1.0f, 5.0f, -5.0f);
			Assert::AreEqual(2.0f, vec3.x, L"Vector x-component not calculated correctly.", LINE_INFO());
			Assert::AreEqual(1.0f, vec3.y, L"Vector y-component not calculated correctly.", LINE_INFO());
			Assert::AreEqual(-1.0f, vec3.z, L"Vector z-component not calculated correctly.", LINE_INFO());
			Assert::AreEqual(6.0f, vec3.w, L"Vector w-component not calculated correctly.", LINE_INFO());
		}

		TEST_METHOD(Vector4f_DivisionTest)
		{
			Vector4f vec1(0, 1.0f, 2.0f, 3.0f);
			Vector4f vec2(-1.0f, 1.0f, -2.0f, 2.0f);
			Vector4f vec3 = vec1 / vec2;
			Assert::AreEqual(0.0f, vec3.x, L"Vector x-component not calculated correctly.", LINE_INFO());
			Assert::AreEqual(1.0f, vec3.y, L"Vector y-component not calculated correctly.", LINE_INFO());
			Assert::AreEqual(-1.0f, vec3.z, L"Vector z-component not calculated correctly.", LINE_INFO());
			Assert::AreEqual(1.5f, vec3.w, L"Vector w-component not calculated correctly.", LINE_INFO());

			vec3 /= Vector4f(-1.0f, -1.0f, 5.0f, -0.5f);
			Assert::AreEqual(0.0f, vec3.x, L"Vector x-component not calculated correctly.", LINE_INFO());
			Assert::AreEqual(-1.0f, vec3.y, L"Vector y-component not calculated correctly.", LINE_INFO());
			Assert::AreEqual(-0.2f, vec3.z, L"Vector z-component not calculated correctly.", LINE_INFO());
			Assert::AreEqual(-3.0f, vec3.w, L"Vector w-component not calculated correctly.", LINE_INFO());

			vec3 /= -2.0f;
			Assert::AreEqual(0.0f, vec3.x, L"Vector x-component not calculated correctly.", LINE_INFO());
			Assert::AreEqual(0.5f, vec3.y, L"Vector y-component not calculated correctly.", LINE_INFO());
			Assert::AreEqual(0.1f, vec3.z, L"Vector z-component not calculated correctly.", LINE_INFO());
			Assert::AreEqual(1.5f, vec3.w, L"Vector w-component not calculated correctly.", LINE_INFO());

			vec3 = Vector4f(1.0f, 2.0f, 3.0f, 4.0f) / 4.0f;
			Assert::AreEqual(0.25f, vec3.x, L"Vector x-component not calculated correctly.", LINE_INFO());
			Assert::AreEqual(0.5f, vec3.y, L"Vector y-component not calculated correctly.", LINE_INFO());
			Assert::AreEqual(0.75f, vec3.z, L"Vector z-component not calculated correctly.", LINE_INFO());
			Assert::AreEqual(1.0f, vec3.w, L"Vector w-component not calculated correctly.", LINE_INFO());
		}

		TEST_METHOD(Vector4f_MultiplicationTest)
		{
			Vector4f vec1(0, 1.0f, 2.0f, 3.0f);
			Vector4f vec2(-1.0f, 1.0f, -2.0f, 2.0f);
			Vector4f vec3 = vec1 * vec2;
			Assert::AreEqual(0.0f, vec3.x, L"Vector x-component not calculated correctly.", LINE_INFO());
			Assert::AreEqual(1.0f, vec3.y, L"Vector y-component not calculated correctly.", LINE_INFO());
			Assert::AreEqual(-4.0f, vec3.z, L"Vector z-component not calculated correctly.", LINE_INFO());
			Assert::AreEqual(6.0f, vec3.w, L"Vector w-component not calculated correctly.", LINE_INFO());

			vec3 *= Vector4f(-1.0f, -1.0f, 5.0f, -0.5f);
			Assert::AreEqual(0.0f, vec3.x, L"Vector x-component not calculated correctly.", LINE_INFO());
			Assert::AreEqual(-1.0f, vec3.y, L"Vector y-component not calculated correctly.", LINE_INFO());
			Assert::AreEqual(-20.0f, vec3.z, L"Vector z-component not calculated correctly.", LINE_INFO());
			Assert::AreEqual(-3.0f, vec3.w, L"Vector w-component not calculated correctly.", LINE_INFO());

			vec3 *= -2.0f;
			Assert::AreEqual(0.0f, vec3.x, L"Vector x-component not calculated correctly.", LINE_INFO());
			Assert::AreEqual(2.0f, vec3.y, L"Vector y-component not calculated correctly.", LINE_INFO());
			Assert::AreEqual(40.0f, vec3.z, L"Vector z-component not calculated correctly.", LINE_INFO());
			Assert::AreEqual(6.0f, vec3.w, L"Vector w-component not calculated correctly.", LINE_INFO());

			vec3 = Vector4f(1.0f, 2.0f, 3.0f, 4.0f) * 4.0f;
			Assert::AreEqual(4.0f, vec3.x, L"Vector x-component not calculated correctly.", LINE_INFO());
			Assert::AreEqual(8.0f, vec3.y, L"Vector y-component not calculated correctly.", LINE_INFO());
			Assert::AreEqual(12.0f, vec3.z, L"Vector z-component not calculated correctly.", LINE_INFO());
			Assert::AreEqual(16.0f, vec3.w, L"Vector w-component not calculated correctly.", LINE_INFO());
		}

		TEST_METHOD(Vector4f_NegationTest)
		{
			Vector4f vec1(0, 1.0f, 2.0f, -3.0f);
			Vector4f vec2 = -vec1;
			Assert::AreEqual(0.0f, vec2.x, L"Vector x-component not calculated correctly.", LINE_INFO());
			Assert::AreEqual(-1.0f, vec2.y, L"Vector y-component not calculated correctly.", LINE_INFO());
			Assert::AreEqual(-2.0f, vec2.z, L"Vector z-component not calculated correctly.", LINE_INFO());
			Assert::AreEqual(3.0f, vec2.w, L"Vector w-component not calculated correctly.", LINE_INFO());
		}

		TEST_METHOD(Vector4f_NormalizeTest)
		{
			Vector4f vec1(0, 10.0f, 0, 0);
			Vector4f vec2 = vec1.getNormalized();
			float magnitude1 = vec1.getMagnitude();
			float magnitudesqr1 = vec1.getMagnitudeSqr();
			Assert::AreEqual(10.0f, magnitude1, L"Magnitude not calculated correctly.", LINE_INFO());
			Assert::AreEqual(100.0f, magnitudesqr1, L"Magnitude not calculated correctly.", LINE_INFO());
			magnitude1 = vec2.getMagnitude();
			magnitudesqr1 = vec2.getMagnitudeSqr();
			Assert::AreEqual(0.0f, vec2.x, L"Vector x-component not calculated correctly.", LINE_INFO());
			Assert::AreEqual(1.0f, vec2.y, L"Vector y-component not calculated correctly.", LINE_INFO());
			Assert::AreEqual(0.0f, vec2.z, L"Vector z-component not calculated correctly.", LINE_INFO());
			Assert::AreEqual(0.0f, vec2.w, L"Vector w-component not calculated correctly.", LINE_INFO());
			Assert::AreEqual(1.0f, magnitude1, L"Magnitude not calculated correctly.", LINE_INFO());
			Assert::AreEqual(1.0f, magnitudesqr1, L"Magnitude not calculated correctly.", LINE_INFO());

			vec1.y *= -1;
			vec1.normalize();
			magnitude1 = vec1.getMagnitude();
			magnitudesqr1 = vec1.getMagnitudeSqr();
			Assert::AreEqual(0.0f, vec1.x, L"Vector x-component not calculated correctly.", LINE_INFO());
			Assert::AreEqual(-1.0f, vec1.y, L"Vector y-component not calculated correctly.", LINE_INFO());
			Assert::AreEqual(0.0f, vec1.z, L"Vector z-component not calculated correctly.", LINE_INFO());
			Assert::AreEqual(0.0f, vec1.w, L"Vector w-component not calculated correctly.", LINE_INFO());
			Assert::AreEqual(1.0f, magnitude1, L"Magnitude not calculated correctly.", LINE_INFO());
			Assert::AreEqual(1.0f, magnitudesqr1, L"Magnitude not calculated correctly.", LINE_INFO());

			vec1 = Vector4f(-1.0f, 1.0f, -1.0f, -1.0f);
			vec2 = vec1.getNormalized();
			magnitude1 = vec1.getMagnitude();
			magnitudesqr1 = vec1.getMagnitudeSqr();
			Assert::AreEqual(2.0f, magnitude1, L"Magnitude not calculated correctly.", LINE_INFO());
			Assert::AreEqual(4.0f, magnitudesqr1, L"Magnitude not calculated correctly.", LINE_INFO());
			magnitude1 = vec2.getMagnitude();
			magnitudesqr1 = vec2.getMagnitudeSqr();
			Assert::AreEqual(-0.5f, vec2.x, L"Vector x-component not calculated correctly.", LINE_INFO());
			Assert::AreEqual(0.5f, vec2.y, L"Vector y-component not calculated correctly.", LINE_INFO());
			Assert::AreEqual(-0.5f, vec2.z, L"Vector z-component not calculated correctly.", LINE_INFO());
			Assert::AreEqual(-0.5f, vec2.w, L"Vector w-component not calculated correctly.", LINE_INFO());
			Assert::AreEqual(1.0f, magnitude1, L"Magnitude not calculated correctly.", LINE_INFO());
			Assert::AreEqual(1.0f, magnitudesqr1, L"Magnitude not calculated correctly.", LINE_INFO());

			vec1 *= -1;
			vec1.normalize();
			magnitude1 = vec1.getMagnitude();
			magnitudesqr1 = vec1.getMagnitudeSqr();
			Assert::AreEqual(0.5f, vec1.x, L"Vector x-component not calculated correctly.", LINE_INFO());
			Assert::AreEqual(-0.5f, vec1.y, L"Vector y-component not calculated correctly.", LINE_INFO());
			Assert::AreEqual(0.5f, vec1.z, L"Vector z-component not calculated correctly.", LINE_INFO());
			Assert::AreEqual(0.5f, vec1.w, L"Vector w-component not calculated correctly.", LINE_INFO());
			Assert::AreEqual(1.0f, magnitude1, L"Magnitude not calculated correctly.", LINE_INFO());
			Assert::AreEqual(1.0f, magnitudesqr1, L"Magnitude not calculated correctly.", LINE_INFO());
		}
	};
}