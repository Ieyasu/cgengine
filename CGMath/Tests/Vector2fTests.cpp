#include "stdafx.h"
#include "CppUnitTest.h"
#include "Vector2.h"
#include "Vector3.h"
#include "Vector4.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;
using namespace cg::math;

namespace CGMathLibTests
{
	TEST_CLASS(Vector2f_TestClass)
	{
	public:
		TEST_METHOD(Vector2f_ConstructorTest)
		{
			Vector2f vec1(FLT_MAX, 1.0f);
			Assert::AreEqual(FLT_MAX, vec1.x, L"Vector x-component not initialized correctly.", LINE_INFO());
			Assert::AreEqual(1.0f, vec1.y, L"Vector y-component not initialized correctly.", LINE_INFO());

			Vector2f vec2(-2.5f);
			Assert::AreEqual(-2.5f, vec2.x, L"Vector x-component not initialized correctly.", LINE_INFO());
			Assert::AreEqual(-2.5f, vec2.y, L"Vector y-component not initialized correctly.", LINE_INFO());

			Vector2f vec3;
			Assert::AreEqual(0.0f, vec3.x, L"Vector x-component not initialized correctly.", LINE_INFO());
			Assert::AreEqual(0.0f, vec3.y, L"Vector y-component not initialized correctly.", LINE_INFO());

			Vector2f vec4 = Vector3f(vec1);
			Assert::AreEqual(FLT_MAX, vec4.x, L"Vector x-component not initialized correctly.", LINE_INFO());
			Assert::AreEqual(1.0f, vec4.y, L"Vector y-component not initialized correctly.", LINE_INFO());

			Vector2f vec5 = vec2;
			Assert::AreEqual(-2.5f, vec5.x, L"Vector x-component not initialized correctly.", LINE_INFO());
			Assert::AreEqual(-2.5f, vec5.y, L"Vector y-component not initialized correctly.", LINE_INFO());

			const float data[2] = { 0.5f, -0.5f };
			Vector2f vec6 = Vector3f(data);
			Assert::AreEqual(0.5f, vec6.x, L"Vector x-component not initialized correctly.", LINE_INFO());
			Assert::AreEqual(-0.5f, vec6.y, L"Vector y-component not initialized correctly.", LINE_INFO());

			Vector2f vec7 = Vector3f(1.0f, 0.5f, -1.0f);
			Assert::AreEqual(1.0f, vec7.x, L"Vector x-component not initialized correctly.", LINE_INFO());
			Assert::AreEqual(0.5f, vec7.y, L"Vector y-component not initialized correctly.", LINE_INFO());

			Vector2f vec8 = Vector2f(Vector3f(5, -5, -10));
			Assert::AreEqual(5.0f, vec8.x, L"Vector x-component not initialized correctly.", LINE_INFO());
			Assert::AreEqual(-5.0f, vec8.y, L"Vector y-component not initialized correctly.", LINE_INFO());

			Vector2f vec9 = Vector4f(1.0f, 0.5f, 2.0f, 3.0f);
			Assert::AreEqual(1.0f, vec9.x, L"Vector x-component not initialized correctly.", LINE_INFO());
			Assert::AreEqual(0.5f, vec9.y, L"Vector y-component not initialized correctly.", LINE_INFO());

			Vector2f vec10 = Vector2f(Vector4f(5, -5, 10, -10));
			Assert::AreEqual(5.0f, vec10.x, L"Vector x-component not initialized correctly.", LINE_INFO());
			Assert::AreEqual(-5.0f, vec10.y, L"Vector y-component not initialized correctly.", LINE_INFO());

			Vector2f vec11 = Vector2i(2, -2);
			Assert::AreEqual(2.0f, vec11.x, L"Vector x-component not initialized correctly.", LINE_INFO());
			Assert::AreEqual(-2.0f, vec11.y, L"Vector y-component not initialized correctly.", LINE_INFO());

			Vector2f vec12 = Vector4i(2, -2, 1, -1);
			Assert::AreEqual(2.0f, vec12.x, L"Vector x-component not initialized correctly.", LINE_INFO());
			Assert::AreEqual(-2.0f, vec12.y, L"Vector y-component not initialized correctly.", LINE_INFO());
		}

		TEST_METHOD(Vector2f_DirectionTest)
		{
			Vector2f up2 = Vector2f::getUp();
			Assert::AreEqual(0.0f, up2.x, L"Vector x-component not initialized correctly.", LINE_INFO());
			Assert::AreEqual(1.0f, up2.y, L"Vector y-component not initialized correctly.", LINE_INFO());
			Vector2f right2 = Vector2f::getRight();
			Assert::AreEqual(1.0f, right2.x, L"Vector x-component not initialized correctly.", LINE_INFO());
			Assert::AreEqual(0.0f, right2.y, L"Vector y-component not initialized correctly.", LINE_INFO());
		}

		TEST_METHOD(Vector2f_MinMaxTest)
		{
			Vector2f vec1(-1.0f, 1.0f);
			Assert::AreEqual(-1.0f, vec1.min(), L"Min component not found.", LINE_INFO());
			Assert::AreEqual(1.0f, vec1.max(), L"Max component not found.", LINE_INFO());
			Vector2f vec2(-15.0f, -15.0f);
			Assert::AreEqual(-15.0f, vec2.min(), L"Min component not found.", LINE_INFO());
			Assert::AreEqual(-15.0f, vec2.max(), L"Max component not found.", LINE_INFO());
			Vector2f vec3(15.0f, 15.0f);
			Assert::AreEqual(15.0f, vec3.min(), L"Min component not found.", LINE_INFO());
			Assert::AreEqual(15.0f, vec3.max(), L"Max component not found.", LINE_INFO());
			Vector2f vec4(1.0f, 2.0f);
			Assert::AreEqual(1.0f, vec4.min(), L"Min component not found.", LINE_INFO());
			Assert::AreEqual(2.0f, vec4.max(), L"Max component not found.", LINE_INFO());
			Vector2f vec5(2.0f, 1.0f);
			Assert::AreEqual(1.0f, vec5.min(), L"Min component not found.", LINE_INFO());
			Assert::AreEqual(2.0f, vec5.max(), L"Max component not found.", LINE_INFO());
			Vector2f vec6(-2.0f, -1.0f);
			Assert::AreEqual(-2.0f, vec6.min(), L"Min component not found.", LINE_INFO());
			Assert::AreEqual(-1.0f, vec6.max(), L"Max component not found.", LINE_INFO());
			Vector2f vec7(-1.0f, -2.0f);
			Assert::AreEqual(-2.0f, vec7.min(), L"Min component not found.", LINE_INFO());
			Assert::AreEqual(-1.0f, vec7.max(), L"Max component not found.", LINE_INFO());
			Vector2f vec8(FLT_MAX, FLT_MIN);
			Assert::AreEqual(FLT_MIN, vec8.min(), L"Min component not found.", LINE_INFO());
			Assert::AreEqual(FLT_MAX, vec8.max(), L"Max component not found.", LINE_INFO());
		}

		TEST_METHOD(Vector2f_DataAccessTest)
		{
			Vector2f vec1 = Vector2f(0.5f, -0.5f);
			Assert::AreEqual(0.5f, vec1[0], L"Vector x-component not initialized correctly.", LINE_INFO());
			Assert::AreEqual(-0.5f, vec1[1], L"Vector y-component not initialized correctly.", LINE_INFO());
			vec1[0] -= 1.0f;
			vec1[1] += 1.0f;
			Assert::AreEqual(-0.5f, vec1[0], L"Vector x-component not initialized correctly.", LINE_INFO());
			Assert::AreEqual(0.5f, vec1[1], L"Vector y-component not initialized correctly.", LINE_INFO());
			Assert::AreEqual(-0.5f, vec1.getData()[0], L"Vector x-component not initialized correctly.", LINE_INFO());
			Assert::AreEqual(0.5f, vec1.getData()[1], L"Vector y-component not initialized correctly.", LINE_INFO());
		}

		TEST_METHOD(Vector2f_AdditionTest)
		{
			Vector2f vec1(0, 1.0f);
			Vector2f vec2(-1.0f, 1.0f);
			Vector2f vec3 = vec1 + vec2;
			Assert::AreEqual(-1.0f, vec3.x, L"Vector x-component not calculated correctly.", LINE_INFO());
			Assert::AreEqual(2.0f, vec3.y, L"Vector y-component not calculated correctly.", LINE_INFO());

			vec3 += Vector2f(-1.0f, -1.0f);
			Assert::AreEqual(-2.0f, vec3.x, L"Vector x-component not calculated correctly.", LINE_INFO());
			Assert::AreEqual(1.0f, vec3.y, L"Vector y-component not calculated correctly.", LINE_INFO());
		}

		TEST_METHOD(Vector2f_SubtractTest)
		{
			Vector2f vec1(0, 1.0f);
			Vector2f vec2(-1.0f, 1.0f);
			Vector2f vec3 = vec1 - vec2;
			Assert::AreEqual(1.0f, vec3.x, L"Vector x-component not calculated correctly.", LINE_INFO());
			Assert::AreEqual(0.0f, vec3.y, L"Vector y-component not calculated correctly.", LINE_INFO());

			vec3 -= Vector2f(-1.0f, -1.0f);
			Assert::AreEqual(2.0f, vec3.x, L"Vector x-component not calculated correctly.", LINE_INFO());
			Assert::AreEqual(1.0f, vec3.y, L"Vector y-component not calculated correctly.", LINE_INFO());
		}

		TEST_METHOD(Vector2f_DivisionTest)
		{
			Vector2f vec1(0, 1.0f);
			Vector2f vec2(-1.0f, 1.0f);
			Vector2f vec3 = vec1 / vec2;
			Assert::AreEqual(0.0f, vec3.x, L"Vector x-component not calculated correctly.", LINE_INFO());
			Assert::AreEqual(1.0f, vec3.y, L"Vector y-component not calculated correctly.", LINE_INFO());

			vec3 /= Vector2f(-1.0f, -1.0f);
			Assert::AreEqual(0.0f, vec3.x, L"Vector x-component not calculated correctly.", LINE_INFO());
			Assert::AreEqual(-1.0f, vec3.y, L"Vector y-component not calculated correctly.", LINE_INFO());

			vec3 /= -2.0f;
			Assert::AreEqual(0.0f, vec3.x, L"Vector x-component not calculated correctly.", LINE_INFO());
			Assert::AreEqual(0.5f, vec3.y, L"Vector y-component not calculated correctly.", LINE_INFO());

			vec3 = Vector2f(1.0f, 2.0f) / 4.0f;
			Assert::AreEqual(0.25f, vec3.x, L"Vector x-component not calculated correctly.", LINE_INFO());
			Assert::AreEqual(0.5f, vec3.y, L"Vector y-component not calculated correctly.", LINE_INFO());
		}

		TEST_METHOD(Vector2f_MultiplicationTest)
		{
			Vector2f vec1(0, 1.0f);
			Vector2f vec2(-1.0f, 1.0f);
			Vector2f vec3 = vec1 * vec2;
			Assert::AreEqual(0.0f, vec3.x, L"Vector x-component not calculated correctly.", LINE_INFO());
			Assert::AreEqual(1.0f, vec3.y, L"Vector y-component not calculated correctly.", LINE_INFO());

			vec3 *= Vector2f(-1.0f, -1.0f);
			Assert::AreEqual(0.0f, vec3.x, L"Vector x-component not calculated correctly.", LINE_INFO());
			Assert::AreEqual(-1.0f, vec3.y, L"Vector y-component not calculated correctly.", LINE_INFO());

			vec3 *= -2.0f;
			Assert::AreEqual(0.0f, vec3.x, L"Vector x-component not calculated correctly.", LINE_INFO());
			Assert::AreEqual(2.0f, vec3.y, L"Vector y-component not calculated correctly.", LINE_INFO());

			vec3 = Vector2f(1.0f, 2.0f) * 4.0f;
			Assert::AreEqual(4.0f, vec3.x, L"Vector x-component not calculated correctly.", LINE_INFO());
			Assert::AreEqual(8.0f, vec3.y, L"Vector y-component not calculated correctly.", LINE_INFO());
		}

		TEST_METHOD(Vector2f_NegationTest)
		{
			Vector2f vec1(-3.0f, 1.0f);
			Vector2f vec2 = -vec1;
			Assert::AreEqual(3.0f, vec2.x, L"Vector x-component not calculated correctly.", LINE_INFO());
			Assert::AreEqual(-1.0f, vec2.y, L"Vector y-component not calculated correctly.", LINE_INFO());
		}

		TEST_METHOD(Vector3f_NormalizeTest)
		{
			Vector2f vec1(0, 10.0f);
			Vector2f vec2 = vec1.getNormalized();
			float magnitude1 = vec1.getMagnitude();
			float magnitudesqr1 = vec1.getMagnitudeSqr();
			Assert::AreEqual(10.0f, magnitude1, L"Magnitude not calculated correctly.", LINE_INFO());
			Assert::AreEqual(100.0f, magnitudesqr1, L"Magnitude not calculated correctly.", LINE_INFO());
			magnitude1 = vec2.getMagnitude();
			magnitudesqr1 = vec2.getMagnitudeSqr();
			Assert::AreEqual(0.0f, vec2.x, L"Vector x-component not calculated correctly.", LINE_INFO());
			Assert::AreEqual(1.0f, vec2.y, L"Vector y-component not calculated correctly.", LINE_INFO());
			Assert::AreEqual(1.0f, magnitude1, L"Magnitude not calculated correctly.", LINE_INFO());
			Assert::AreEqual(1.0f, magnitudesqr1, L"Magnitude not calculated correctly.", LINE_INFO());

			vec1.y *= -1;
			vec1.normalize();
			magnitude1 = vec1.getMagnitude();
			magnitudesqr1 = vec1.getMagnitudeSqr();
			Assert::AreEqual(0.0f, vec1.x, L"Vector x-component not calculated correctly.", LINE_INFO());
			Assert::AreEqual(-1.0f, vec1.y, L"Vector y-component not calculated correctly.", LINE_INFO());
			Assert::AreEqual(1.0f, magnitude1, L"Magnitude not calculated correctly.", LINE_INFO());
			Assert::AreEqual(1.0f, magnitudesqr1, L"Magnitude not calculated correctly.", LINE_INFO());

			vec1 = Vector2f(-2.0f, 2.0f);
			vec2 = vec1.getNormalized();
			magnitude1 = vec1.getMagnitude();
			magnitudesqr1 = vec1.getMagnitudeSqr();
			Assert::AreEqual(std::sqrt(8.0f), magnitude1, L"Magnitude not calculated correctly.", LINE_INFO());
			Assert::AreEqual(8.0f, magnitudesqr1, L"Magnitude not calculated correctly.", LINE_INFO());
			magnitude1 = vec2.getMagnitude();
			magnitudesqr1 = vec2.getMagnitudeSqr();
			Assert::AreEqual(-std::sqrt(0.5f), vec2.x, L"Vector x-component not calculated correctly.", LINE_INFO());
			Assert::AreEqual(std::sqrt(0.5f), vec2.y, L"Vector y-component not calculated correctly.", LINE_INFO());
			Assert::AreEqual(1.0f, magnitude1, 0.0001f, L"Magnitude not calculated correctly.", LINE_INFO());
			Assert::AreEqual(1.0f, magnitudesqr1, 0.0001f, L"Magnitude not calculated correctly.", LINE_INFO());

			vec1 *= -1;
			vec1.normalize();
			magnitude1 = vec1.getMagnitude();
			magnitudesqr1 = vec1.getMagnitudeSqr();
			Assert::AreEqual(std::sqrt(0.5f), vec1.x, L"Vector x-component not calculated correctly.", LINE_INFO());
			Assert::AreEqual(-std::sqrt(0.5f), vec1.y, L"Vector y-component not calculated correctly.", LINE_INFO());
			Assert::AreEqual(1.0f, magnitude1, 0.0001f, L"Magnitude not calculated correctly.", LINE_INFO());
			Assert::AreEqual(1.0f, magnitudesqr1, 0.0001f, L"Magnitude not calculated correctly.", LINE_INFO());
		}
	};
}