#include "stdafx.h"
#include "CppUnitTest.h"
#include "Vector2.h"
#include "Vector3.h"
#include "Vector4.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;
using namespace cg::math;

namespace CGMathLibTests
{
	TEST_CLASS(Vector3f_TestClass)
	{
	public:
		TEST_METHOD(Vector3f_ConstructorTest)
		{
			Vector3f vec1(FLT_MAX, 0.0f, -1.0f);
			Assert::AreEqual(FLT_MAX, vec1.x, L"Vector x-component not initialized correctly.", LINE_INFO());
			Assert::AreEqual(0.0f, vec1.y, L"Vector y-component not initialized correctly.", LINE_INFO());
			Assert::AreEqual(-1.0f, vec1.z, L"Vector z-component not initialized correctly.", LINE_INFO());

			Vector3f vec2(-2.5f);
			Assert::AreEqual(-2.5f, vec2.x, L"Vector x-component not initialized correctly.", LINE_INFO());
			Assert::AreEqual(-2.5f, vec2.y, L"Vector y-component not initialized correctly.", LINE_INFO());
			Assert::AreEqual(-2.5f, vec2.z, L"Vector z-component not initialized correctly.", LINE_INFO());

			Vector3f vec3;
			Assert::AreEqual(0.0f, vec3.x, L"Vector x-component not initialized correctly.", LINE_INFO());
			Assert::AreEqual(0.0f, vec3.y, L"Vector y-component not initialized correctly.", LINE_INFO());
			Assert::AreEqual(0.0f, vec3.z, L"Vector z-component not initialized correctly.", LINE_INFO());

			Vector3f vec4 = Vector3f(vec1);
			Assert::AreEqual(FLT_MAX, vec4.x, L"Vector x-component not initialized correctly.", LINE_INFO());
			Assert::AreEqual(0.0f, vec4.y, L"Vector y-component not initialized correctly.", LINE_INFO());
			Assert::AreEqual(-1.0f, vec4.z, L"Vector z-component not initialized correctly.", LINE_INFO());

			Vector3f vec5 = vec2;
			Assert::AreEqual(-2.5f, vec5.x, L"Vector x-component not initialized correctly.", LINE_INFO());
			Assert::AreEqual(-2.5f, vec5.y, L"Vector y-component not initialized correctly.", LINE_INFO());
			Assert::AreEqual(-2.5f, vec5.z, L"Vector z-component not initialized correctly.", LINE_INFO());

			const float data[3] = { 0.5f, -0.5f, FLT_MAX };
			Vector3f vec6 = Vector3f(data);
			Assert::AreEqual(0.5f, vec6.x, L"Vector x-component not initialized correctly.", LINE_INFO());
			Assert::AreEqual(-0.5f, vec6.y, L"Vector y-component not initialized correctly.", LINE_INFO());
			Assert::AreEqual(FLT_MAX, vec6.z, L"Vector z-component not initialized correctly.", LINE_INFO());

			Vector3f vec7 = Vector2f(1.0f, 0.5f);
			Assert::AreEqual(1.0f, vec7.x, L"Vector x-component not initialized correctly.", LINE_INFO());
			Assert::AreEqual(0.5f, vec7.y, L"Vector y-component not initialized correctly.", LINE_INFO());
			Assert::AreEqual(0.0f, vec7.z, L"Vector z-component not initialized correctly.", LINE_INFO());

			Vector3f vec8 = Vector3f(Vector2f(5, -5));
			Assert::AreEqual(5.0f, vec8.x, L"Vector x-component not initialized correctly.", LINE_INFO());
			Assert::AreEqual(-5.0f, vec8.y, L"Vector y-component not initialized correctly.", LINE_INFO());
			Assert::AreEqual(0.0f, vec8.z, L"Vector z-component not initialized correctly.", LINE_INFO());

			Vector3f vec9 = Vector4f(1.0f, 0.5f, 2.0f, 3.0f);
			Assert::AreEqual(1.0f, vec9.x, L"Vector x-component not initialized correctly.", LINE_INFO());
			Assert::AreEqual(0.5f, vec9.y, L"Vector y-component not initialized correctly.", LINE_INFO());
			Assert::AreEqual(2.0f, vec9.z, L"Vector z-component not initialized correctly.", LINE_INFO());

			Vector3f vec10 = Vector3f(Vector4f(5, -5, 10, -10));
			Assert::AreEqual(5.0f, vec10.x, L"Vector x-component not initialized correctly.", LINE_INFO());
			Assert::AreEqual(-5.0f, vec10.y, L"Vector y-component not initialized correctly.", LINE_INFO());
			Assert::AreEqual(10.0f, vec10.z, L"Vector z-component not initialized correctly.", LINE_INFO());

			Vector3f vec11 = Vector2i(2, -2);
			Assert::AreEqual(2.0f, vec11.x, L"Vector x-component not initialized correctly.", LINE_INFO());
			Assert::AreEqual(-2.0f, vec11.y, L"Vector y-component not initialized correctly.", LINE_INFO());
			Assert::AreEqual(0.0f, vec11.z, L"Vector z-component not initialized correctly.", LINE_INFO());

			Vector3f vec12 = Vector3i(2, -2, 1);
			Assert::AreEqual(2.0f, vec12.x, L"Vector x-component not initialized correctly.", LINE_INFO());
			Assert::AreEqual(-2.0f, vec12.y, L"Vector y-component not initialized correctly.", LINE_INFO());
			Assert::AreEqual(1.0f, vec12.z, L"Vector z-component not initialized correctly.", LINE_INFO());
		}

		TEST_METHOD(Vector3f_DirectionTest)
		{
			Vector3f up3 = Vector3f::getUp();
			Assert::AreEqual(0.0f, up3.x, L"Vector x-component not initialized correctly.", LINE_INFO());
			Assert::AreEqual(1.0f, up3.y, L"Vector y-component not initialized correctly.", LINE_INFO());
			Assert::AreEqual(0.0f, up3.z, L"Vector x-component not initialized correctly.", LINE_INFO());
			Vector3f right3 = Vector3f::getRight();
			Assert::AreEqual(1.0f, right3.x, L"Vector x-component not initialized correctly.", LINE_INFO());
			Assert::AreEqual(0.0f, right3.y, L"Vector y-component not initialized correctly.", LINE_INFO());
			Assert::AreEqual(0.0f, right3.z, L"Vector z-component not initialized correctly.", LINE_INFO());
			Vector3f front3 = Vector3f::getForward();
			Assert::AreEqual(0.0f, front3.x, L"Vector x-component not initialized correctly.", LINE_INFO());
			Assert::AreEqual(0.0f, front3.y, L"Vector y-component not initialized correctly.", LINE_INFO());
			Assert::AreEqual(-1.0f, front3.z, L"Vector z-component not initialized correctly.", LINE_INFO());
		}

		TEST_METHOD(Vector3f_MinMaxTest)
		{
			Vector3f vec1(-1.0f, 1.0f, 0.0f);
			Assert::AreEqual(-1.0f, vec1.min(), L"Min component not found.", LINE_INFO());
			Assert::AreEqual(1.0f, vec1.max(), L"Max component not found.", LINE_INFO());
			Vector3f vec2(-15.0f, -15.0f, -15.0f);
			Assert::AreEqual(-15.0f, vec2.min(), L"Min component not found.", LINE_INFO());
			Assert::AreEqual(-15.0f, vec2.max(), L"Max component not found.", LINE_INFO());
			Vector3f vec3(15.0f, 15.0f, 15.0f);
			Assert::AreEqual(15.0f, vec3.min(), L"Min component not found.", LINE_INFO());
			Assert::AreEqual(15.0f, vec3.max(), L"Max component not found.", LINE_INFO());
			Vector3f vec4(1.0f, 2.0f, 1.5f);
			Assert::AreEqual(1.0f, vec4.min(), L"Min component not found.", LINE_INFO());
			Assert::AreEqual(2.0f, vec4.max(), L"Max component not found.", LINE_INFO());
			Vector3f vec5(1.5f, 2.0f, 1.0f);
			Assert::AreEqual(1.0f, vec5.min(), L"Min component not found.", LINE_INFO());
			Assert::AreEqual(2.0f, vec5.max(), L"Max component not found.", LINE_INFO());
			Vector3f vec6(-2.0f, -1.5f, -1.0f);
			Assert::AreEqual(-2.0f, vec6.min(), L"Min component not found.", LINE_INFO());
			Assert::AreEqual(-1.0f, vec6.max(), L"Max component not found.", LINE_INFO());
			Vector3f vec7(-1.0f, -1.5f, -2.0f);
			Assert::AreEqual(-2.0f, vec7.min(), L"Min component not found.", LINE_INFO());
			Assert::AreEqual(-1.0f, vec7.max(), L"Max component not found.", LINE_INFO());
			Vector3f vec8(FLT_MAX, FLT_MIN, 0.5f);
			Assert::AreEqual(FLT_MIN, vec8.min(), L"Min component not found.", LINE_INFO());
			Assert::AreEqual(FLT_MAX, vec8.max(), L"Max component not found.", LINE_INFO());
		}

		TEST_METHOD(Vector3f_DataAccessTest)
		{
			Vector3f vec1 = Vector3f(0.5f, -0.5f, 1.0f);
			Assert::AreEqual(0.5f, vec1[0], L"Vector x-component not initialized correctly.", LINE_INFO());
			Assert::AreEqual(-0.5f, vec1[1], L"Vector y-component not initialized correctly.", LINE_INFO());
			Assert::AreEqual(1.0f, vec1[2], L"Vector z-component not initialized correctly.", LINE_INFO());
			vec1[0] -= 1.0f;
			vec1[1] += 1.0f;
			vec1[2] *= 2.0f;
			Assert::AreEqual(-0.5f, vec1[0], L"Vector x-component not initialized correctly.", LINE_INFO());
			Assert::AreEqual(0.5f, vec1[1], L"Vector y-component not initialized correctly.", LINE_INFO());
			Assert::AreEqual(2.0f, vec1[2], L"Vector z-component not initialized correctly.", LINE_INFO());
			Assert::AreEqual(-0.5f, vec1.getData()[0], L"Vector x-component not initialized correctly.", LINE_INFO());
			Assert::AreEqual(0.5f, vec1.getData()[1], L"Vector y-component not initialized correctly.", LINE_INFO());
			Assert::AreEqual(2.0f, vec1.getData()[2], L"Vector z-component not initialized correctly.", LINE_INFO());
		}

		TEST_METHOD(Vector3f_AdditionTest)
		{
			Vector3f vec1(0, 1.0f, 2.0f);
			Vector3f vec2(-1.0f, 1.0f, -2.0f);
			Vector3f vec3 = vec1 + vec2;
			Assert::AreEqual(-1.0f, vec3.x, L"Vector x-component not calculated correctly.", LINE_INFO());
			Assert::AreEqual(2.0f, vec3.y, L"Vector y-component not calculated correctly.", LINE_INFO());
			Assert::AreEqual(0.0f, vec3.z, L"Vector z-component not calculated correctly.", LINE_INFO());

			vec3 += Vector3f(-1.0f, -1.0f, 5.0f);
			Assert::AreEqual(-2.0f, vec3.x, L"Vector x-component not calculated correctly.", LINE_INFO());
			Assert::AreEqual(1.0f, vec3.y, L"Vector y-component not calculated correctly.", LINE_INFO());
			Assert::AreEqual(5.0f, vec3.z, L"Vector z-component not calculated correctly.", LINE_INFO());
		}

		TEST_METHOD(Vector3f_SubtractTest)
		{
			Vector3f vec1(0, 1.0f, 2.0f);
			Vector3f vec2(-1.0f, 1.0f, -2.0f);
			Vector3f vec3 = vec1 - vec2;
			Assert::AreEqual(1.0f, vec3.x, L"Vector x-component not calculated correctly.", LINE_INFO());
			Assert::AreEqual(0.0f, vec3.y, L"Vector y-component not calculated correctly.", LINE_INFO());
			Assert::AreEqual(4.0f, vec3.z, L"Vector z-component not calculated correctly.", LINE_INFO());

			vec3 -= Vector3f(-1.0f, -1.0f, 5.0f);
			Assert::AreEqual(2.0f, vec3.x, L"Vector x-component not calculated correctly.", LINE_INFO());
			Assert::AreEqual(1.0f, vec3.y, L"Vector y-component not calculated correctly.", LINE_INFO());
			Assert::AreEqual(-1.0f, vec3.z, L"Vector z-component not calculated correctly.", LINE_INFO());
		}

		TEST_METHOD(Vector3f_DivisionTest)
		{
			Vector3f vec1(0, 1.0f, 2.0f);
			Vector3f vec2(-1.0f, 1.0f, -2.0f);
			Vector3f vec3 = vec1 / vec2;
			Assert::AreEqual(0.0f, vec3.x, L"Vector x-component not calculated correctly.", LINE_INFO());
			Assert::AreEqual(1.0f, vec3.y, L"Vector y-component not calculated correctly.", LINE_INFO());
			Assert::AreEqual(-1.0f, vec3.z, L"Vector z-component not calculated correctly.", LINE_INFO());

			vec3 /= Vector3f(-1.0f, -1.0f, 5.0f);
			Assert::AreEqual(0.0f, vec3.x, L"Vector x-component not calculated correctly.", LINE_INFO());
			Assert::AreEqual(-1.0f, vec3.y, L"Vector y-component not calculated correctly.", LINE_INFO());
			Assert::AreEqual(-0.2f, vec3.z, L"Vector z-component not calculated correctly.", LINE_INFO());

			vec3 /= -2.0f;
			Assert::AreEqual(0.0f, vec3.x, L"Vector x-component not calculated correctly.", LINE_INFO());
			Assert::AreEqual(0.5f, vec3.y, L"Vector y-component not calculated correctly.", LINE_INFO());
			Assert::AreEqual(0.1f, vec3.z, L"Vector z-component not calculated correctly.", LINE_INFO());

			vec3 = Vector3f(1.0f, 2.0f, 3.0f) / 4.0f;
			Assert::AreEqual(0.25f, vec3.x, L"Vector x-component not calculated correctly.", LINE_INFO());
			Assert::AreEqual(0.5f, vec3.y, L"Vector y-component not calculated correctly.", LINE_INFO());
			Assert::AreEqual(0.75f, vec3.z, L"Vector z-component not calculated correctly.", LINE_INFO());
		}

		TEST_METHOD(Vector3f_MultiplicationTest)
		{
			Vector3f vec1(0, 1.0f, 2.0f);
			Vector3f vec2(-1.0f, 1.0f, -2.0f);
			Vector3f vec3 = vec1 * vec2;
			Assert::AreEqual(0.0f, vec3.x, L"Vector x-component not calculated correctly.", LINE_INFO());
			Assert::AreEqual(1.0f, vec3.y, L"Vector y-component not calculated correctly.", LINE_INFO());
			Assert::AreEqual(-4.0f, vec3.z, L"Vector z-component not calculated correctly.", LINE_INFO());

			vec3 *= Vector3f(-1.0f, -1.0f, 5.0f);
			Assert::AreEqual(0.0f, vec3.x, L"Vector x-component not calculated correctly.", LINE_INFO());
			Assert::AreEqual(-1.0f, vec3.y, L"Vector y-component not calculated correctly.", LINE_INFO());
			Assert::AreEqual(-20.0f, vec3.z, L"Vector z-component not calculated correctly.", LINE_INFO());

			vec3 *= -2.0f;
			Assert::AreEqual(0.0f, vec3.x, L"Vector x-component not calculated correctly.", LINE_INFO());
			Assert::AreEqual(2.0f, vec3.y, L"Vector y-component not calculated correctly.", LINE_INFO());
			Assert::AreEqual(40.0f, vec3.z, L"Vector z-component not calculated correctly.", LINE_INFO());

			vec3 = Vector3f(1.0f, 2.0f, 3.0f) * 4.0f;
			Assert::AreEqual(4.0f, vec3.x, L"Vector x-component not calculated correctly.", LINE_INFO());
			Assert::AreEqual(8.0f, vec3.y, L"Vector y-component not calculated correctly.", LINE_INFO());
			Assert::AreEqual(12.0f, vec3.z, L"Vector z-component not calculated correctly.", LINE_INFO());
		}

		TEST_METHOD(Vector3f_NegationTest)
		{
			Vector3f vec1(-3.0f, 1.0f, 2.0f);
			Vector3f vec2 = -vec1;
			Assert::AreEqual(3.0f, vec2.x, L"Vector x-component not calculated correctly.", LINE_INFO());
			Assert::AreEqual(-1.0f, vec2.y, L"Vector y-component not calculated correctly.", LINE_INFO());
			Assert::AreEqual(-2.0f, vec2.z, L"Vector z-component not calculated correctly.", LINE_INFO());
		}

		TEST_METHOD(Vector3f_NormalizeTest)
		{
			Vector3f vec1(0, 10.0f, 0);
			Vector3f vec2 = vec1.getNormalized();
			float magnitude1 = vec1.getMagnitude();
			float magnitudesqr1 = vec1.getMagnitudeSqr();
			Assert::AreEqual(10.0f, magnitude1, L"Magnitude not calculated correctly.", LINE_INFO());
			Assert::AreEqual(100.0f, magnitudesqr1, L"Magnitude not calculated correctly.", LINE_INFO());
			magnitude1 = vec2.getMagnitude();
			magnitudesqr1 = vec2.getMagnitudeSqr();
			Assert::AreEqual(0.0f, vec2.x, L"Vector x-component not calculated correctly.", LINE_INFO());
			Assert::AreEqual(1.0f, vec2.y, L"Vector y-component not calculated correctly.", LINE_INFO());
			Assert::AreEqual(0.0f, vec2.z, L"Vector z-component not calculated correctly.", LINE_INFO());
			Assert::AreEqual(1.0f, magnitude1, L"Magnitude not calculated correctly.", LINE_INFO());
			Assert::AreEqual(1.0f, magnitudesqr1, L"Magnitude not calculated correctly.", LINE_INFO());

			vec1.y *= -1;
			vec1.normalize();
			magnitude1 = vec1.getMagnitude();
			magnitudesqr1 = vec1.getMagnitudeSqr();
			Assert::AreEqual(0.0f, vec1.x, L"Vector x-component not calculated correctly.", LINE_INFO());
			Assert::AreEqual(-1.0f, vec1.y, L"Vector y-component not calculated correctly.", LINE_INFO());
			Assert::AreEqual(0.0f, vec1.z, L"Vector z-component not calculated correctly.", LINE_INFO());
			Assert::AreEqual(1.0f, magnitude1, L"Magnitude not calculated correctly.", LINE_INFO());
			Assert::AreEqual(1.0f, magnitudesqr1, L"Magnitude not calculated correctly.", LINE_INFO());

			vec1 = Vector3f(-2.0f, 2.0f, 0.0f);
			vec2 = vec1.getNormalized();
			magnitude1 = vec1.getMagnitude();
			magnitudesqr1 = vec1.getMagnitudeSqr();
			Assert::AreEqual(std::sqrt(8.0f), magnitude1, L"Magnitude not calculated correctly.", LINE_INFO());
			Assert::AreEqual(8.0f, magnitudesqr1, L"Magnitude not calculated correctly.", LINE_INFO());
			magnitude1 = vec2.getMagnitude();
			magnitudesqr1 = vec2.getMagnitudeSqr();
			Assert::AreEqual(-std::sqrt(0.5f), vec2.x, L"Vector x-component not calculated correctly.", LINE_INFO());
			Assert::AreEqual(std::sqrt(0.5f), vec2.y, L"Vector y-component not calculated correctly.", LINE_INFO());
			Assert::AreEqual(0.0f, vec2.z, L"Vector z-component not calculated correctly.", LINE_INFO());
			Assert::AreEqual(1.0f, magnitude1, 0.0001f, L"Magnitude not calculated correctly.", LINE_INFO());
			Assert::AreEqual(1.0f, magnitudesqr1, 0.0001f, L"Magnitude not calculated correctly.", LINE_INFO());

			vec1 *= -1;
			vec1.normalize();
			magnitude1 = vec1.getMagnitude();
			magnitudesqr1 = vec1.getMagnitudeSqr();
			Assert::AreEqual(std::sqrt(0.5f), vec1.x, L"Vector x-component not calculated correctly.", LINE_INFO());
			Assert::AreEqual(-std::sqrt(0.5f), vec1.y, L"Vector y-component not calculated correctly.", LINE_INFO());
			Assert::AreEqual(0.0f, vec2.z, L"Vector z-component not calculated correctly.", LINE_INFO());
			Assert::AreEqual(1.0f, magnitude1, 0.0001f, L"Magnitude not calculated correctly.", LINE_INFO());
			Assert::AreEqual(1.0f, magnitudesqr1, 0.0001f, L"Magnitude not calculated correctly.", LINE_INFO());
		}
	};
}